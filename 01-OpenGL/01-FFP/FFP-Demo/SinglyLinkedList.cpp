#include <stdio.h>
#include <stdlib.h>
#include "datastructure.h"

//struct Node {
//
//	float x;
//	float y;
//	float z;
//	struct Node* next;
//
//};

struct Node* head;

struct Node* createNode(float x, float y, float z) {
	
	struct Node* newNode = (struct Node *)malloc(sizeof(struct Node));
	newNode->x = x;
	newNode->y = y;
	newNode->z = z;
	newNode->next = NULL;

	return newNode;

}

void addNode(float x, float y, float z) {

	struct Node* newNode = createNode(x, y, z);
	struct Node* temp = NULL;
	if (head == NULL) {

		head = newNode;

	} else {
		
		temp = head;
		while (temp->next != NULL) {
			
			temp = temp->next;
		
		}

		temp->next = newNode;
	}

}

int countNode() {
	
	struct Node* temp = head;
	int count = 0;
	if (head == NULL) {
		
		return 0;
	}
	while (temp != NULL) {
		
		temp = temp->next;
		count++;
	
	}

	return count;
}

int deleteNode(int count) {
	
	struct Node* temp = NULL;
	struct Node* temp1 = NULL;
	if (countNode() == 0) {
	
		printf("Don't have element to delete.\n");
		return -1;
	} if (countNode() < count) {
		
		printf("Please enter correct count\n");
		return -1;
	
	}
	if (count == 1) {
		
		temp = head->next;
		free(head);
		head = temp;
	
	} else {
		
		temp = head;
		for (int i = 1; i < count-1; i++) {
				
			temp = temp->next;

		}
		temp1 = temp->next;
		temp->next = temp1->next;
		free(temp1);
	
	}
	return 0;

}

void displayNode() {
	
	struct Node* temp = head;
	while (temp != NULL) {
		
		printf("| %f %f %f |\n", temp->x, temp->y, temp->z);
		temp = temp->next;
	
	}

}

struct Node* getNode(int index) {

	struct Node* temp = head;
	for (int i = 1; i < index; i++) {
		
		temp = temp->next;
	}

	return temp;
}


void main() {
	
	addNode(1.1, 1.1, 1.1);
	addNode(2.1, 2.1, 2.1);
	addNode(3.1, 3.1, 3.1);
	addNode(4.1, 4.1, 4.1);
	printf("Display Before delete\n");
	displayNode();
	deleteNode(7);
	printf("Display After delete\n");
	displayNode();
	struct Node* node = NULL;
	node = getNode(4);
	printf("GetNode 4 = %f %f %f\n", node->x, node->y, node->z);

	printf("%d\n", countNode());
}