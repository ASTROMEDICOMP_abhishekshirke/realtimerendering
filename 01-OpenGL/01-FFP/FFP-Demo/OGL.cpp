// header files
#include <windows.h>
#include "OGL.h"

#include <stdio.h>				//for file io functions
#include <stdlib.h>				//for exit()
#include <math.h>		
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include ".\\datastructure.h"

// OpenGL header files
#include <GL/gl.h>
#include <GL/glu.h>


// OpenGL Libraries
#pragma comment (lib, "OpenGL32.lib")
#pragma comment (lib, "glu32.lib")

#define WIN_WIDTH	800			
#define WIN_HEIGHT	600


// Global Funtion Declaration
LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

//Global variable declaration
HWND ghwnd;
BOOL gbFullScreen = FALSE;
FILE *gpFile = NULL;
BOOL gbActiveWindow = FALSE;
BOOL bDone = FALSE;
int iRetVal = 0;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

// camera variables
float sceneOnex = -4.0f;
float sceneOney = -0.5f;
float sceneOnez = -6.0f;

float cx;
float cy = 0.0f;
float cz = 0.0f;

GLUquadric* quadric = NULL;

// ------------------------------------------ Scene Title Texture ------------------------------------------

GLuint texture_title;
// city
GLuint texture_building1;
GLuint texture_building2;
GLuint texture_building3;
GLuint texture_building4;
GLuint texture_building5;
GLuint texture_building6;
GLuint texture_building7;

// sky
GLuint texture_sky;

// Scene one ground
GLuint texture_cg1;
GLuint texture_cg2;

// humanoid
int sceneOneShoulderL = 0;
int sceneOneShoulderR = 0;
int sceneOneElbowL = 0;
int sceneOneElbowR = 0;
int sceneOneWrist = 0;
float sceneOneWholeBodyAngle = 0.0f;
int sceneOneUpperLeg = 0;
int sceneOneLowerLeg = 0;

// secneOne human texture
GLuint texture_shirt;
GLuint texture_pant;
GLuint texture_head;

// child 1
GLuint sceneOneTexture_shirt1;
GLuint sceneOneTexture_pant1;


// child 2
GLuint sceneOneTexture_shirt2;
GLuint sceneOneTexture_pant2;

// child c3
GLuint sceneOneTexture_shirt3;
GLuint sceneOneTexture_pant3;

// c4
GLuint sceneOneTexture_shirt4;
GLuint sceneOneTexture_pant4;

// bioscope
GLuint sceneOneTexture_bioscope;
GLuint sceneOneTexture_woodbar;
GLuint sceneOneTexture_bioscope_poster;

// Bus
GLuint texture_bus_side;
GLuint texture_bus_front;
GLuint texture_tyre;

// Image
GLuint texture_image;
GLuint texture_role1;
GLuint texture_role2;
GLuint texture_role3;
GLuint texture_role4;
GLuint texture_role5;



float sceneOneHumanWidth = 1.3f;
float sceneOneHumanMiddle = 3.5f;


float sceneOneFullBodyMove = -13.5;
float sceneOneZBodyMove = 0.0f;


int sceneOneLegFlag = 0;

// Start key flag 
int sceneOneStartKeyFlag = 0;
int sceneOneZFLag = 0;

// 
int sceneOneBendFlag = 0;

// Scene 2 variables
// camera variables
float sceneTwoX;
float sceneTwoY = 1.0f;
float sceneTwoZ = 23.0;

// Tree variables
float sceneTwoHeight = 0.6f;
int sceneTwoCount = 0;

float sceneTwoRAngle = 0.0f;
float sceneTwoRediusReduce = 0.58f;
int sceneTwoFlagTree = 0;

float sceneTwoHeightLimit = 0.16f;

// Terrain
float sceneTwoCol = 30;
float sceneTwoRow = 10;
float sceneTwoScl = 40.0f;

float sceneThreeCol = 50;
float sceneThreeRow = 10;
float sceneThreeScl = 40.0f;

float terrainZ[40][40];


// Fog
GLfloat sceneTwoUp = -23.0f;
GLfloat sceneTwoDown = 62.0f;

// Camera rotate
float sceneTwoR = 0.0f;

// Rotate angle flag
// for temple turn
int sceneTwoRotateFLag = 0;

// road
GLuint sceneTwoTexture_road;
GLuint sceneTwoTexture_grass;

// tree
GLuint sceneTwoTexture_leaf;
GLuint sceneTwoTexture_trunk;
GLuint sceneTwoTexture_base;

// mark
GLuint sceneTwoTexture_mark;

// Temple texture
GLuint sceneTwoTexture_pole;
GLuint sceneTwoTexture_stares;
GLuint sceneTwoTexture_top;
GLuint sceneTwoTexture_upper;
GLuint sceneTwoTexture_god;
GLuint sceneTwoTexture_side;		// for main cube

// Tree
GLuint sceneTwoTexture_Leaves;

// Terrain
GLuint sceneTwoTexture_water;

// Board
GLuint sceneTwoTexture_woodbar;
GLuint sceneTwoTexture_bioscope;
GLuint villageBoard;

// Bird
GLuint sceneTwoTexture_bird_middle;

// Three bird display flag
int sceneTwoThreeBirdDisplayFlag = 1;

// RotateAngle
int sceneTwoRotate = 0;

// Bird Translation
float sceneTwoBirdX = -6.0f;
float sceneTwoBirdY = 2.0f;
float sceneTwoBirdZ = 28.0f;


float sceneTwoMainBirdX = -3.0f;
float sceneTwoMainBirdY = 3.0f;
float sceneTwoMainBirdZ = 25.0f;

// Bird Rotation
float sceneTwoBirdRotate = -20.0f;

// bird flag
float sceneTwoWingFlag = 0;
int sceneTwoFlagBird = 0;


// Scene 3
// camera variables
float sceneThreeX = -16.5f;
float sceneThreeY = 0.5f;
float sceneThreeZ = 12.5f;

GLuint sceneThreeTexture_wall1;
GLuint sceneThreeTexture_wall2;
GLuint sceneThreeTexture_h2s;
GLuint sceneThreeTexture_h2md;
GLuint sceneThreeTexture_h3s;
GLuint sceneThreeTexture_h3md;
GLuint sceneThreeTexture_h4s;
GLuint sceneThreeTexture_h4md;
GLuint sceneThreeTexture_h5s;
GLuint sceneThreeTexture_h5md;
GLuint sceneThreeTexture_h5sf;
GLuint sceneThreeTexture_g1;

// home
GLuint sceneThreeTexture_Fence;
GLuint sceneThreeTexture_roof;
GLuint sceneThreeTexture_room;
GLuint sceneThreeTexture_room1;

// Girl
GLuint sceneThreeTexture_sadi;
GLuint sceneThreeTexture_sadi_lower;
GLuint sceneThreeTexture_sadi_triangle;
GLuint sceneThreeTexture_arm;
GLuint sceneThreeTexture_pant;
GLuint sceneThreeTexture_head;

// Cart
GLuint sceneThreeTexture_bars;
GLuint sceneThreeTexture_cart_base;
GLuint sceneThreeTexture_bar;

// Tree
GLuint sceneThreeTexture_leaf;
GLuint sceneThreeTexture_trunk;
GLuint sceneThreeTexture_base;
GLuint sceneThreeTexture_Leaves;

// CubeMap
GLuint sceneThreeTexture_front;
GLuint sceneThreeTexture_right;
GLuint sceneThreeTexture_back;
GLuint sceneThreeTexture_left;
GLuint sceneThreeTexture_top;
GLuint sceneThreeTexture_bottom;

GLuint texture_leady;


float sceneThreeHumanWidth = 1.3f;
float sceneThreeHumanMiddle = 3.5f;

float sceneThreeFullBodyMove = 0.0;
int sceneThreeLegFlag = 0;

int sceneThreeShoulderL = 0;
int sceneThreeShoulderR = 0;
int sceneThreeElbowL = 0;
int sceneThreeElbowR = 0;
int sceneThreeWrist = 0;
int sceneThreeWholeBodyAngle = 90.0f;
int sceneThreeUpperLeg = 0;
int sceneThreeLowerLeg = 0;

// Tree
float sceneThreeHeight = 0.6f;
int sceneThreeCount = 0;

float sceneThreeRAngle = 0.0f;
float sceneThreeRediusReduce = 0.58f;
int sceneThreeFlag = 0;

float heightLimit = 0.16f;

// Backgorund color
float sceneThreeBackR = 135.0f;
float sceneThreeBackG = 206.0;
float sceneThreeBackB = 235.0;

// Blue Color Flag
float sceneThreeBlueR = 135.0f;
float sceneThreeBlueG = 206.0;
float sceneThreeBlueB = 235.0;


// Color flag
int sceneThreeBagColorFLag = 0;
int sceneThreeBagColorCount = 0;



// scene flag
int sceneZeroDisplayFlag = 0;
int sceneTitleDisplayFLag = 0;
int sceneOneDisplayFlag = 0;
int sceneTwoRoleDisplayFlag = 0;
int sceneReturnRoleDisplayFlag = 0;

int sceneTwoDisplayFlag = 0;
int sceneThreeDisplayFlag = 0;
int scenesEndFlag = 0;
int nameCreditFlag = 0;
int technologyCreditFlag = 0;
int specialThanksFlag = 0;


//
float lightDiffuseValue = 0.0;

// Lights and font
GLuint nFontList;


GLfloat lightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[] = { lightDiffuseValue,lightDiffuseValue,lightDiffuseValue,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition[] = { 50.0f,50.0f,50.0f,1.0f };

GLfloat materialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat materialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialShininess = 50.0f;
BOOL gbLight = FALSE;


// End Credits
float devX = -7.0f;
float devY = 0.0f;
float devZ = 0.0f;

float nameX = 5.0f;
float nameY = -0.7f;
float nameZ = 0.0f;

float techX = -1.0f;
float techY = -2.0f;
float techZ = 0.0f;


// Entry Point Function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {

	// function declarations
	int initialize(void);
	void display(void);
	void update(void);
	void uninitialize(void);

	// variable declarations 
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");
	//int width = 800, height = 600;
	int SysWidth = GetSystemMetrics(SM_CXSCREEN);
	int SysHeight = GetSystemMetrics(SM_CYSCREEN);

	// code

	if (fopen_s(&gpFile, "Log.txt", "w") != 0) {

		MessageBox(NULL, TEXT("Creation Of Log File Failed Exiting..."), TEXT("File I/O Error"), MB_OK);
		exit(0);

	}
	else {

		fprintf(gpFile, "Log File Successfully Opned\n");

	}


	// initalization of WNDCLASSEX structure

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;													//handle
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);					//handle

	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));								//handle	
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);									//handle
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));


	// registering above wndclass
	RegisterClassEx(&wndclass);

	// create the window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Abhishek Laxman Shirke"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(SysWidth / 2) - WIN_WIDTH / 2,
		(SysHeight / 2) - WIN_HEIGHT / 2,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	// initialize
	iRetVal = initialize();
	if (iRetVal == -1) {

		fprintf(gpFile, "ChoosePixelFormat falied\n");
		uninitialize();

	}
	else if (iRetVal == -2) {

		fprintf(gpFile, "SetPixelFormat falied\n");
		uninitialize();
	}
	else if (iRetVal == -3) {

		fprintf(gpFile, "wglCreateContext falied\n");
		uninitialize();
	}
	else if (iRetVal == -4) {

		fprintf(gpFile, "making OpenGL context as current context failed\n");
			uninitialize();
	
	} else {
		
		fprintf(gpFile, "call to initalize successfull\n");
	}


	// show window
	ShowWindow(hwnd, iCmdShow);

	//Foregrounding and focusing the window
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	

	// GameLoop
	while (bDone == FALSE) {
		
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			
			if (msg.message == WM_QUIT) {
				
				bDone = TRUE;
			
			} else {
				
				TranslateMessage(&msg);
				DispatchMessage(&msg);

			}
		
		} else {
		
			//if (gbActiveWindow == TRUE) {
				
				// Render the scene
				display();

				// update the scene
				update();
			
			//}
		}
	
	
	}
		
	uninitialize();													//change done
	return ((int) msg.wParam);
}

// Callback Function
LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	
	int width, height;
	TCHAR str[255];

	// function declarations
	void ToggleFullScreen(void);
	void resize(int, int);
	//void uninitialize(void);

	// code
	switch (iMsg) {
		
		case WM_SETFOCUS:
			gbActiveWindow = TRUE;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = FALSE;
			break;
		case WM_ERASEBKGND:
			return 0;
		case WM_CHAR:
			switch (wParam) {
				
				case 27:
					PostQuitMessage(0);
					break;
				case 'F':
				case 'f':
					ToggleFullScreen();
					sceneZeroDisplayFlag = 1;
					PlaySound(".\\Music\\FullAudioWIthOutro.wav", NULL, SND_ASYNC);
					break;
				case 'o':
				case 'O':
					sceneOneStartKeyFlag = 1;	
					break;
				default:
					break;
			}
			break;
		case WM_SIZE:
			resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_CLOSE:
			DestroyWindow(hWnd);
			break;
		case WM_DESTROY:
			//uninitialize();
			PostQuitMessage(0);
			break;
		
		default:
			break;
	}

	return (DefWindowProc(hWnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void) {

	//Variable Declarations
	static DWORD dwStyle;
	static WINDOWPLACEMENT wp;
	MONITORINFO mi;

	//code
	wp.length = sizeof(WINDOWPLACEMENT);

	if (gbFullScreen == FALSE) {

		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) {

			mi.cbSize = sizeof(MONITORINFO);

			if (GetWindowPlacement(ghwnd, &wp) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {

				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}

			ShowCursor(FALSE);
			gbFullScreen = TRUE;

		}

	}
	else {

		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wp);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
		gbFullScreen = FALSE;
	}
}

int initialize(void) {
	
	// function declarations
	// warmup resize
	void resize(int, int);
	BOOL LoadGLTexture(GLuint*, TCHAR[]);
	void uninitialize(void);
	void genarateZ(int row, int col);
	BOOL NewLoadGLTexture(GLuint*, const char*);
	void SetupRC(HDC hDC, char* fontName);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	// code
	// Initialization of PIXELFORMATDESCRIPTOR structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 0;
	pfd.cDepthBits = 32;							//24 can also be done

	// Get DC
	ghdc = GetDC(ghwnd);

	// Choose pixel format
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
		return -1;
	
	// Set the choosen pixel format
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
		return -2;

	// Create OpenGL Rendaring Context
	ghrc = wglCreateContext(ghdc);
	
	if (ghrc == NULL)
		return -3;

	// make the rendaring context as a current context 
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
		return -4;

	// Here starts OpenGL code
	// Clear the screen using black color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	// Depth related changes (5 romentic lines)
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// optional function
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	// Fog
	float fLowLight[] = { 0.7f, 0.7f, 0.7f, 1.0f };

	glEnable(GL_FOG); // Turn Fog on
	glFogfv(GL_FOG_COLOR, fLowLight); // Set fog color to match background

	glFogi(GL_FOG_MODE, GL_LINEAR); // Which fog equation to use

	glHint(GL_FOG_HINT, GL_NICEST);

	glDisable(GL_FOG);
	//glEnable(GL_FOG);

	if (LoadGLTexture(&texture_building1, MAKEINTRESOURCE(IDBITMAP_BUILDING1)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_building1");
		uninitialize();
		return -106;

	}

	if (LoadGLTexture(&texture_building2, MAKEINTRESOURCE(IDBITMAP_BUILDING2)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_building2");
		uninitialize();
		return -107;

	}

	if (LoadGLTexture(&texture_building3, MAKEINTRESOURCE(IDBITMAP_BUILDING3)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_building3");
		uninitialize();
		return -108;

	}
	if (LoadGLTexture(&texture_building4, MAKEINTRESOURCE(IDBITMAP_BUILDING4)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_building4");
		uninitialize();
		return -109;

	}
	if (LoadGLTexture(&texture_building5, MAKEINTRESOURCE(IDBITMAP_BUILDING5)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_building5");
		uninitialize();
		return -110;

	}
	if (LoadGLTexture(&texture_building6, MAKEINTRESOURCE(IDBITMAP_BUILDING6)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_building6");
		uninitialize();
		return 111;

	}
	if (LoadGLTexture(&texture_building7, MAKEINTRESOURCE(IDBITMAP_BUILDING7)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_building7");
		uninitialize();
		return -112;

	}


	if (LoadGLTexture(&texture_sky, MAKEINTRESOURCE(IDBITMAP_SKY)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_sky");
		uninitialize();
		return -150;

	}

	if (LoadGLTexture(&texture_sky, MAKEINTRESOURCE(IDBITMAP_SKY)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_sky");
		uninitialize();
		return -150;

	}

	if (LoadGLTexture(&texture_cg1, MAKEINTRESOURCE(IDBITMAP_CITY_GROUND1)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_cg1");
		uninitialize();
		return -113;

	}

	if (LoadGLTexture(&texture_cg2, MAKEINTRESOURCE(IDBITMAP_CITY_GROUND2)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_cg2");
		uninitialize();
		return -114;

	}


	if (LoadGLTexture(&texture_shirt, MAKEINTRESOURCE(IDBITMAP_SHIRT)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_shirt");
		uninitialize();
		return -115;

	}

	if (LoadGLTexture(&texture_pant, MAKEINTRESOURCE(IDBITMAP_PANT)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_pant");
		uninitialize();
		return -116;

	}

	if (LoadGLTexture(&texture_head, MAKEINTRESOURCE(IDBITMAP_HEAD)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_head");
		uninitialize();
		return -117;

	}

	// c1
	if (LoadGLTexture(&sceneOneTexture_shirt1, MAKEINTRESOURCE(IDBITMAP_SHIRT1)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_shirt1");
		uninitialize();
		return -118;

	}

	if (LoadGLTexture(&sceneOneTexture_pant1, MAKEINTRESOURCE(IDBITMAP_PANT1)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_pant1");
		uninitialize();
		return -119;

	}

	// c2
	if (LoadGLTexture(&sceneOneTexture_shirt2, MAKEINTRESOURCE(IDBITMAP_SHIRT2)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_shirt2");
		uninitialize();
		return -120;

	}

	if (LoadGLTexture(&sceneOneTexture_pant2, MAKEINTRESOURCE(IDBITMAP_PANT2)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_pant2");
		uninitialize();
		return -121;

	}

	// c3
	if (LoadGLTexture(&sceneOneTexture_shirt3, MAKEINTRESOURCE(IDBITMAP_SHIRT3)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_shirt3");
		uninitialize();
		return -122;

	}

	if (LoadGLTexture(&sceneOneTexture_pant3, MAKEINTRESOURCE(IDBITMAP_PANT3)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_pant3");
		uninitialize();
		return -123;

	}

	// c4
	if (LoadGLTexture(&sceneOneTexture_shirt4, MAKEINTRESOURCE(IDBITMAP_SHIRT4)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_shirt4");
		uninitialize();
		return -124;

	}
	if (LoadGLTexture(&sceneOneTexture_pant4, MAKEINTRESOURCE(IDBITMAP_PANT4)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_pant4");
		uninitialize();
		return -125;

	}

	if (LoadGLTexture(&sceneOneTexture_bioscope, MAKEINTRESOURCE(IDBITMAP_BIOSCOPE)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_bioscope");
		uninitialize();
		return -126;

	}

	if (LoadGLTexture(&texture_image, MAKEINTRESOURCE(IDBITMAP_IMAGE)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_image");
		uninitialize();
		return -127;

	}

	if (LoadGLTexture(&texture_role1, MAKEINTRESOURCE(IDBITMAP_ROLE1)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_role1");
		uninitialize();
		return -128;

	}

	if (LoadGLTexture(&texture_role2, MAKEINTRESOURCE(IDBITMAP_ROLE2)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_role2");
		uninitialize();
		return -129;

	}

	if (LoadGLTexture(&texture_role3, MAKEINTRESOURCE(IDBITMAP_ROLE3)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_role3");
		uninitialize();
		return -130;

	}

	if (LoadGLTexture(&texture_role4, MAKEINTRESOURCE(IDBITMAP_ROLE4)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_role4");
		uninitialize();
		return -131;

	}

	if (LoadGLTexture(&texture_role5, MAKEINTRESOURCE(IDBITMAP_ROLE5)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_role5");
		uninitialize();
		return -132;

	}

	if (LoadGLTexture(&sceneOneTexture_woodbar, MAKEINTRESOURCE(IDBITMAP_WOOD_BAR)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_woodbar");
		uninitialize();
		return -133;

	}

	if (LoadGLTexture(&sceneOneTexture_bioscope_poster, MAKEINTRESOURCE(IDBITMAP_POSTER)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_bioscope");
		uninitialize();
		return -131;

	}

	// Title
	if (LoadGLTexture(&texture_title, MAKEINTRESOURCE(IDBITMAP_TITLE)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_title");
		return -132;
	}

	// Scene 2
	if (LoadGLTexture(&sceneTwoTexture_road, MAKEINTRESOURCE(IDBITMAP_ROAD)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_road");
		uninitialize();
		return -226;

	}


	if (LoadGLTexture(&sceneTwoTexture_grass, MAKEINTRESOURCE(IDBITMAP_GRASS)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_grass");
		uninitialize();
		return -227;

	}

	if (LoadGLTexture(&sceneTwoTexture_leaf, MAKEINTRESOURCE(IDBITMAP_LEAF)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for leaf");
		return -228;
	}

	if (LoadGLTexture(&sceneTwoTexture_trunk, MAKEINTRESOURCE(IDBITMAP_TRUNK)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for trunk");
		return -229;
	}

	if (LoadGLTexture(&sceneTwoTexture_base, MAKEINTRESOURCE(IDBITMAP_BASE)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for base");
		return -210;
	}

	if (LoadGLTexture(&sceneTwoTexture_mark, MAKEINTRESOURCE(IDBITMAP_MARK)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for mark");
		return -211;
	}

	if (LoadGLTexture(&sceneTwoTexture_pole, MAKEINTRESOURCE(IDBITMAP_POLE)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_pole");
		uninitialize();
		return -212;

	}

	if (LoadGLTexture(&sceneTwoTexture_stares, MAKEINTRESOURCE(IDBITMAP_STARES)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_stares");
		uninitialize();
		return -213;


	}

	if (LoadGLTexture(&sceneTwoTexture_top, MAKEINTRESOURCE(IDBITMAP_TOP)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_top");
		uninitialize();
		return -214;

	}


	if (LoadGLTexture(&sceneTwoTexture_upper, MAKEINTRESOURCE(IDBITMAP_UPPER)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_upper");
		uninitialize();
		return -215;

	}

	if (LoadGLTexture(&sceneTwoTexture_god, MAKEINTRESOURCE(IDBITMAP_GOD)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_god");
		uninitialize();
		return -216;

	}

	if (LoadGLTexture(&sceneTwoTexture_side, MAKEINTRESOURCE(IDBITMAP_SIDE)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_side");
		uninitialize();
		return -217;

	}

	if (LoadGLTexture((&sceneTwoTexture_water), MAKEINTRESOURCE(IDBITMAP_TEXTURE_WATER)) == FALSE) {
		return(-218);
	}

	if (NewLoadGLTexture(&sceneTwoTexture_Leaves, ".\\Scene2Textures\\lea2.png") == FALSE) {
		return -219;
	}


	if (LoadGLTexture(&sceneTwoTexture_woodbar, MAKEINTRESOURCE(IDBITMAP_WOOD_BAR_SCENE_TWO)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_woodbar");
		uninitialize();
		return -220;

	}

	if (LoadGLTexture(&sceneTwoTexture_bioscope, MAKEINTRESOURCE(IDBITMAP_POSTER_SCENE_TWO)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_bioscope");
		uninitialize();
		return -221;

	}

	if (LoadGLTexture(&sceneTwoTexture_bird_middle, MAKEINTRESOURCE(IDBITMAP_BIRD_MIDDLE)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_bird_middle");
		uninitialize();
		return -222;

	}

	if (LoadGLTexture(&villageBoard, MAKEINTRESOURCE(IDBITMAP_VILLAGE_BOARD)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for villageBoard");
		uninitialize();
		return -223;

	}

	if (NewLoadGLTexture(&texture_leady, ".\\Leady\\leady.png") == FALSE)
	{
		return -224;
	}

	// Scene 3
	if (LoadGLTexture(&sceneThreeTexture_Fence, MAKEINTRESOURCE(IDBITMAP_FENCE)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_kundali");
		uninitialize();
		return -36;

	}

	if (LoadGLTexture(&sceneThreeTexture_roof, MAKEINTRESOURCE(IDBITMAP_ROOF)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_roof");
		uninitialize();
		return -37;

	}

	if (LoadGLTexture(&sceneThreeTexture_room, MAKEINTRESOURCE(IDBITMAP_ROOM)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_room");
		uninitialize();
		return -37;

	}

	if (LoadGLTexture(&sceneThreeTexture_room1, MAKEINTRESOURCE(IDBITMAP_ROOM1)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_room1");
		uninitialize();
		return -38;

	}


	if (LoadGLTexture(&sceneThreeTexture_wall1, MAKEINTRESOURCE(IDBITMAP_HOUSE1_SIDE)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_wall1");
		uninitialize();
		return -39;

	}

	if (LoadGLTexture(&sceneThreeTexture_wall2, MAKEINTRESOURCE(IDBITMAP_HOUSE1_MAIN_DOOR)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_wall2");
		uninitialize();
		return -310;

	}


	if (LoadGLTexture(&sceneThreeTexture_h2s, MAKEINTRESOURCE(IDBITMAP_HOUSE2_SIDE)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_h2s");
		uninitialize();
		return -311;

	}

	if (LoadGLTexture(&sceneThreeTexture_h2md, MAKEINTRESOURCE(IDBITMAP_HOUSE2_MAIN_DOOR)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_h2md");
		uninitialize();
		return -312;

	}

	if (LoadGLTexture(&sceneThreeTexture_h3s, MAKEINTRESOURCE(IDBITMAP_HOUSE3_SIDE)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_h3md");
		uninitialize();
		return -313;

	}

	if (LoadGLTexture(&sceneThreeTexture_h3md, MAKEINTRESOURCE(IDBITMAP_HOUSE3_MAIN_DOOR)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_h3md");
		uninitialize();
		return -314;

	}


	if (LoadGLTexture(&sceneThreeTexture_h4s, MAKEINTRESOURCE(IDBITMAP_HOUSE4_SIDE)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_h4md");
		uninitialize();
		return -315;

	}

	if (LoadGLTexture(&sceneThreeTexture_h4md, MAKEINTRESOURCE(IDBITMAP_HOUSE4_MAIN_DOOR)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_h4md");
		uninitialize();
		return -316;

	}


	if (LoadGLTexture(&sceneThreeTexture_h5s, MAKEINTRESOURCE(IDBITMAP_HOUSE5_SIDE)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_h5s");
		uninitialize();
		return -317;

	}

	if (LoadGLTexture(&sceneThreeTexture_h5md, MAKEINTRESOURCE(IDBITMAP_HOUSE5_MAIN_DOOR)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_h5md");
		uninitialize();
		return -318;

	}

	if (LoadGLTexture(&sceneThreeTexture_h5sf, MAKEINTRESOURCE(IDBITMAP_HOUSE5_SIDE_FRONT)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_h5mf");
		uninitialize();
		return -319;

	}

	if (LoadGLTexture(&sceneThreeTexture_g1, MAKEINTRESOURCE(IDBITMAP_GROUND1)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_g1");
		uninitialize();
		return -319;

	}

	if (LoadGLTexture(&sceneThreeTexture_Fence, MAKEINTRESOURCE(IDBITMAP_FENCE)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_kundali");
		uninitialize();
		return -320;

	}

	if (LoadGLTexture(&sceneThreeTexture_roof, MAKEINTRESOURCE(IDBITMAP_ROOF)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_roof");
		uninitialize();
		return -321;

	}

	if (LoadGLTexture(&sceneThreeTexture_room, MAKEINTRESOURCE(IDBITMAP_ROOM)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_room");
		uninitialize();
		return -322;

	}

	if (LoadGLTexture(&sceneThreeTexture_room1, MAKEINTRESOURCE(IDBITMAP_ROOM1)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_room1");
		uninitialize();
		return -323;

	}


	if (LoadGLTexture(&sceneThreeTexture_sadi, MAKEINTRESOURCE(IDBITMAP_SADI)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_sadi");
		uninitialize();
		return -324;

	}

	if (LoadGLTexture(&sceneThreeTexture_sadi_lower, MAKEINTRESOURCE(IDBITMAP_SADI_LOWER)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_sadi_lower");
		uninitialize();
		return -325;

	}

	if (LoadGLTexture(&sceneThreeTexture_sadi_triangle, MAKEINTRESOURCE(IDBITMAP_SADI_TRIANGLE)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_sadi_triangle");
		uninitialize();
		return -326;

	}

	if (LoadGLTexture(&sceneThreeTexture_arm, MAKEINTRESOURCE(IDBITMAP_ARM)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_arm");
		uninitialize();
		return -327;

	}

	if (LoadGLTexture(&sceneThreeTexture_bars, MAKEINTRESOURCE(IDBITMAP_BARS)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_bars");
		uninitialize();
		return -328;

	}

	if (LoadGLTexture(&sceneThreeTexture_cart_base, MAKEINTRESOURCE(IDBITMAP_CART_BASE)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_cart_base");
		uninitialize();
		return -329;

	}

	if (LoadGLTexture(&sceneThreeTexture_bar, MAKEINTRESOURCE(IDBITMAP_BAR)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_bar");
		uninitialize();
		return -330;

	}

	if (LoadGLTexture(&sceneThreeTexture_trunk, MAKEINTRESOURCE(IDBITMAP_TRUNK)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for trunk");
		return -331;
	}

	if (LoadGLTexture(&sceneThreeTexture_base, MAKEINTRESOURCE(IDBITMAP_BASE)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for base");
		return -332;
	}

	if (NewLoadGLTexture(&sceneThreeTexture_Leaves, ".\\Tree\\lea2.png") == FALSE) {

		return -333;
	}

	if (LoadGLTexture(&sceneThreeTexture_right, MAKEINTRESOURCE(IDBITMAP_RIGHT)) == FALSE) {
		return -334;
	}
	if (LoadGLTexture(&sceneThreeTexture_top, MAKEINTRESOURCE(IDBITMAP_TOP)) == FALSE) {
		return -335;
	}
	if (LoadGLTexture(&sceneThreeTexture_front, MAKEINTRESOURCE(IDBITMAP_FRONT)) == FALSE) {
		return -336;
	}
	if (LoadGLTexture(&sceneThreeTexture_left, MAKEINTRESOURCE(IDBITMAP_LEFT)) == FALSE) {
		return -337;
	}
	if (LoadGLTexture(&sceneThreeTexture_bottom, MAKEINTRESOURCE(IDBITMAP_BOTTOM)) == FALSE) {
		return -338;
	}
	if (LoadGLTexture(&sceneThreeTexture_back, MAKEINTRESOURCE(IDBITMAP_BACK)) == FALSE) {
		return -339;
	}

	// Bus
	if (LoadGLTexture(&texture_bus_side, MAKEINTRESOURCE(IDBITMAP_BUS_SIDE)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_bus_side");
		uninitialize();
		return -29;

	}


	if (LoadGLTexture(&texture_tyre, MAKEINTRESOURCE(IDBITMAP_TYRE)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_tyre");
		uninitialize();
		return -30;

	}

	if (NewLoadGLTexture(&texture_bus_front, "..\\Bus\\bus_front.png") == FALSE) {
		return -31;
	}

	// Lights
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

	glEnable(GL_LIGHT0);

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);
	
	glEnable(GL_TEXTURE_2D);
	quadric = gluNewQuadric();
	genarateZ(40, 40);
	SetupRC(ghdc, "Battlesbridge Demo");

	//warmup resize call
	resize(WIN_WIDTH, WIN_HEIGHT);
		
	return 0;

}

void resize(int width, int height) {
	
	//variable declaration

	// code
	if (height == 0)			// to avoid "/ by 0" in future calls
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);																			
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f); 
}

// Scene 1
// sceneOne buildings
void building(int flag) {

	if (flag == 1) {

		glBindTexture(GL_TEXTURE_2D, texture_building1);

	}
	else if (flag == 2) {

		glBindTexture(GL_TEXTURE_2D, texture_building2);

	}
	else if (flag == 3) {

		glBindTexture(GL_TEXTURE_2D, texture_building3);

	}
	else if (flag == 4) {

		glBindTexture(GL_TEXTURE_2D, texture_building4);

	}
	else if (flag == 5) {

		glBindTexture(GL_TEXTURE_2D, texture_building5);

	}
	else if (flag == 6) {

		glBindTexture(GL_TEXTURE_2D, texture_building6);

	}
	else if (flag == 7) {

		glBindTexture(GL_TEXTURE_2D, texture_building1);

	} else if (flag == 8) {

		glBindTexture(GL_TEXTURE_2D, texture_building2);

	} else if (flag == 9) {

		glBindTexture(GL_TEXTURE_2D, texture_building3);

	} else if (flag == 10) {

		glBindTexture(GL_TEXTURE_2D, texture_building4);

	} else if (flag == 11) {

		glBindTexture(GL_TEXTURE_2D, texture_building5);

	}

	glBegin(GL_QUADS);
	//front face
	//glColor3f(1.0f, 0.0f, 0.0f);	

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();

	glBegin(GL_QUADS);
	//right face
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//back face
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//left face
	//cyan color
	//glColor3f(0.0f, 1.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
	glColor3f(192.0f / 255.0f, 192.0f / 255.0f, 192.0f / 255.0f);
	glBegin(GL_QUADS);
	//top surface
	//megeta color
	//glColor3f(1.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	//botttom surface
	//yellow color
	//glColor3f(1.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();
	glColor3f(1.0f, 1.0f, 1.0f);


}

void sceneOneGround(int flag) {

	//botttom surface

	if (flag == 1) {
		
		glBindTexture(GL_TEXTURE_2D, texture_cg1);
	
	} else if (flag == 2) {
	
		glBindTexture(GL_TEXTURE_2D, texture_cg2);

	}

	if (flag == 2) {

		glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, -1.0f);

		glTexCoord2f(3.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f);

		glTexCoord2f(3.0f, 1.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(1.0f, -1.0f, 1.0f);
		glEnd();
	} else {
		
		glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, -1.0f);

		glTexCoord2f(10.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f);

		glTexCoord2f(10.0f, 10.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);

		glTexCoord2f(0.0f, 10.0f);
		glVertex3f(1.0f, -1.0f, 1.0f);
		glEnd();
	}
	glBindTexture(GL_TEXTURE_2D, 0);

}

void sceneOneAllBuildings() {

	for (int i = 1; i < 10; i++) {

		glPushMatrix();
		glTranslatef(0.0f, 0.0f, -8.0f);

		if (i == 4) {
			
			glScalef(0.75f, 3.80f, 0.75f);
			glTranslatef(i * 3.0f, 0.0f, 0.0f);

		
		} else if (i == 5) {

			glScalef(0.75f, 2.50f, 0.75f);
			glTranslatef(i * 3.0f, 0.0f, 0.0f);


		} else if (i == 6) {

			glScalef(0.75f, 2.75f, 0.75f);
			glTranslatef(i * 3.0f, 0.0f, 0.0f);


		} else if (i == 7) {

			glScalef(0.75f, 1.75f, 0.75f);
			glTranslatef(i * 3.0f + 3.0f, 0.0f, 0.0f);


		} else if (i == 8) {

			glScalef(0.75f, 1.75f, 0.75f);
			glTranslatef(i * 3.0f + 6.0f, 0.0f, 0.0f);


		} else if (i == 9) {

			glScalef(0.75f, 1.75f, 0.75f);
			glTranslatef(i * 3.0f + 9.0f, 0.0f, 0.0f);


		} else if (i == 10) {

			glScalef(0.75f, 1.75f, 0.75f);
			glTranslatef(i * 3.0f + 12.0f, 0.0f, 0.0f);


		} 
		else {
			glScalef(0.75f, 1.40f, 0.75f);
			glTranslatef(i * 3.0f, 0.0f, 0.0f);
		}
		building(i);
		glPopMatrix();

	}

	if (sceneOnez > -12.27f && sceneOnez != -3.0f) {
		int j = 1;
		for (int i = 10; i > 0; i--, j++) {

			glPushMatrix();
			
			
			if (i == 7) {

				glScalef(0.75f, 3.80f, 0.75f);
				glTranslatef(i * 3.0f, 0.0f, 0.0f);



			}
			else if (i == 6) {

				glScalef(0.75f, 2.50f, 0.75f);
				glTranslatef(i * 3.0f, 0.0f, 0.0f);


			}
			else if (i == 4) {

				glScalef(0.75f, 2.75f, 0.75f);
				glTranslatef(i * 3.0f, 0.0f, 0.0f);


			}
			else if (i == 5) {

				glScalef(0.75f, 1.75f, 0.75f);
				glTranslatef(i * 3.0f, 0.0f, 0.0f);


			}
			else {
				glScalef(0.75f, 1.40f, 0.75f);
				glTranslatef(i * 3.0f, 0.0f, 0.0f);
			}
			//glScalef(0.75f, 1.40f, 0.75f);
			//glTranslatef(j * 3.0f, 0.0f, 0.0f);
			building(i);
			glPopMatrix();

		}
	}

}

void sceneOneQuad() {

	glBegin(GL_QUADS);

	//front face
	//glColor3f(1.0f, 0.0f, 0.0f);	

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	//right face
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//back face
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//left face
	//cyan color
	//glColor3f(0.0f, 1.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);


	//top surface
	//megeta color
	//glColor3f(1.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	//botttom surface
	//yellow color
	//glColor3f(1.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();
}


void sceneOnePointDebug() {

	glColor3f(1.0f, 0.0f, 0.0f);
	glPointSize(10.0f);
	glBegin(GL_POINTS);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glEnd();
	glColor3f(1.0f, 1.0f, 1.0f);

}

void sceneOneRightHand(int flag) {

	//right hand
	glPushMatrix();
	if(flag == 0)
		glBindTexture(GL_TEXTURE_2D, texture_shirt);
	if(flag == 3)
		glBindTexture(GL_TEXTURE_2D, texture_shirt);
	if (flag == 1)
		glBindTexture(GL_TEXTURE_2D, sceneOneTexture_shirt1);
	if (flag == 2)
		glBindTexture(GL_TEXTURE_2D, sceneOneTexture_shirt2);
	if (flag == 4)
		glBindTexture(GL_TEXTURE_2D, sceneOneTexture_shirt4);

	glScalef(0.10f, 0.10f, 0.10f);
	glTranslatef(-sceneOneHumanWidth * 0.99, sceneOneHumanMiddle * 0.95, 0.0f);
	//PointDebug();
	glPushMatrix();
	if (flag == 0)
		glRotatef((GLfloat)sceneOneShoulderR, 1.0f, 0.0f, 0.0f);
	if (flag == 1)
		glRotatef((GLfloat)30, -1.0f, 0.0f, 0.0f);
	if (flag == 2)
		glRotatef((GLfloat)20, -1.0f, 0.0f, 0.0f);
	if (flag == 3)
		glRotatef((GLfloat)10, -1.0f, 0.0f, 0.0f);
	if (flag == 4)
		glRotatef((GLfloat)50, -1.0f, 0.0f, 0.0f);

	glTranslatef(0.0, -1.0f, 0.0f);
	//PointDebug();

	glPushMatrix();
	glScalef(0.6f, 3.0f, 1.0f);
	// now draw the arm
	//quadric = gluNewQuadric();
	//glColor3f(0.5f, 0.35f, 0.05f);
	gluSphere(quadric, 0.4f, 10, 10);
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();
	glTranslatef(0.0, -1.0f, 0.0f);
	glPushMatrix();
	if (flag == 0)
		glRotatef((GLfloat)sceneOneElbowR, 1.0f, 0.0f, 0.0f);
	if(flag == 1)
		glRotatef((GLfloat)70, -1.0f, 0.0f, 0.0f);
	if (flag == 2)
		glRotatef((GLfloat)60, -1.0f, 0.0f, 0.0f);
	if (flag == 3)
		glRotatef((GLfloat)50, -1.0f, 0.0f, 0.0f);
	if (flag == 4)
		glRotatef((GLfloat)40, -1.0f, 0.0f, 0.0f);

	glTranslatef(0.0, -0.7f, 0.0f);
	//PointDebug();
	glPushMatrix();
	glScalef(0.4f, 2.0f, 0.85f);
	// now draw the arm
	//quadric = gluNewQuadric();
	//glColor3f(0.5f, 0.35f, 0.05f);
	glColor3f(0.78f, 0.58f, 0.38f);
	gluSphere(quadric, 0.38f, 10, 10);
	glColor3f(1.0f, 1.0f, 1.0f);

	glPopMatrix();

	//glRotatef((GLfloat)elbow, 1.0f, 0.0f, 0.0f);
	//glTranslatef(0.0, -1.0f, 0.0f);
	//PointDebug();
	glPopMatrix();
	glPopMatrix();
	glPopMatrix();

}


void sceneOneLeftHand(int flag) {

	//right hand
	if (flag == 0)
		glBindTexture(GL_TEXTURE_2D, texture_shirt);
	if (flag == 1)
		glBindTexture(GL_TEXTURE_2D, sceneOneTexture_shirt1);
	if (flag == 2)
		glBindTexture(GL_TEXTURE_2D, sceneOneTexture_shirt2);
	if (flag == 3)
		glBindTexture(GL_TEXTURE_2D, texture_shirt);
	if (flag == 4)
		glBindTexture(GL_TEXTURE_2D, sceneOneTexture_shirt4);

	glPushMatrix();
	glScalef(0.10f, 0.10f, 0.10f);
	glTranslatef(sceneOneHumanWidth * 0.99, sceneOneHumanMiddle * 0.95, 0.0f);
	//PointDebug();
	glPushMatrix();
	if (flag == 0)
		glRotatef((GLfloat)sceneOneShoulderL, 1.0f, 0.0f, 0.0f);
	if (flag == 1)
		glRotatef((GLfloat)10, -1.0f, 0.0f, 0.0f);
	if (flag == 2)
		glRotatef((GLfloat)20, -1.0f, 0.0f, 0.0f);

	glTranslatef(0.0, -1.0f, 0.0f);
	//PointDebug();

	glPushMatrix();
	glScalef(0.6f, 2.5f, 1.0f);
	// now draw the arm
	//quadric = gluNewQuadric();
	gluSphere(quadric, 0.5f, 10, 10);
	glBindTexture(GL_TEXTURE_2D, 0);
	glColor3f(1.0f, 1.0f, 1.0f);
	glColor3f(0.78f, 0.58f, 0.38f);
	glPopMatrix();
	glTranslatef(0.0, -1.0f, 0.0f);
	glPushMatrix();
	if (flag == 0)
		glRotatef((GLfloat)sceneOneElbowL, 1.0f, 0.0f, 0.0f);
	if (flag == 1)
		glRotatef((GLfloat)70, -1.0f, 0.0f, 0.0f);
	if (flag == 2)
		glRotatef((GLfloat)60, -1.0f, 0.0f, 0.0f);


	glTranslatef(0.0, -0.7f, 0.0f);
	//PointDebug();
	glPushMatrix();
	glScalef(0.4f, 2.0f, 0.85f);
	// now draw the arm
	//quadric = gluNewQuadric();
	glColor3f(0.78f, 0.58f, 0.38f);
	gluSphere(quadric, 0.38f, 10, 10);
	glColor3f(1.0f, 1.0f, 1.0f);
	glPopMatrix();

	//glRotatef((GLfloat)elbow, 1.0f, 0.0f, 0.0f);
	//glTranslatef(0.0, -1.0f, 0.0f);
	//PointDebug();

	glPopMatrix();
	glPopMatrix();
	glPopMatrix();

}

void sceneOneLeftLeg(int flag) {

	if(flag == 0)
		glBindTexture(GL_TEXTURE_2D, texture_pant);
	if(flag == 1)
		glBindTexture(GL_TEXTURE_2D, sceneOneTexture_pant1);
	if (flag == 2)
		glBindTexture(GL_TEXTURE_2D, sceneOneTexture_pant2);
	if (flag == 3)
		glBindTexture(GL_TEXTURE_2D, sceneOneTexture_pant3);
	if (flag == 4)
		glBindTexture(GL_TEXTURE_2D, sceneOneTexture_pant4);

	// Upper Part
	// left leg
	glPushMatrix();
	glTranslatef(-sceneOneHumanWidth / 2, -0.2f, 0.2f);
	glPushMatrix();
	
	if (flag == 0)
		glRotatef((GLfloat)sceneOneUpperLeg, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.0, -sceneOneHumanMiddle / 2, -0.2f);
	//PointDebug();
	glPushMatrix();

	glRotatef(270.0f, 1.0f, 0.0f, 0.0f);
	gluQuadricTexture(quadric, GL_TRUE);
	gluCylinder(quadric, 0.4f, 0.4f, sceneOneHumanMiddle / 2 + 0.4f, 20, 20);
	glPopMatrix();
	glPushMatrix();

	// lower leg
	if(flag == 0)
		glRotatef((GLfloat)sceneOneLowerLeg, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.0, -sceneOneHumanMiddle / 2 + 0.2f, 0.0f);
	glRotatef(270.0f, 1.0f, 0.0f, 0.0f);
	//gluQuadricTexture(quadric, GL_TRUE);
	gluCylinder(quadric, 0.4f, 0.4f, sceneOneHumanMiddle / 2, 20, 20);
	glPushMatrix();
	//PointDebug();
	glTranslatef(0.0f, -0.2f, 0.0f);
	glScalef(0.40f, 0.60f, 0.25f);
	glBindTexture(GL_TEXTURE_2D, 0);
	if (flag == 2) {
		glColor3f(136.0f / 255.0f, 54.0f / 255.0f, 0.0f);
	} else if (flag == 4) {
		glColor3f(0.0f, 0.0f, 0.0f);
	} if (flag == 0) {
		glColor3f(162.0f / 255.0f, 65.0f / 255.0f, 0.0f);
		//glColor3f(136.0f / 255.0f, 54.0f / 255.0f, 0.0f);
	}

	gluSphere(quadric, 1.0, 50, 50);
	glColor3f(1.0f, 1.0f, 1.0f);
	//quad();
	glPopMatrix();


	glPopMatrix();
	glPopMatrix();
	glPopMatrix();

	glBindTexture(GL_TEXTURE_2D, 0);

}

void sceneOneRightLeg(int flag) {

	if(flag == 0)
		glBindTexture(GL_TEXTURE_2D, texture_pant);
	if(flag == 1)
		glBindTexture(GL_TEXTURE_2D, sceneOneTexture_pant1);
	if (flag == 2)
		glBindTexture(GL_TEXTURE_2D, sceneOneTexture_pant2);
	if (flag == 3)
		glBindTexture(GL_TEXTURE_2D, sceneOneTexture_pant3);
	if (flag == 4)
		glBindTexture(GL_TEXTURE_2D, sceneOneTexture_pant4);

	// right leg
	glPushMatrix();
	//glScalef(0.10f, 0.10f, 0.10f);
	glTranslatef(sceneOneHumanWidth / 2, -0.2f, 0.2f);
	//PointDebug();
	glPushMatrix();

	if (flag == 0)
		glRotatef((GLfloat)sceneOneUpperLeg, -1.0f, 0.0f, 0.0f);
	glTranslatef(0.0, -sceneOneHumanMiddle / 2, -0.2f);
	//PointDebug();
	glPushMatrix();

	glRotatef(270.0f, 1.0f, 0.0f, 0.0f);
	gluQuadricTexture(quadric, GL_TRUE);
	gluCylinder(quadric, 0.4f, 0.4f, sceneOneHumanMiddle / 2 + 0.4f, 20, 20);
	glPopMatrix();
	glPushMatrix();

	// lower leg
	if (flag == 0)
		glRotatef((GLfloat)sceneOneLowerLeg, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.0, -sceneOneHumanMiddle / 2 + 0.2f, 0.0f);
	glRotatef(270.0f, 1.0f, 0.0f, 0.0f);
	gluQuadricTexture(quadric, GL_TRUE);
	gluCylinder(quadric, 0.4f, 0.4f, sceneOneHumanMiddle / 2 + 0.2f, 20, 20);
	glPushMatrix();
	//PointDebug();
	glTranslatef(0.0f, -0.2f, 0.0f);
	glScalef(0.40f, 0.60f, 0.25f);
	glBindTexture(GL_TEXTURE_2D, 0);
	if (flag == 2) {
		glColor3f(136.0f / 255.0f, 54.0f / 255.0f, 0.0f);
	} else if (flag == 4) {
		glColor3f(0.0f, 0.0f, 0.0f);
	} if (flag == 0) {
		glColor3f(162.0f / 255.0f, 65.0f / 255.0f, 0.0f);
	}
	gluSphere(quadric, 1.0, 50, 50);
	glColor3f(1.0f, 1.0f, 1.0f);
	glPopMatrix();

	glPopMatrix();
	glPopMatrix();
	glPopMatrix();
	glPopMatrix();

	glBindTexture(GL_TEXTURE_2D, 0);
}

float sceneOneBendAngle = 0.0f;

void sceneOneMiddleBody(int flag) {

	//humanWidth = humanWidth - 0.2f;
	if(flag == 0)
		glBindTexture(GL_TEXTURE_2D, texture_shirt);
	if(flag == 1)
		glBindTexture(GL_TEXTURE_2D, sceneOneTexture_shirt1);
	if (flag == 2)
		glBindTexture(GL_TEXTURE_2D, sceneOneTexture_shirt2);
	if (flag == 3)
		glBindTexture(GL_TEXTURE_2D, sceneOneTexture_shirt3);
	if (flag == 4)
		glBindTexture(GL_TEXTURE_2D, sceneOneTexture_shirt4);


	if(sceneOneBendFlag == 1 && flag == 0)
		glRotatef(sceneOneBendAngle, 1.0f, 0.0f, 0.0f);

	// Middle Body
	glPushMatrix();
	glScalef(0.10f, 0.10f, 0.10f);
	glPushMatrix();

	glRotatef(270.0f, 1.0f, 0.0f, 0.0f);
	/*glScalef(0.65f, 2.25f, 1.0);
	gluSphere(quadric, 2.0f, 50, 50);*/
	//gluQuadricTexture(quadric, GL_TRUE);
	gluCylinder(quadric, sceneOneHumanWidth - 0.2f, sceneOneHumanWidth - 0.2f, sceneOneHumanMiddle, 50, 50);
	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	//gluSphere(quadric, 0.5f, 50, 50);
	glPopMatrix();
	
	if (sceneOneBendFlag == 1 && flag == 0)
		glRotatef(sceneOneBendAngle, -1.0f, 0.0f, 0.0f);

	glBindTexture(GL_TEXTURE_2D, 0);

}

void sceneOneNeck() {

	glPushMatrix();
	glColor3f(0.78f, 0.58f, 0.38f);
	glColor3f(255.0f/ 255.0f, 180.0f / 255.0f, 130.0f / 255.0f);
	glScalef(0.10f, 0.10f, 0.10f);
	glTranslatef(0.0f, sceneOneHumanMiddle, 0.0);
	//PointDebug();
	glRotatef(270.0f, 1.0f, 0.0f, 0.0f);
	gluCylinder(quadric, sceneOneHumanWidth * 0.20, sceneOneHumanWidth * 0.20, sceneOneHumanMiddle * 0.15, 50, 50);
	glPopMatrix();
	glColor3f(1.0f, 1.0f, 1.0f);
}

void sceneOneHead() {

	glBindTexture(GL_TEXTURE_2D, texture_head);
	glPushMatrix();
	glScalef(0.10f, 0.10f, 0.10f);
	glTranslatef(0.0f, sceneOneHumanMiddle + sceneOneHumanMiddle * 0.40, 0.0);

	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glRotatef(90.0f, 0.0f, -1.0f, 0.0f);
	gluQuadricTexture(quadric, GL_TRUE);
	gluSphere(quadric, sceneOneHumanWidth * 0.70, 50, 50);
	glBindTexture(GL_TEXTURE_2D, 0);
	//PointDebug();
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

}

void sceneOneWholeBody(int flag) {

	sceneOneMiddleBody(flag);
	sceneOneLeftLeg(flag);
	sceneOneRightLeg(flag);
	sceneOneRightHand(flag);
	sceneOneLeftHand(flag);
	sceneOneNeck();
	sceneOneHead();

}

// bioscope
void bioScope() {

	glColor3f(1.0f, 1.0f, 1.0f);
	
	glBindTexture(GL_TEXTURE_2D, sceneOneTexture_bioscope);
	glPushMatrix();
	glScalef(1.00f, 0.50f, 0.50f);
	// main box
	sceneOneQuad();

	glPopMatrix();
	glPushMatrix();

	// legs (left )
	glColor3f(1.0f, 1.0f, 1.0f);
	glTranslatef(0.0f, -1.00f, 0.0f);
	glTranslatef(0.04f, 0.0f, 0.0f);
	glRotatef(55.0f, 0.0f, 0.0f, 1.0f);

	glScalef(0.05, 1.20, 0.05);
	sceneOneQuad();
	glPopMatrix();
	glPushMatrix();

	// legs (right )
	glColor3f(1.0f, 1.0f, 1.0f);
	glTranslatef(0.0f, -1.00f, 0.0f);
	glTranslatef(-0.02f, 0.0f, 0.0f);
	glRotatef(55.0f, 0.0f, 0.0f, -1.0f);

	glScalef(0.05, 1.20, 0.05);
	sceneOneQuad();
	glPopMatrix();

	// Sphere
	glBindTexture(GL_TEXTURE_2D, 0);
	glPushMatrix();
	//glColor3f(218 / 255, 165 / 255, 32 / 255);
	//glColor3f(205.0f / 255.0, 133.0f / 255.0f, 63.0f / 255.0f);
	glColor3f(169.0 / 255.0, 169.0 / 255.0f, 169.0 / 255.0f);
	glTranslatef(-0.5f, 0.0f, 0.40f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	gluSphere(quadric, 0.20f, 30, 30);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.5f, 0.0f, 0.40f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	gluSphere(quadric, 0.20f, 30, 30);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-1.0f, 0.0f, 0.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	gluSphere(quadric, 0.20f, 30, 30);
	glPopMatrix();
	glColor3f(1.0f, 1.0f, 1.0f);

	// Inner sphere

	glPushMatrix();
	glColor3f(0.0f, 0.0f, 0.0f);
	glTranslatef(-0.495f, 0.0f, 0.50f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	gluSphere(quadric, 0.13f, 30, 30);
	glPopMatrix();

	glPushMatrix();

	glTranslatef(0.495f, 0.0f, 0.50f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	gluSphere(quadric, 0.13f, 30, 30);
	glPopMatrix();


	glColor3f(1.0f, 1.0f, 1.0f);
}


void sceneOnePoster_front() {

	glBegin(GL_QUADS);

	//front face
	//glColor3f(1.0f, 0.0f, 0.0f);	

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glEnd();
}

void sceneOnePoster_wood_bar() {

	glBegin(GL_QUADS);

	//front face
	//glColor3f(1.0f, 0.0f, 0.0f);	

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);


	//right face
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//back face
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//left face
	//cyan color
	//glColor3f(0.0f, 1.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);


	//top surface
	//megeta color
	//glColor3f(1.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	//botttom surface
	//yellow color
	//glColor3f(1.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();

}

void sceneOneBioscope_poster() {
	
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, sceneOneTexture_bioscope_poster);
	glScalef(0.9f, 0.35f, 0.0f);
	sceneOnePoster_front();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.9f, -0.5f, 0.0f);
	glScalef(0.05, 1.0f, 0.05f);
	glBindTexture(GL_TEXTURE_2D, sceneOneTexture_woodbar);
	sceneOnePoster_wood_bar();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-0.9f, -0.5f, 0.0f);
	glScalef(0.05, 1.0f, 0.05f);
	glBindTexture(GL_TEXTURE_2D, sceneOneTexture_woodbar);
	sceneOnePoster_wood_bar();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

}

float busX = 20.0f;
float busY = -0.7f;
float busZ = -1.0f;

void bus() {

	glBindTexture(GL_TEXTURE_2D, texture_bus_side);
	glBegin(GL_QUADS);

	//front face
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

	glBindTexture(GL_TEXTURE_2D, texture_bus_side);
	glBegin(GL_QUADS);
	//right face
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);


	glBindTexture(GL_TEXTURE_2D, texture_bus_side);
	glBegin(GL_QUADS);
	//back face
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

	/*glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 1.0f);*/
	glBindTexture(GL_TEXTURE_2D, texture_bus_front);
	glBegin(GL_QUADS);
	//left face
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glEnd();

	//glDisable(GL_ALPHA_TEST);
	glBindTexture(GL_TEXTURE_2D, 0);

	glBegin(GL_QUADS);
	//top surface
	//megeta color
	//glColor3f(1.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glEnd();

	glBegin(GL_QUADS);
	//botttom surface
	//yellow color
	//glColor3f(1.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();

}

void wholeBus() {

	// Bus main part
	glTranslatef(busX, busY, busZ);
	glScalef(1.25f, 0.50f, 0.60f);
	glPushMatrix();
	//glBindTexture(GL_TEXTURE_2D, texture_kundali);
	bus();
	glPopMatrix();

	// Tyre
	// front 1 right
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, texture_tyre);
	glTranslatef(-0.65f, -0.95f, 0.50f);
	glScalef(0.35f, 0.35f, 1.0f);
	gluQuadricTexture(quadric, texture_tyre);
	gluCylinder(quadric, 1.0f, 1.0f, 0.2f, 40, 40);
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	// front 2 left
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, texture_tyre);
	glTranslatef(-0.65f, -0.95f, -0.80f);
	glScalef(0.35f, 0.35f, 1.0f);
	gluQuadricTexture(quadric, texture_tyre);
	gluCylinder(quadric, 1.0f, 1.0f, 0.2f, 40, 40);
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	// back right
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, texture_tyre);
	glTranslatef(0.65f, -0.95f, 0.50f);
	glScalef(0.35f, 0.35f, 1.0f);
	gluQuadricTexture(quadric, texture_tyre);
	gluCylinder(quadric, 1.0f, 1.0f, 0.2f, 40, 40);
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	// black left
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, texture_tyre);
	glTranslatef(0.65f, -0.95f, -0.80f);
	glScalef(0.35f, 0.35f, 1.0f);
	gluQuadricTexture(quadric, texture_tyre);
	gluCylinder(quadric, 1.0f, 1.0f, 0.2f, 40, 40);
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

}

void image() {

	glBegin(GL_QUADS);

	//front face
	//glColor3f(1.0f, 0.0f, 0.0f);	

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glEnd();
}

float sceneTwoImagex = 3.0f;
float sceneTwoImagey = 0.0f;
float sceneTwoImagez = 0.0f;

float sceneReturnImagex = 3.0f;
float sceneReturnImagey = 0.0f;
float sceneReturnImagez = 0.0f;

void sceneTwoRoleDisplay() {
	
	glTranslatef(sceneTwoImagex, sceneTwoImagey, sceneTwoImagez);
	glPushMatrix();

	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, texture_image);
	image();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, texture_role1);
	glTranslatef(2.0f, 0.0f, 0.0f);
	image();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(4.05f, 0.0f, 0.0f);
	glBindTexture(GL_TEXTURE_2D, texture_role2);
	image();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(6.05f, 0.0f, 0.0f);
	glBindTexture(GL_TEXTURE_2D, texture_image);
	image();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();
	glPopMatrix();

}

void sceneReturnRoleDisplay() {
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glTranslatef(sceneReturnImagex, sceneReturnImagey, sceneReturnImagez);
	
	glPushMatrix();

	glPushMatrix();
	glScalef(1.20f, 1.0f, 0.0f);
	glBindTexture(GL_TEXTURE_2D, texture_role3);
	image();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	glPushMatrix();
	glScalef(1.20f, 1.0f, 0.0f);
	glBindTexture(GL_TEXTURE_2D, texture_role4);
	glTranslatef(2.02f, 0.0f, 0.0f);
	image();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	glPushMatrix();
	glScalef(1.20f, 1.0f, 0.0f);
	glTranslatef(4.05f, 0.0f, 0.0f);
	glBindTexture(GL_TEXTURE_2D, texture_role5);
	image();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();
	
	glPopMatrix();

}

void sceneOneDisplay() {
	
	// variables
	//main room
	//glClearColor(135.0f / 255.0f, 206.0 / 255.0f, 235.0 / 255.0f, 1.0f);
	
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, texture_sky);
	gluQuadricTexture(quadric, GL_TRUE);
	glScalef(2.50f, 2.50f, 2.50f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	gluSphere(quadric, 10.0f, 100, 100);
	glBindTexture(GL_TEXTURE_2D, 0);
	glRotatef(90.0f, -1.0f, 0.0f, 0.0f);
	glPopMatrix();
	int flag = 0;				// for home texture
	float width1 = 1.25;
	float height1 = 0.65f;
	float zLength1 = 1.0f;


	gluLookAt(sceneOnex, sceneOney, sceneOnez,
		sceneOnex, sceneOney, sceneOnez - 10.0f,
		0.0f, 1.0f, 0.0f);


	fprintf(gpFile, "%f %f %f\n", sceneOnex, sceneOney, sceneOnez);

	if (sceneOnez > -12.25f && sceneOneZFLag == 0) {

		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);


	}
	else {

		sceneOneZFLag = 1;

		sceneOnex = 5.0f;
		sceneOney = -0.1f;
		sceneOnez = -3.0f;

		gluLookAt(sceneOnex, sceneOney, sceneOnez,
			sceneOnex, sceneOney, sceneOnez - 10.0f,
			0.0f, 1.0f, 0.0f);


	}
	sceneOneAllBuildings();

	glPushMatrix();
	glTranslatef(8.0f, 2.57f, -5.0f);
	glScalef(40.0f, 4.0f, 40.0f);
	sceneOneGround(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(8.0f, 2.6f, -4.0f);
	glScalef(40.0f, 4.0f, 2.0f);
	sceneOneGround(2);
	glPopMatrix();

	// humaoid
	glPushMatrix();
	// rotate whole body
	glTranslatef(12.0f, 0.0f, 0.0f);
	glTranslatef(0.0f, -1.05f, -5.0f);
	glScalef(0.70f, 0.70f, 0.70f);
	glTranslatef(sceneOneFullBodyMove, 0.0f, sceneOneZBodyMove);
	glRotatef(sceneOneWholeBodyAngle, 0.0f, 1.0f, 0.0f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	sceneOneWholeBody(0);
	glPopMatrix();

	glPushMatrix();

	glTranslatef(0.0f, 0.0f, -2.0f);
	// child 1
	glPushMatrix();
	// rotate whole body
	glTranslatef(15.0f, -1.15f, -3.5f);
	glScalef(0.60f, 0.60f, 0.60f);
	glRotatef(45, 0.0f, 1.0f, 0.0f);
	//glTranslatef(fullBodyMove, 0.0f, 0.0f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	sceneOneWholeBody(1);
	glPopMatrix();


	// child 2
	glPushMatrix();
	// rotate whole body
	glTranslatef(16.0f, -1.15f, -4.5f);
	glScalef(0.60f, 0.60f, 0.60f);
	glRotatef(200, 0.0f, 1.0f, 0.0f);
	//glTranslatef(fullBodyMove, 0.0f, 0.0f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	sceneOneWholeBody(2);
	glPopMatrix();

	// child 3
	glPushMatrix();
	// rotate whole body
	glTranslatef(15.9f, -1.15f, -3.5f);
	glScalef(0.60f, 0.60f, 0.60f);
	glRotatef(120, 0.0f, 1.0f, 0.0f);
	//glTranslatef(fullBodyMove, 0.0f, 0.0f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	sceneOneWholeBody(3);
	glPopMatrix();


	// child 4
	glPushMatrix();
	// rotate whole body
	glTranslatef(15.9f, -1.15f, -5.0f);
	glScalef(0.60f, 0.60f, 0.60f);
	glRotatef(220, 0.0f, 1.0f, 0.0f);
	//glTranslatef(fullBodyMove, 0.0f, 0.0f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	sceneOneWholeBody(4);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(15.5f, -0.9f, -4.2f);
	glScalef(0.20f, 0.25f, 0.20f);
	glRotatef(10.0f, 0.0f, 1.0f, 0.0f);
	bioScope();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(15.5f, -0.5f, -6.0f);
	glScalef(0.55f, 0.75f, 0.75f);
	sceneOneBioscope_poster();
	glPopMatrix();

	glPushMatrix();
	if (busX >= -11.0f)
		wholeBus();
	glPopMatrix();

	glPopMatrix();

}

void scenesEnd() {
	// variables
		//main room
		//glClearColor(135.0f / 255.0f, 206.0 / 255.0f, 235.0 / 255.0f, 1.0f);

	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, texture_sky);
	gluQuadricTexture(quadric, GL_TRUE);
	glScalef(2.50f, 2.50f, 2.50f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	gluSphere(quadric, 10.0f, 100, 100);
	glBindTexture(GL_TEXTURE_2D, 0);
	glRotatef(90.0f, -1.0f, 0.0f, 0.0f);
	glPopMatrix();
	int flag = 0;				// for home texture
	float width1 = 1.25;
	float height1 = 0.65f;
	float zLength1 = 1.0f;


	gluLookAt(sceneOnex, sceneOney, sceneOnez,
		sceneOnex, sceneOney, sceneOnez - 10.0f,
		0.0f, 1.0f, 0.0f);


	fprintf(gpFile, "%f %f %f\n", sceneOnex, sceneOney, sceneOnez);

	if (sceneOnez > -12.25f && sceneOneZFLag == 0) {

		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);


	}
	else {

		sceneOneZFLag = 1;

		sceneOnex = 5.0f;
		sceneOney = -0.1f;
		sceneOnez = -3.0f;

		gluLookAt(sceneOnex, sceneOney, sceneOnez,
			sceneOnex, sceneOney, sceneOnez - 10.0f,
			0.0f, 1.0f, 0.0f);


	}
	sceneOneAllBuildings();

	glPushMatrix();
	glTranslatef(8.0f, 2.57f, -5.0f);
	glScalef(40.0f, 4.0f, 40.0f);
	sceneOneGround(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(8.0f, 2.6f, -4.0f);
	glScalef(40.0f, 4.0f, 2.0f);
	sceneOneGround(2);
	glPopMatrix();

	// humaoid
	glPushMatrix();
	// rotate whole body
	glTranslatef(12.0f, 0.0f, 0.0f);
	glTranslatef(0.0f, -1.05f, -5.0f);
	glScalef(0.70f, 0.70f, 0.70f);
	glTranslatef(sceneOneFullBodyMove, 0.0f, sceneOneZBodyMove);
	glRotatef(sceneOneWholeBodyAngle, 0.0f, 1.0f, 0.0f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	sceneOneWholeBody(0);
	glPopMatrix();

	glPushMatrix();

	glTranslatef(0.0f, 0.0f, -2.0f);
	// child 1
	glPushMatrix();
	// rotate whole body
	glTranslatef(15.0f, -1.15f, -3.5f);
	glScalef(0.60f, 0.60f, 0.60f);
	glRotatef(45, 0.0f, 1.0f, 0.0f);
	//glTranslatef(fullBodyMove, 0.0f, 0.0f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	sceneOneWholeBody(1);
	glPopMatrix();


	// child 2
	glPushMatrix();
	// rotate whole body
	glTranslatef(16.0f, -1.15f, -4.5f);
	glScalef(0.60f, 0.60f, 0.60f);
	glRotatef(200, 0.0f, 1.0f, 0.0f);
	//glTranslatef(fullBodyMove, 0.0f, 0.0f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	sceneOneWholeBody(2);
	glPopMatrix();

	// child 3
	glPushMatrix();
	// rotate whole body
	glTranslatef(15.9f, -1.15f, -3.5f);
	glScalef(0.60f, 0.60f, 0.60f);
	glRotatef(120, 0.0f, 1.0f, 0.0f);
	//glTranslatef(fullBodyMove, 0.0f, 0.0f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	sceneOneWholeBody(3);
	glPopMatrix();


	// child 4
	glPushMatrix();
	// rotate whole body
	glTranslatef(15.9f, -1.15f, -5.0f);
	glScalef(0.60f, 0.60f, 0.60f);
	glRotatef(220, 0.0f, 1.0f, 0.0f);
	//glTranslatef(fullBodyMove, 0.0f, 0.0f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	sceneOneWholeBody(4);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(15.5f, -0.9f, -4.2f);
	glScalef(0.20f, 0.25f, 0.20f);
	glRotatef(10.0f, 0.0f, 1.0f, 0.0f);
	bioScope();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(15.5f, -0.5f, -6.0f);
	glScalef(0.55f, 0.75f, 0.75f);
	sceneOneBioscope_poster();
	glPopMatrix();

	glPushMatrix();
	if (busX >= -11.0f)
		wholeBus();
	glPopMatrix();

	glPopMatrix();

}



// Scene 2
void sceneTwoGround(int flag) {

	//botttom surface

	if (flag == 1) {

		glBindTexture(GL_TEXTURE_2D, sceneTwoTexture_road);


	}
	else if (flag == 2) {

		glBindTexture(GL_TEXTURE_2D, sceneTwoTexture_grass);
		//glBindTexture(GL_TEXTURE_2D, texture_cg2);
	}

	if (flag == 1) {

		glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, -1.0f);

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(1.0f, -1.0f, 1.0f);
		glEnd();
	}
	else {

		glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, -1.0f);

		glTexCoord2f(10.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f);

		glTexCoord2f(10.0f, 10.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);

		glTexCoord2f(0.0f, 10.0f);
		glVertex3f(1.0f, -1.0f, 1.0f);
		glEnd();
	}
	glBindTexture(GL_TEXTURE_2D, 0);

}

void sceneTwoPointDebug() {

	glColor3f(1.0f, 0.0f, 0.0f);
	glPointSize(10.0f);
	glBegin(GL_POINTS);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glEnd();
	glColor3f(1.0f, 1.0f, 1.0f);

}

float sceneTwoScaleValue = 0.08f;
void sceneTwoDisplayQuad() {

	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0.9f);
	glBindTexture(GL_TEXTURE_2D, sceneTwoTexture_Leaves);

	glRotatef(40.0f, 0.0f, 1.0f, 0.0f);
	glScalef(sceneTwoScaleValue, sceneTwoScaleValue, sceneTwoScaleValue);
	glBegin(GL_QUADS);
	//glColor3f(1.0f, 0.0f, 0.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 0.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);

	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);

	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);

	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);

}
void sceneTwoBranch(float height, float redius) {

	// main code to draw 
	///////////////////////////////////////////////////////////////////////////////

	// flag == 0 for first trunk drawing
	glBindTexture(GL_TEXTURE_2D, sceneTwoTexture_trunk);
	gluQuadricTexture(quadric, GL_TRUE);

	if (sceneTwoFlagTree == 0) {

		gluCylinder(quadric, redius * (1 + sceneTwoRediusReduce) * 1.50, redius * (1 + sceneTwoRediusReduce), height, 5, 5);
		sceneTwoFlagTree = 1;

		// flag != 0 for branches
	}
	else {

		gluCylinder(quadric, redius * (1 + sceneTwoRediusReduce), redius * 0.75, height, 5, 5);
	}

	///////////////////////////////////////////////////////////////////////////////

	glTranslatef(0.0f, 0.0f, height);
	glPushMatrix();

	if (height > sceneTwoHeightLimit) {

		glRotatef(45.0f, 0.0f, 1.0f, 0.0f);
		if (height > 0.5)
			sceneTwoBranch(height * 0.67f, redius);
		else
			sceneTwoBranch(height * 0.67f, redius * sceneTwoRediusReduce);


		glPopMatrix();
		glPushMatrix();

		glRotatef(45.0f, 0.0f, -1.0f, 0.0f);
		if (height > 0.5)
			sceneTwoBranch(height * 0.67f, redius);
		else
			sceneTwoBranch(height * 0.67f, redius * sceneTwoRediusReduce);

		glPopMatrix();
		glPushMatrix();


		glRotatef(45.0f, 1.0f, 0.0f, 0.0f);
		if (height > 0.5)
			sceneTwoBranch(height * 0.67f, redius);
		else
			sceneTwoBranch(height * 0.67f, redius * sceneTwoRediusReduce);


		glPopMatrix();
		glPushMatrix();

		glRotatef(45.0f, -1.0f, 0.0f, 0.0f);
		if (height > 0.5)
			sceneTwoBranch(height * 0.67f, redius);
		else
			sceneTwoBranch(height * 0.67f, redius * sceneTwoRediusReduce);

		glPopMatrix();

	}
	else if (height < sceneTwoHeightLimit && height > 0.1f) {
		glColor3f(0.0, 0.7f, 0.0f);
		sceneTwoDisplayQuad();
		glColor3f(1.0, 1.0f, 1.0f);

		glPopMatrix();

	}

	glBindTexture(GL_TEXTURE_2D, 0);

}

void sceneTwoCallToBranch(float x, float y, float z) {

	glPushMatrix();
	float redius = 0.04f;
	sceneTwoFlagTree = 0;

	glTranslatef(x, y, z);
	glRotatef(45.0, 0.0f, 1.0f, 0.0f);

	glRotatef(270.0f, 1.0f, 0.0f, 0.0f);
	sceneTwoBranch(sceneTwoHeight, redius);
	glPopMatrix();
}

void sceneTwoCircle(float redius) {

	glBegin(GL_POLYGON);
	for (float i = 0; i < 360; i += 0.5f) {

		glVertex3f(redius * cos(i), redius * sin(i), 0.0f);

	}
	glEnd();

}
void sceneTwoBase(float b) {

	//glColor3f(0.7f, 0.7f, 0.7f);
	glPushMatrix();
	glRotatef(270.0f, 1.0f, 0.0f, 0.0f);

	glBindTexture(GL_TEXTURE_2D, sceneTwoTexture_base);
	gluCylinder(quadric, b, b, 0.3f, 50, 50);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glColor3f(0.7f, 0.7f, 0.7f);
	glPushMatrix();
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	sceneTwoCircle(0.5f);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 0.3f, 0.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	sceneTwoCircle(0.5f);
	glPopMatrix();


	glColor3f(1.0f, 1.0f, 1.0f);

}

//struct Node* head = NULL;

void useLinkedList(float b) {
	
	addNode(-2.5f, 0.5f - b, 15.0f);
	addNode(2.5f, 0.5f - b, 0.0f);
	//addNode(0.0f, -(b * 0.60), 0.0f);
	addNode(-2.5f, 0.5f - b, -15.0f);
	//addNode(2.5f, 0.5f - b, -25.0f);

}


void sceneTwoTrees(float b) {

	// variables
	// Use linked list to store trees postion
	useLinkedList(b);
	int index = 1;
	struct Node* temp = NULL;

	// 1 tree
	glPushMatrix();
	temp = getNode(index);
	index++;
	//glTranslatef(-2.5f, 0.5f - b, 15.0f);
	glTranslatef(temp-> x , temp-> y, temp-> z);
	glScalef(1.5f, 1.5f, 1.5f);
	glPushMatrix();
	sceneTwoCallToBranch(0.0f, 0.0f, 0.0f);
	glPopMatrix();

	glPushMatrix();
	//PointDebug();
	glTranslatef(0.0f, -(b * 0.60), 0.0f);
	//base(b);
	glPopMatrix();
	glPopMatrix();

	// 2 tree
	glPushMatrix();
	temp = getNode(index);
	index++;
	//glTranslatef(2.5f, 0.5f - b, 0.0f);
	glTranslatef(temp->x, temp->y, temp->z);
	glScalef(1.5f, 1.5f, 1.5f);
	glPushMatrix();
	sceneTwoCallToBranch(0.0f, 0.0f, 0.0f);
	glPopMatrix();

	glPushMatrix();
	//PointDebug();
	glTranslatef(0.0f, -(b * 0.60), 0.0f);
	//base(b);
	glPopMatrix();
	glPopMatrix();

	// 3 tree
	glPushMatrix();
	temp = getNode(index);
	index++;
	//glTranslatef(-2.5f, 0.5f - b, -15.0f);
	glTranslatef(temp->x, temp->y, temp->z);
	glScalef(1.5f, 1.5f, 1.5f);
	glPushMatrix();
	sceneTwoCallToBranch(0.0f, 0.0f, 0.0f);
	glPopMatrix();

	glPushMatrix();
	//PointDebug();
	glTranslatef(0.0f, -(b * 0.60), 0.0f);
	//base(b);
	glPopMatrix();
	glPopMatrix();

	glPopMatrix();
}

void sceneTwoCube(int flag) {


	if (flag == 0) {

		glBindTexture(GL_TEXTURE_2D, sceneTwoTexture_mark);

	}
	glBegin(GL_QUADS);

	//front face
	//glColor3f(1.0f, 0.0f, 0.0f);	

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

	glBegin(GL_QUADS);

	//right face
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//back face
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//left face
	//cyan color
	//glColor3f(0.0f, 1.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);


	//top surface
	//megeta color
	//glColor3f(1.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	//botttom surface
	//yellow color
	//glColor3f(1.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();

}
void sceneTwoMark() {

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	GLdouble mark[4] = { 0.0f, 0.0f, -1.0f, 0.0f };

	glTranslatef(0.0f, 0.0f, -9.0f);
	glPushMatrix();
	glPushMatrix();
	glTranslatef(1.0f, 0.3f, 0.0f);
	glScalef(0.20, 0.25, 0.10);
	sceneTwoCube(0);
	glPopMatrix();

	glTranslatef(1.0f, 0.55f, 0.0f);
	glScalef(0.40f, 0.60f, 0.30f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	glPushMatrix();
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glEnable(GL_CLIP_PLANE1);
	glClipPlane(GL_CLIP_PLANE1, mark);
	glColor3f(1.0f, 1.0f, 0.0f);
	gluSphere(quadric, 0.5f, 30, 30);
	glColor3f(1.0f, 1.0f, 1.0f);

	glDisable(GL_CLIP_PLANE1);

	glPopMatrix();
	glPopMatrix();

}

// tree 2D
float sceneTwoHeight2D = 0.5f;
float sceneTwoAngle2D = 45.0f;
float sceneTwoLineWidth2D = 7.0f;
float sceneTwoRAngle2D = 0.0f;
float sceneTwoLimitHeight2D = 0.07f;
void sceneTwoBranch2D(float height, float lineWidth) {

	float xCo = 0.0f;
	float yCo = 0.0f;

	if (height > 0.1) {

		glColor3f(102.0 / 255.0f, 51.0f / 255.0f, 0.0f);

	}
	else {

		glColor3f(5.0 / 255.0f, 144.0f / 255.0f, 51.0f / 255.0f);

	}
	glLineWidth(lineWidth);
	glBegin(GL_LINES);

	xCo = 0.0f;
	yCo = 0.0f;
	glVertex3f(xCo, yCo, 0.0f);
	glVertex3f(xCo, yCo + height, 0.0f);
	glEnd();

	glTranslatef(0.0f, height, 0.0f);


	glPushMatrix();
	if (height > sceneTwoLimitHeight2D) {


		glRotatef(sceneTwoAngle2D, 0.0f, 0.0f, -1.0f);
		sceneTwoBranch2D(height * 0.67, lineWidth * 0.8);

		glPopMatrix();
		glPushMatrix();
		glRotatef(sceneTwoAngle2D, 0.0f, 0.0f, 1.0f);
		sceneTwoBranch2D(height = height * 0.67, lineWidth * 0.8);
		glPopMatrix();

		if (height < 0.4) {
			glPushMatrix();
			glRotatef(sceneTwoAngle2D, -1.0f, 0.0f, 0.0f);
			sceneTwoBranch2D(height, lineWidth * 0.4);
			glPopMatrix();

			glPushMatrix();
			glRotatef(sceneTwoAngle2D, 1.0f, 0.0f, 0.0f);
			sceneTwoBranch2D(height, lineWidth * 0.4);
			glPopMatrix();

			glPushMatrix();
			glRotatef(sceneTwoAngle2D, 1.0f, 0.0f, 0.0f);
			sceneTwoBranch2D(height, lineWidth * 0.4);
			glPopMatrix();


		}


		//lineWidth -= 0.1f;
	}
	else {

		glBegin(GL_LINES);

		glVertex3f(xCo, yCo, 0.0f);
		glVertex3f(xCo, yCo + height * 0.67, 0.0f);
		glEnd();
		glPopMatrix();
	}

	glColor3f(1.0f, 1.0, 1.0);


}

void sceneTwoCallToBranch2D(float x, float y, float z) {

	sceneTwoLineWidth2D = 15.0f;

	glTranslatef(0.0f, 0.0f, -3.0f);
	//glTranslatef(0.0f, -1.25f, 0.0f);
	glTranslatef(x, y, z);
	glPushMatrix();
	//glRotatef(rAngle, 0.0f, 1.0f, 0.0f);
	sceneTwoBranch2D(0.6f, sceneTwoLineWidth2D);

	glPopMatrix();
}


void sceneTwoTrees2D() {

	// 1
	glPushMatrix();
	glTranslatef(-2.0f, 0.0f, 0.0f);
	glScalef(1.20f, 1.20f, 1.20f);
	sceneTwoCallToBranch(0.0f, 0.0f, 0.0f);
	glPopMatrix();


	// 2
	glPushMatrix();
	glTranslatef(1.5f, 0.0f, 20.0f);
	glScalef(1.20f, 1.20f, 1.20f);
	sceneTwoCallToBranch(0.0f, 0.0f, 0.0f);
	glPopMatrix();

	// 3
	glPushMatrix();
	glTranslatef(3.0f, 0.0f, 10.0f);
	glScalef(1.20f, 1.20f, 1.20f);
	sceneTwoCallToBranch(0.0f, 0.0f, 0.0f);
	glPopMatrix();

}


// Temple
void sceneTwoQuadWithoutFront() {
	//glColor3f(123.0f / 255.0f, 63.0f / 255.0f, 0.0f);

	glBindTexture(GL_TEXTURE_2D, sceneTwoTexture_side);
	glBegin(GL_QUADS);

	//right face
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

	glBindTexture(GL_TEXTURE_2D, sceneTwoTexture_stares);
	glBegin(GL_QUADS);
	//back face
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);


	glBindTexture(GL_TEXTURE_2D, sceneTwoTexture_side);
	glBegin(GL_QUADS);
	//left face
	//cyan color
	//glColor3f(0.0f, 1.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

	glBindTexture(GL_TEXTURE_2D, sceneTwoTexture_side);
	glBegin(GL_QUADS);
	//top surface
	//megeta color
	//glColor3f(1.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);


	glBindTexture(GL_TEXTURE_2D, sceneTwoTexture_side);
	glBegin(GL_QUADS);
	//botttom surface
	//yellow color
	//glColor3f(1.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);
	//glColor3f(1.0f, 1.0f, 1.0f);

}

void sceneTwoPole() {

	glBegin(GL_QUADS);

	//front face
	//glColor3f(1.0f, 0.0f, 0.0f);	

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);


	//right face
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//back face
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//left face
	//cyan color
	//glColor3f(0.0f, 1.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);


	//top surface
	//megeta color
	//glColor3f(1.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	//botttom surface
	//yellow color
	//glColor3f(1.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();

}

void sceneTwoQuadWithNormalTetxure() {

	glBegin(GL_QUADS);

	//front face
	//glColor3f(1.0f, 0.0f, 0.0f);	

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);


	//right face
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//back face
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//left face
	//cyan color
	//glColor3f(0.0f, 1.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);


	//top surface
	//megeta color
	//glColor3f(1.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	//botttom surface
	//yellow color
	//glColor3f(1.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();


}

void sceneTwoSingleQuad() {

	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();

}
void sceneTwoQuad() {

	glBegin(GL_QUADS);

	//front face
	//glColor3f(1.0f, 0.0f, 0.0f);	

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(3.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(3.0f, 3.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 3.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);


	//right face
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(3.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(3.0f, 3.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 3.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//back face
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(3.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(3.0f, 3.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 3.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//left face
	//cyan color
	//glColor3f(0.0f, 1.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(3.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(3.0f, 3.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 3.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);


	//top surface
	//megeta color
	//glColor3f(1.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glColor3f(0.95f, 0.95f, 0.95f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(3.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(3.0f, 3.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0f, 3.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	//botttom surface
	//yellow color
	//glColor3f(1.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(3.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(3.0f, 3.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 3.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();

}

void sceneTwoPyradmid() {

	glBegin(GL_TRIANGLES);

	//front face
	//apex
	//glColor3f(1.0f, 1.0f, 1.0f);									// apex will not be on forward or behind hence z is 0.0f
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	//right face
	//appex
	//glColor3f(1.0f, 0.0f, 0.0f);									// apex will not be on forward or behind hence z is 0.0f
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	//left bottom
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	//right bottom
	//glColor3f(0.0f, 1.0f, 0.0f);
	//glTexCoord2f(0.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//back face
	//glColor3f(1.0f, 0.0f, 0.0f);									// apex will not be on forward or behind hence z is 0.0f
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	//left bottom
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//right bottom
	//glColor3f(0.0f, 0.0f, 1.0f);
	//glTexCoord2f(1.0f, 0.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);


	// left face
	//appex
	//glColor3f(1.0f, 0.0f, 0.0f);									// apex will not be on forward or behind hence z is 0.0f
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	// left bottom
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	// right bottom
	//glColor3f(0.0f, 1.0f, 0.0f);
	//glTexCoord2f(1.0f, 0.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glEnd();
}


void sceneTwoTriangle() {

	glBegin(GL_TRIANGLES);
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();

}

void sceneTwoTemple() {

	glPushMatrix();

	glTranslatef(-5.5f, 1.3f, -10.0f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	//main room
	glPushMatrix();
	//glColor3f(0.9f, 0.0f, 0.0f);
	glBindTexture(GL_TEXTURE_2D, sceneTwoTexture_stares);
	glTranslatef(0.0f, 0.0f, -1.3f);
	glScalef(1.5f, 0.75f, 0.75f);
	sceneTwoQuadWithoutFront();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	// base
	glPushMatrix();
	//glBindTexture(GL_TEXTURE_2D, texture_pole);
	//glColor3f(123.0f / 255.0f, 63.0f / 255.0f, 0.0f);
	glColor3f(101.0f / 255.0f, 70.0f / 255.0f, 62.0f / 255.0f);
	glTranslatef(0.0f, -1.05f, -1.42f);
	glScalef(2.3f, 0.25f, 1.97f);
	sceneTwoQuadWithNormalTetxure();
	//glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	// god frame
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, sceneTwoTexture_god);
	glColor3f(1.0f, 1.0f, 1.0f);
	glTranslatef(0.0f, 0.0f, -1.25f - 1.2f);
	glScalef(0.50, 0.50, 0.50);
	sceneTwoSingleQuad();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();



	// upper1
	glColor3f(65.0 / 255.0f, 25.0 / 255.0f, 0);
	glPushMatrix();
	//glBindTexture(GL_TEXTURE_2D, texture_pole);

	glTranslatef(0.0f, 0.88f, -1.3f);
	glScalef(1.25f, 0.12f, 0.75f);
	sceneTwoQuadWithNormalTetxure();
	//glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	// upper2
	glPushMatrix();
	//glBindTexture(GL_TEXTURE_2D, texture_pole);
	glTranslatef(0.0f, 1.10f, -1.3f);
	glScalef(1.00f, 0.12f, 0.75f);
	sceneTwoQuadWithNormalTetxure();
	//glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	glColor3f(1.0f, 1.0f, 1.0f);
	// pataka
	glPushMatrix();
	glColor3f(141.0 / 255.0f, 85.0 / 255.0f, 36.0 / 255.0f);
	glTranslatef(0.0f, 2.7f, -0.2f - 1.1f);
	glLineWidth(10.0f);
	glBegin(GL_LINES);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glEnd();
	glPopMatrix();
	glColor3f(1.0f, 1.0f, 1.0f);


	glPushMatrix();
	//glColor3f(255.0f / 255.0f, 165.0f / 255.0f, 0.0f);
	glColor3f(254.0f / 255.0f, 80 / 255.0f, 0);
	glTranslatef(0.718f, 4.22f, -0.75f - 1.1f);
	glScalef(0.75f, 0.55f, 0.55f);
	glRotatef(90.0f, 0.0f, 0.0f, -1.0f);
	sceneTwoTriangle();
	glPopMatrix();


	// Center pyramid
	//glColor3f(1.0f, 1.0f, 1.0f);
	//glColor3f(212.0f / 255.0f, 175.0f / 255.0f, 55.0f / 255.0f);
	glColor3f(128.0f / 255.0f, 90.0f / 255.0f, 70.0f / 255.0f);

	//glBindTexture(GL_TEXTURE_2D, texture_top);
	glPushMatrix();
	glTranslatef(0.0f, 2.00f, -0.2f - 1.1f);
	glScalef(0.75f, 0.8f, 1.0f);
	sceneTwoPyradmid();
	glPopMatrix();
	//glBindTexture(GL_TEXTURE_2D, 0);


	glColor3f(1.0f, 1.0f, 1.0f);

	// step 1
	glColor3f(114.0f / 255.0f, 114.0f / 255.0f, 114.0f / 255.0f);
	//glBindTexture(GL_TEXTURE_2D, texture_stares);
	glPushMatrix();
	glTranslatef(0.0f, -0.88f, 1.20f - 0.55f);
	glScalef(1.00f, 0.08f, 0.10f);
	sceneTwoQuad();
	glPopMatrix();
	//glBindTexture(GL_TEXTURE_2D, 0);



	//// step 2
	glColor3f(114.0f / 255.0f, 114.0f / 255.0f, 114.0f / 255.0f);
	//glBindTexture(GL_TEXTURE_2D, texture_stares);
	glPushMatrix();
	glTranslatef(0.0f, -1.07f, 0.85);
	glScalef(1.40f, 0.12f, 0.10f);
	sceneTwoQuad();
	glPopMatrix();
	//glBindTexture(GL_TEXTURE_2D, 0);

	//// step 3
	glColor3f(114.0f / 255.0f, 114.0f / 255.0f, 114.0f / 255.0f);
	//glBindTexture(GL_TEXTURE_2D, texture_stares);
	glPushMatrix();
	glTranslatef(0.0f, -1.2f, 1.05);
	glScalef(1.20f, 0.12f, 0.10f);
	sceneTwoQuad();
	glPopMatrix();
	//glBindTexture(GL_TEXTURE_2D, 0);


	glPopMatrix();

}

void genarateZ(int row, int col) {

	for (int i = 0; i < row; i++) {

		for (int j = 0; j < col; j++) {

			terrainZ[i][j] = (((float)rand()) / RAND_MAX) * 200.0f;
			fprintf(gpFile, "%f\n", terrainZ[i][j]);

		}

	}

}

// Mountain
void sceneTwoTerrain() {

	glTranslatef(-38.0f, 5.0f, -50.0f);
	glScalef(0.06f, 0.05f, 0.05f);
	//glColor3f(0.0f, 0.0f, 1.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	//glRotatef(180.0f, 1.0f, 0.0f, 0.0f);
	glBindTexture(GL_TEXTURE_2D, sceneTwoTexture_water);
	for (int i = 0; i < sceneTwoRow; i++) {
		glBegin(GL_TRIANGLE_STRIP);

		for (int j = 0; j < sceneTwoCol; j++) {

			glTexCoord2f((j / sceneTwoCol), (i / sceneTwoRow));

			//glTexCoord2f(1.0f, 1.0f);
			glVertex3f(j * sceneTwoScl, i * sceneTwoScl, terrainZ[i][j]);
			//glVertex3f(j * scl, i * scl, rand() % 2);

			glTexCoord2f((j / sceneTwoCol), ((i + 1) / sceneTwoRow));
			//glTexCoord2f(0.0f, 1.0f);
			glVertex3f(j * sceneTwoScl, (i + 1) * sceneTwoScl, terrainZ[i + 1][j]);
			//glVertex3f(j * scl, (i + 1) * scl, rand() % 2);

		}
		glEnd();


	}
	glBindTexture(GL_TEXTURE_2D, 0);


	glColor3f(1.0f, 1.0f, 1.0f);

}

// Poster
void sceneTwoPoster_front() {

	glBegin(GL_QUADS);

	//front face
	//glColor3f(1.0f, 0.0f, 0.0f);	

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glEnd();

}

void sceneTwoPoster_wood_bar() {

	glBegin(GL_QUADS);

	//front face
	//glColor3f(1.0f, 0.0f, 0.0f);	

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);


	//right face
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//back face
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//left face
	//cyan color
	//glColor3f(0.0f, 1.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);


	//top surface
	//megeta color
	//glColor3f(1.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	//botttom surface
	//yellow color
	//glColor3f(1.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();

}

void sceneTwoBoard() {

	glPushMatrix();
	glTranslatef(40.0f, 2.4f, -30.0f);
	glRotatef(40.0f, 0.0f, -1.0f, 0.0f);
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, villageBoard);
	glTranslated(0.0f, 1.5f, 0.0f);
	glScalef(2.6f, 0.8f, 0.5f);
	sceneTwoPoster_front();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(2.5f, -0.5f, 0.0f);
	glScalef(0.2, 2.0f, 0.2f);
	glBindTexture(GL_TEXTURE_2D, sceneTwoTexture_woodbar);
	sceneTwoPoster_wood_bar();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-2.5f, -0.5f, 0.0f);
	glScalef(0.2, 2.0f, 0.2f);
	glBindTexture(GL_TEXTURE_2D, sceneTwoTexture_woodbar);
	sceneTwoPoster_wood_bar();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	/*glPushMatrix();
	glTranslatef(0.0, -2.5f, 0.0f);
	glScalef(10.0f, 0.0f, 10.0f);
	ground(2);
	glPopMatrix();*/

	glPopMatrix();

}

void sceneTwoPeek() {

	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glVertex3f(-1.0f, -1.0f, 0.0f);

	glVertex3f(1.0f, -1.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);

	glEnd();

}

void sceneTwoWings(int flag) {

	glBindTexture(GL_TEXTURE_2D, sceneTwoTexture_bird_middle);
	glBegin(GL_QUADS);

	//front face
	//glColor3f(1.0f, 0.0f, 0.0f);	

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.2f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.2f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	//glVertex3f(-1.0f, -1.0f, 1.0f);

	if (flag == 1)
		glVertex3f(-1.0f, -0.3f, sceneTwoWingFlag);
	if (flag == 2)
		glVertex3f(-1.0f, -0.3f, sceneTwoWingFlag);

	glTexCoord2f(0.0f, 1.0f);
	if (flag == 1)
		glVertex3f(0.2f, -0.3f, sceneTwoWingFlag);
	if (flag == 2)
		glVertex3f(0.2f, -0.3f, sceneTwoWingFlag);


	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

}

void sceneTwoQuad_wings() {

	glBindTexture(GL_TEXTURE_2D, sceneTwoTexture_bird_middle);

	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(1.0f, 1.0f, 1.0f);

	glVertex3f(-1.0f, 1.0f, 1.0f);

	glVertex3f(-1.0f, -1.0f, 1.0f);

	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

}

// bird source code 
void sceneTwoBirdCode() {

	glBindTexture(GL_TEXTURE_2D, sceneTwoTexture_bird_middle);
	glRotatef(90.0f, 0.0f, -1.0f, 0.0f);
	glPushMatrix();
	//middle
	//glPushMatrix();
	glScalef(0.50f, 0.25f, 0.25f);
	glColor3f(1.0f, 1.0f, 1.0f);
	gluQuadricTexture(quadric, GL_TRUE);
	gluSphere(quadric, 0.3f, 30, 30);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);



	// legs
	glColor3f(199.0 / 255.0f, 133.0 / 255.0f, 72.0 / 255.0f);
	glTranslatef(0.0f, -0.07f, 0.0f);
	glLineWidth(20.0f);
	glPushMatrix();
	glBegin(GL_LINES);
	glVertex3f(0.00f, 0.0f, 0.0f);
	glVertex3f(0.0f, -0.05f, 0.05f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(0.00f, 0.0f, 0.0f);
	glVertex3f(0.0f, -0.05f, -0.05f);
	glEnd();
	glColor3f(1.0f, 1.0f, 1.0f);

	glPopMatrix();

	// head
	glPushMatrix();
	glColor3f(72.0f / 255.0f, 138.0 / 255.0f, 199.0 / 255.0f);

	//glColor3f(72.0f / 255.0f, 138.0 / 255.0f, 210.0 / 255.0f);
	glTranslatef(-0.16, 0.09f, 0.0f);
	glScalef(0.16f, 0.16f, 0.16f);
	//glColor3f(1.0f, 1.0f, 0.0f);
	gluSphere(quadric, 0.3f, 30, 30);
	//PointDebug();
	glColor3f(1.0f, 1.0f, 1.0f);
	glPopMatrix();

	// Peek
	glPushMatrix();
	glColor3f(199.0 / 255.0f, 133.0 / 255.0f, 72.0 / 255.0f);
	glTranslatef(-0.2, 0.09f, 0.0f);
	glScalef(0.16f, 0.16f, 0.16f);

	glRotatef(270.0f, 0.0f, 1.0f, 0.0f);
	//glRotatef(100.0f, -1.0f, 0.0f, 0.0f);
	gluCylinder(quadric, 0.08f, 0.01f, 0.28f, 30, 30);
	glColor3f(1.0f, 1.0f, 1.0f);
	glPopMatrix();


	// LEFT
	glPushMatrix();
	glScalef(0.10f, 0.10f, 0.10f);
	glTranslatef(0.1, -0.5f, 0.9f);
	glRotatef(70.0f, -1.0f, 0.0f, 0.0f);
	sceneTwoWings(1);
	glPopMatrix();

	// RIGHT
	glPushMatrix();
	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	//glPushMatrix();
	glScalef(0.10f, 0.10f, 0.10f);
	glTranslatef(0.1, -0.5f, 1.0f);
	glRotatef(70.0f, -1.0f, 0.0f, 0.0f);
	sceneTwoWings(1);
	glPopMatrix();

	//glPopMatrix();

	////
	// eyes Inner
	/*glPushMatrix();
	glColor3f(0.0f, 0.0f, 0.0f);
	glTranslatef(-0.19f, 0.1f, -0.02f);
	glScalef(0.03f, 0.03f, 0.03f);
	gluSphere(quadric, 0.3f, 30, 30);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-0.19f, 0.1f, 0.03f);
	glScalef(0.03f, 0.03f, 0.03f);
	gluSphere(quadric, 0.3f, 30, 30);
	glPopMatrix();*/

	glColor3f(0.0f, 0.0f, 0.0f);
	// Eyes Outer
	glPushMatrix();
	glTranslatef(-0.195f, 0.10f, -0.03f);
	glScalef(0.02f, 0.02f, 0.02f);
	gluSphere(quadric, 0.3f, 30, 30);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-0.195f, 0.10f, 0.03f);
	glScalef(0.02f, 0.02f, 0.02f);
	gluSphere(quadric, 0.3f, 30, 30);
	glPopMatrix();
	glColor3f(1.0f, 1.0f, 1.0f);


	// back
	glPushMatrix();
	glTranslatef(0.165f, 0.15f, 0.0f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	glRotatef(70.0f, 1.0f, 0.0f, 0.0f);
	glScalef(0.07f, 0.07f, 0.07f);
	sceneTwoQuad_wings();
	glPopMatrix();
	////PointDebug();
	glPopMatrix();

}


void sceneTwoBird(int flag) {

	if (flag == 1)
		glTranslatef(1.0f, 1.0f, sceneTwoBirdZ);
	else if (flag == 2) {

		glTranslatef(sceneTwoMainBirdX, sceneTwoMainBirdY, sceneTwoMainBirdZ);
		glRotatef(sceneTwoBirdRotate, 0.0f, 1.0f, 0.0f);

	}

	glPushMatrix();
	glTranslatef(0.0f, 0.0f, 0.0f);
	sceneTwoBirdCode();
	glPopMatrix();

}

// call to call birds
void sceneTwoDisplayBirds() {

	glPushMatrix();
	glTranslatef(0.5f, 1.0, 0.5f);
	if (sceneTwoThreeBirdDisplayFlag == 1)
		sceneTwoBird(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 1.0, 0.0f);
	if (sceneTwoThreeBirdDisplayFlag == 1)
		sceneTwoBird(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-0.5f, 1.0, 0.5f);
	if (sceneTwoThreeBirdDisplayFlag == 1)
		sceneTwoBird(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-3.0f, 1.0, 0.5f);
	sceneTwoBird(2);
	glPopMatrix();

}


// Scene Three
void sceneThreeTexture_house(int flag) {

	if (flag == 1) {

		glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_wall2);

	}
	else if (flag == 2) {

		glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_h2md);

	}
	else if (flag == 3) {

		glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_h3md);

	}
	else if (flag == 4) {

		glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_h4md);

	}
	else if (flag == 5) {

		glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_h5md);

	}
	else if (flag == 6) {

		glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_h5sf);

	}
	glBegin(GL_QUADS);
	//front face
	//glColor3f(1.0f, 0.0f, 0.0f);	

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

	if (flag == 1) {

		glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_wall1);

	}
	else if (flag == 2) {

		glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_h2s);

	}
	else if (flag == 3) {

		glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_h3s);

	}
	else if (flag == 4) {

		glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_h4s);

	}
	else if (flag == 5) {

		glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_h5s);

	}
	else if (flag == 6) {

		glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_h5s);

	}

	glBegin(GL_QUADS);
	//right face
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//back face
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//left face
	//cyan color
	//glColor3f(0.0f, 1.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);


	//top surface
	//megeta color
	//glColor3f(1.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	//botttom surface
	//yellow color
	//glColor3f(1.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);

}


void sceneThreeBase2() {

	glBegin(GL_QUADS);
	//front face
	//glColor3f(1.0f, 0.0f, 0.0f);	

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);


	//right face
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//back face
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//left face
	//cyan color
	//glColor3f(0.0f, 1.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);


	//top surface
	//megeta color
	//glColor3f(1.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	//botttom surface
	//yellow color
	//glColor3f(1.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();

}
void sceneThreeBase1() {

	glBegin(GL_QUADS);

	//front face
	//glColor3f(1.0f, 0.0f, 0.0f);	

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);


	//right face
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//back face
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//left face
	//cyan color
	//glColor3f(0.0f, 1.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);


	//top surface
	//megeta color
	//glColor3f(1.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	//botttom surface
	//yellow color
	//glColor3f(1.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();

}


void sceneThreeHome1(int flag) {

	float width1 = 1.25;
	float height1 = 0.65f;
	float zLength1 = 1.0f;
	//glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	// main room
	glPushMatrix();
	glScalef(width1, height1, zLength1);
	sceneThreeTexture_house(flag);
	glPopMatrix();

}


void sceneThreeRightRoom(int flag) {

	float width1 = 1.25;
	float height1 = 0.65f;
	float zLength1 = 1.0f;

	if (flag == 1) {

		glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_h5s);

	}

	// right room
	glPushMatrix();
	glTranslatef(1.8f, 0.0f, 0.5f);
	glScalef(width1 * 0.50, height1, zLength1 * 1.50);
	sceneThreeTexture_house(6);
	//glColor3f(1.0f, 1.0f, 1.0f);
	//glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

}

void sceneThreeHouse1() {

	sceneThreeHome1(1);				// house 1
}

void sceneThreeHouse2() {

	sceneThreeHome1(2);
}

void sceneThreeHouse3() {

	sceneThreeHome1(3);
}

void sceneThreeHouse4() {

	sceneThreeHome1(4);
}

void sceneThreeHouse5() {
	glPushMatrix();
	glTranslatef(-13.0f, 0.0f, 0.0f);
	sceneThreeHome1(5);
	sceneThreeRightRoom(1);
	glPopMatrix();
}

void sceneThreeGround(int flag) {

	//botttom surface
	if (flag == 1)
		glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_g1);

	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.0f);
	if (flag == 2) {
		if (sceneThreeBagColorCount != 150)
			glColor3f(135.0f / 255.0f, 206.0 / 255.0f, 235.0 / 255.0f);
		else
			glColor3f(sceneThreeBlueR / 255.0f, sceneThreeBlueG / 255.0f, sceneThreeBlueB / 255.0f);

	}
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(25.0f, 0.0f);
	if (flag == 2) {
		if (sceneThreeBagColorCount != 150)
			glColor3f(135.0f / 255.0f, 206.0 / 255.0f, 235.0 / 255.0f);
		else
			glColor3f(sceneThreeBlueR / 255.0f, sceneThreeBlueG / 255.0f, sceneThreeBlueB / 255.0f);

	}
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(25.0f, 25.0f);
	if (flag == 2)
		glColor3f(sceneThreeBackR / 255.0f, sceneThreeBackG / 255.0f, sceneThreeBackB / 255.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 25.0f);
	if (flag == 2)
		glColor3f(sceneThreeBackR / 255.0f, sceneThreeBackG / 255.0f, sceneThreeBackB / 255.0f);
	//glColor3f(254.0 / 255.0f, 105.0 / 255.0f, 0.5f / 255.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();


	glColor3f(1.0f, 1.0f, 1.0f);

	glBindTexture(GL_TEXTURE_2D, 0);

}

// home

void sceneThreeRoom1() {

	glBegin(GL_QUADS);

	//front face
	//glColor3f(1.0f, 0.0f, 0.0f);	

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);


	//right face
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//back face
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//left face
	//cyan color
	//glColor3f(0.0f, 1.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);


	//top surface
	//megeta color
	//glColor3f(1.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	//botttom surface
	//yellow color
	//glColor3f(1.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();

}

void sceneThreeRoom2() {

	glBegin(GL_QUADS);

	//front face
	//glColor3f(1.0f, 0.0f, 0.0f);	

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);


	//right face
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//back face
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//left face
	//cyan color
	//glColor3f(0.0f, 1.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);


	//top surface
	//megeta color
	//glColor3f(1.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	//botttom surface
	//yellow color
	//glColor3f(1.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();

}

void sceneThreeRoof() {

	glBegin(GL_QUADS);

	//front face
	//glColor3f(1.0f, 0.0f, 0.0f);	

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(2.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(2.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glEnd();

}

void sceneThreeHome() {

	float width1 = 1.25;
	float height1 = 0.65f;
	float zLength1 = 1.0f;

	glTranslatef(2.0f, 0.2f, -8.0f);
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_room);
	glScalef(width1, height1, zLength1);
	sceneThreeRoom1();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	// base1
	glPushMatrix();
	glColor3f(0.7f, 0.7f, 0.7f);
	glTranslatef(0.0f, -0.78f, 0.0f);
	glScalef(width1, height1 * 0.20, zLength1);
	sceneThreeRoom1();
	glColor3f(1.0f, 1.0f, 1.0f);
	glPopMatrix();

	// right room
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_room1);
	//glColor3f(123.0f / 255.0f, 63.0f / 255.0f, 0.0f);
	glTranslatef(1.8f, 0.0f, 0.5f);
	glScalef(width1 * 0.50, height1, zLength1 * 1.50);
	sceneThreeRoom2();
	//glColor3f(1.0f, 1.0f, 1.0f);
	//glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	// base2
	glPushMatrix();
	glColor3f(0.7f, 0.7f, 0.7f);
	glTranslatef(1.8f, -0.78f, 0.5f);
	glScalef(width1 * 0.50, height1 * 0.20, zLength1 * 1.50);
	sceneThreeRoom2();
	glColor3f(1.0f, 1.0f, 1.0f);
	glPopMatrix();


	// roofFront room1
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_roof);
	glColor3f(0.7f, 0.7f, 0.7f);
	glTranslated(0.4f, -0.645f, 0.45f);
	glRotatef(60.0f, -1.0f, 0.0f, 0.0f);
	glScalef(width1 * 1.80f, height1 * 1.40f, zLength1 * 1.90);
	sceneThreeRoof();
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	// roofBack room1
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_roof);
	//glColor3f(1.0f, 0.0f, 0.0f);
	glTranslated(0.4f, 2.69f, -1.0f);
	glRotatef(65.0f, 1.0f, 0.0f, 0.0f);
	glScalef(width1 * 1.80f, height1 * 1.45f, zLength1 * 1.80);
	sceneThreeRoof();
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	// fence front
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_Fence);
	glTranslatef(-0.82f, -0.62f, 1.35f);
	glScalef(width1 * 0.60f, height1 * 0.50f, zLength1);
	sceneThreeRoof();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	// left fence
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_Fence);
	//glColor3f(1.0f, 0.0f, 0.0f);
	glTranslatef(-2.57f, -0.62f, 0.6f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	glScalef(width1 * 1.40f, height1 * 0.50f, zLength1);
	sceneThreeRoof();
	glBindTexture(GL_TEXTURE_2D, 0);
	glColor3f(1.0f, 1.0f, 1.0f);
	glPopMatrix();

	// back fence
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_Fence);
	glTranslatef(0.0f, 0.0f, 0.6f);
	//glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.525f, -0.62f, -2.74f);
	glScalef(width1 * 1.70f, height1 * 0.50f, zLength1);
	sceneThreeRoof();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	// right front fence
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_Fence);
	glTranslatef(1.9f, -0.62f, 1.35f);
	glScalef(width1 * 0.60f, height1 * 0.50f, zLength1);
	sceneThreeRoof();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	// right fence
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_Fence);
	//glColor3f(1.0f, 0.0f, 0.0f);
	glTranslatef(1.65f, -0.62f, 0.6f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	glScalef(width1 * 1.40f, height1 * 0.50f, zLength1);
	sceneThreeRoof();
	glBindTexture(GL_TEXTURE_2D, 0);
	glColor3f(1.0f, 1.0f, 1.0f);
	glPopMatrix();

}

// girl
void sceneThreeRightHand() {


	//right hand
	glPushMatrix();
	glScalef(0.10f, 0.10f, 0.10f);
	glTranslatef(-sceneThreeHumanWidth * 1.05, sceneThreeHumanMiddle * 0.55, 0.0f);
	//PointDebug();
	//PointDebug();
	glPushMatrix();
	glRotatef((GLfloat)sceneThreeShoulderL, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.0, -1.0f, 0.0f);
	//PointDebug();

	glPushMatrix();
	glScalef(0.5f, 2.0f, 0.5f);
	// now draw the arm
	//quadric = gluNewQuadric();
	glColor3f(34.0f / 255.0f, 177.0f / 255.0f, 76.0f / 255.0f);
	//glRotatef(10.0f, 0.0f, 0.0f, 1.0f);
	gluSphere(quadric, 0.5f, 10, 10);
	glPopMatrix();
	glTranslatef(0.0, -1.0f, 0.0f);
	glPushMatrix();
	glRotatef((GLfloat)sceneThreeElbowL, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.0, -0.5f, 0.0f);
	//PointDebug();
	glPushMatrix();
	glScalef(0.55f, 2.0f, 0.5f);
	// now draw the arm
	//quadric = gluNewQuadric();
	glColor3f(0.5f, 0.35f, 0.05f);
	gluSphere(quadric, 0.5f, 10, 10);
	glPopMatrix();



	//glRotatef((GLfloat)elbow, 1.0f, 0.0f, 0.0f);
	//glTranslatef(0.0, -1.0f, 0.0f);
	//PointDebug();

	glPopMatrix();
	glPopMatrix();
	glPopMatrix();

}

void sceneThreeLeftHand() {

	//right hand
	glPushMatrix();
	glScalef(0.10f, 0.10f, 0.10f);
	glTranslatef(sceneThreeHumanWidth * 0.95, sceneThreeHumanMiddle * 0.55, 0.0f);
	//PointDebug();
	glPushMatrix();
	glRotatef((GLfloat)sceneThreeShoulderL, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.0, -1.0f, 0.0f);
	//PointDebug();

	glPushMatrix();
	glScalef(0.5f, 2.0f, 0.5f);
	// now draw the arm
	//quadric = gluNewQuadric();
	glColor3f(34.0f / 255.0f, 177.0f / 255.0f, 76.0f / 255.0f);
	gluSphere(quadric, 0.5f, 10, 10);
	glPopMatrix();
	glTranslatef(0.0, -1.0f, 0.0f);
	glPushMatrix();
	glRotatef((GLfloat)sceneThreeElbowL, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.0, -0.5f, 0.0f);
	//PointDebug();
	glPushMatrix();
	glScalef(0.55f, 2.0f, 0.5f);
	// now draw the arm
	//quadric = gluNewQuadric();
	glColor3f(0.5f, 0.35f, 0.05f);
	gluSphere(quadric, 0.5f, 10, 10);
	glColor3f(1.0f, 1.0f, 1.0f);

	glPopMatrix();


	//glRotatef((GLfloat)elbow, 1.0f, 0.0f, 0.0f);
	//glTranslatef(0.0, -1.0f, 0.0f);
	//PointDebug();

	glPopMatrix();
	glPopMatrix();
	glPopMatrix();


}

void sceneThreeLowerBody() {

	// lower body
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_sadi_lower);

	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	/*glScalef(0.65f, 2.25f, 1.0);
	gluSphere(quadric, 2.0f, 50, 50);*/
	gluQuadricTexture(quadric, GL_TRUE);
	glRotatef(10.0f, 0.0f, 0.0f, -1.0f);
	gluCylinder(quadric, sceneThreeHumanWidth * 0.65, sceneThreeHumanWidth * 0.95, sceneThreeHumanMiddle * 0.55, 50, 50);
	glBindTexture(GL_TEXTURE_2D, 0);
	//gluSphere(quadric, 0.5f, 50, 50);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_sadi_lower);
	glTranslatef(0.0f, -(sceneThreeHumanMiddle * 0.55 + sceneThreeHumanMiddle * 0.45), 0.0f);
	glRotatef(270.0f, 1.0f, 0.0f, 0.0f);
	gluQuadricTexture(quadric, GL_TRUE);
	glRotatef(150.0f, 0.0f, 0.0f, 1.0f);
	gluCylinder(quadric, sceneThreeHumanWidth * 0.65, sceneThreeHumanWidth * 0.95, sceneThreeHumanMiddle * 0.45, 50, 50);
	glBindTexture(GL_TEXTURE_2D, 0);
	//PointDebug();

	glPopMatrix();

}


void sceneThreeMiddleBody() {

	// Middle Body
	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_sadi);
	glPushMatrix();
	glScalef(0.10f, 0.10f, 0.10f);
	glPushMatrix();
	glRotatef(270.0f, 1.0f, 0.0f, 0.0f);
	/*glScalef(0.65f, 2.25f, 1.0);
	gluSphere(quadric, 2.0f, 50, 50);*/
	gluQuadricTexture(quadric, GL_TRUE);
	glRotatef(200.0f, 0.0f, 0.0f, 1.0f);
	gluCylinder(quadric, sceneThreeHumanWidth * 0.65, sceneThreeHumanWidth * 1.05, sceneThreeHumanMiddle * 0.55, 50, 50);
	//gluSphere(quadric, 0.5f, 50, 50);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

}

void sceneThreeNeck() {

	glPushMatrix();
	glScalef(0.10f, 0.10f, 0.10f);
	glTranslatef(0.0f, sceneThreeHumanMiddle * 0.55, 0.0);
	//PointDebug();
	glRotatef(270.0f, 1.0f, 0.0f, 0.0f);
	gluCylinder(quadric, sceneThreeHumanWidth * 0.20, sceneThreeHumanWidth * 0.20, sceneThreeHumanMiddle * 0.15, 50, 50);
	glPopMatrix();

}

void sceneThreeHead() {

	//glBindTexture(GL_TEXTURE_2D, texture_head);
	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_arm);
	glPushMatrix();
	glScalef(0.10f, 0.10f, 0.10f);
	glTranslatef(0.0f, sceneThreeHumanMiddle * 0.55 + sceneThreeHumanMiddle * 0.40, 0.0);

	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	gluQuadricTexture(quadric, GL_TRUE);
	gluSphere(quadric, sceneThreeHumanWidth * 0.70, 50, 50);
	//PointDebug();
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

}

void sceneThreeWholeBody() {

	sceneThreeMiddleBody();
	sceneThreeLowerBody();
	sceneThreeNeck();
	sceneThreeHead();
	sceneThreeLeftHand();
	sceneThreeRightHand();

}

void sceneThreeTriangle() {

	glBegin(GL_TRIANGLES);

	//front face
	//apex
	//glColor3f(1.0f, 1.0f, 1.0f);									// apex will not be on forward or behind hence z is 0.0f
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glEnd();

}

void sceneThreeLeadyQuad() {
	
	glBegin(GL_QUADS);

	//front face
	glColor3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glEnd();

}
void sceneThreeLeady() {


	glPushMatrix();
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0.9f);
	glBindTexture(GL_TEXTURE_2D, texture_leady);
	glTranslatef(0.4f, 0.3f, 0.0f);
	glScalef(0.75f, 0.75f, 0.75f);
	sceneThreeLeadyQuad();
	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_ALPHA_TEST);
	glPopMatrix();
}

// Vart
void sceneThreeWheel() {

	// redius
	float r = 0.5f;
	glColor3f(123.0f / 255.0f, 63.0f / 255.0f, 0.0f);
	glPointSize(10.0f);
	for (float i = 0; i < 359; i += 0.5f) {

		glBegin(GL_POINTS);
		glVertex3f(r * cos(i), r * sin(i), 0.0f);
		glVertex3f(r * cos(i + 1), r * sin(i + 1), 0.0f);
		glEnd();

	}
	glColor3f(1.0f, 1.0f, 1.0f);

	glColor3f(192.0f / 255.0f, 192.0f / 255.0f, 192.0f / 255.0f);

	for (int i = 0; i < 360; i += 2) {

		glBegin(GL_LINES);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f((r * 0.98) * cos(i), (r * 0.98) * sin(i), 0.0f);
		glEnd();

	}
	glColor3f(1.0f, 1.0f, 1.0f);

}

void sceneThreeCube() {

	glBegin(GL_QUADS);

	//front face
	//glColor3f(1.0f, 0.0f, 0.0f);	

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);


	//right face
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//back face
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//left face
	//cyan color
	//glColor3f(0.0f, 1.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);


	//top surface
	//megeta color
	//glColor3f(1.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	//botttom surface
	//yellow color
	//glColor3f(1.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();

}

void sceneThreeMain_cart(int flag) {

	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_bar);

	glBegin(GL_QUADS);
	//botttom surface
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);

}

void sceneThreeCart() {

	glPushMatrix();
	glScalef(0.80f, 0.35f, 0.35f);
	sceneThreeMain_cart(1);
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_bar);
	glPopMatrix();

	// +z standing bars 
	for (int i = -2; i < 3; i++) {
		// 3
		glPushMatrix();
		glScalef(0.80f, 0.35f, 0.35f);

		glScalef(0.50f, 1.25f, 0.50f);
		glTranslatef(i, -0.8f, 1.8f);
		glRotatef(270.0f, 1.0f, 0.0f, 0.0f);
		gluQuadricTexture(quadric, TRUE);
		gluCylinder(quadric, 0.1f, 0.1f, 2.0f, 30, 30);
		glPopMatrix();

	}

	// + z horizontal bar
	glPushMatrix();
	glScalef(0.80f, 0.35f, 0.35f);
	glScalef(1.30f, 1.25f, 0.50f);
	glTranslatef(-1.0, 1.2f, 1.8f);
	glRotatef(270.0f, 1.0f, 0.0f, 0.0f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	gluQuadricTexture(quadric, TRUE);
	gluCylinder(quadric, 0.1f, 0.1f, 2.0f, 30, 30);
	glPopMatrix();

	// -z standing bars 
	for (int i = -2; i < 3; i++) {
		// 3
		glPushMatrix();
		glScalef(0.80f, 0.35f, 0.35f);

		glScalef(0.50f, 1.25f, 0.50f);
		glTranslatef(i, -0.8f, -1.8f);
		glRotatef(270.0f, 1.0f, 0.0f, 0.0f);
		gluQuadricTexture(quadric, TRUE);

		gluCylinder(quadric, 0.1f, 0.1f, 2.0f, 30, 30);
		glPopMatrix();

	}

	// - z horizontal bar
	glPushMatrix();
	glScalef(0.80f, 0.35f, 0.35f);
	glScalef(1.30f, 1.25f, 0.50f);
	glTranslatef(-1.0, 1.2f, -1.8f);
	glRotatef(270.0f, 1.0f, 0.0f, 0.0f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	gluQuadricTexture(quadric, TRUE);
	gluCylinder(quadric, 0.1f, 0.1f, 2.0f, 30, 30);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);


	// z+ wheel
	glPushMatrix();
	glTranslatef(0.2f, -0.32f, 0.39f);
	sceneThreeWheel();
	glPopMatrix();

	// z- wheel
	glPushMatrix();
	glTranslatef(0.2f, -0.32f, -0.39f);
	sceneThreeWheel();
	glPopMatrix();

	// rod1
	glPushMatrix();
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_bar);
	glTranslatef(-1.3f, -0.48f, 0.0f);
	glRotatef(20.0f, 0.0f, 0.0f, 1.0f);
	glScalef(0.7f, 0.04f, 0.03f);
	sceneThreeCube();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

	// rod2
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_bar);
	glTranslatef(-2.0f, -0.7f, 0.0f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	glScalef(0.6f, 0.08f, 0.06f);
	sceneThreeCube();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();


}

// 
void sceneThreeTerrain() {
	
	glTranslatef(75.0f, 5.0f, -40.0f);
	glScalef(0.09f, 0.05f, 0.05f);
	//glColor3f(0.0f, 0.0f, 1.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, sceneTwoTexture_water);
	for (int i = 0; i < sceneThreeRow; i++) {
		glBegin(GL_TRIANGLE_STRIP);

		for (int j = 0; j < sceneThreeCol; j++) {

			glTexCoord2f((j / sceneThreeCol), (i / sceneThreeRow));

			//glTexCoord2f(1.0f, 1.0f);
			glVertex3f(j * sceneThreeScl, i * sceneThreeScl, terrainZ[i][j]);
			//glVertex3f(j * scl, i * scl, rand() % 2);

			glTexCoord2f((j / sceneThreeCol), ((i + 1) / sceneThreeRow));
			//glTexCoord2f(0.0f, 1.0f);
			glVertex3f(j * sceneThreeScl, (i + 1) * sceneThreeScl, terrainZ[i + 1][j]);
			//glVertex3f(j * scl, (i + 1) * scl, rand() % 2);

		}
		glEnd();


	}
	glBindTexture(GL_TEXTURE_2D, 0);

	glColor3f(1.0f, 1.0f, 1.0f);

}

// Tree
float sceneThreeScaleValue = 0.08f;
void sceneThreeDisplayQuad() {

	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0.9f);
	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_Leaves);

	glRotatef(40.0f, 0.0f, 1.0f, 0.0f);
	glScalef(sceneThreeScaleValue, sceneThreeScaleValue, sceneThreeScaleValue);
	glBegin(GL_QUADS);
	//glColor3f(1.0f, 0.0f, 0.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 0.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);

	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);

	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);

	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);


}
void sceneThreeBranch(float height, float redius) {

	// main code to draw 
	///////////////////////////////////////////////////////////////////////////////

	// flag == 0 for first trunk drawing
	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_trunk);
	gluQuadricTexture(quadric, GL_TRUE);

	if (sceneThreeFlag == 0) {

		gluCylinder(quadric, redius * (1 + sceneThreeRediusReduce) * 1.50, redius * (1 + sceneThreeRediusReduce), height, 5, 5);
		sceneThreeFlag = 1;

		// flag != 0 for branches
	}
	else {

		gluCylinder(quadric, redius * (1 + sceneThreeRediusReduce), redius * 0.75, height, 5, 5);
	}

	///////////////////////////////////////////////////////////////////////////////

	glTranslatef(0.0f, 0.0f, height);
	glPushMatrix();

	if (height > heightLimit) {

		glRotatef(45.0f, 0.0f, 1.0f, 0.0f);
		if (height > 0.5)
			sceneThreeBranch(height * 0.67f, redius);
		else
			sceneThreeBranch(height * 0.67f, redius * sceneThreeRediusReduce);


		glPopMatrix();
		glPushMatrix();

		glRotatef(45.0f, 0.0f, -1.0f, 0.0f);
		if (height > 0.5)
			sceneThreeBranch(height * 0.67f, redius);
		else
			sceneThreeBranch(height * 0.67f, redius * sceneThreeRediusReduce);

		glPopMatrix();
		glPushMatrix();


		glRotatef(45.0f, 1.0f, 0.0f, 0.0f);
		if (height > 0.5)
			sceneThreeBranch(height * 0.67f, redius);
		else
			sceneThreeBranch(height * 0.67f, redius * sceneThreeRediusReduce);


		glPopMatrix();
		glPushMatrix();

		glRotatef(45.0f, -1.0f, 0.0f, 0.0f);
		if (height > 0.5)
			sceneThreeBranch(height * 0.67f, redius);
		else
			sceneThreeBranch(height * 0.67f, redius * sceneThreeRediusReduce);

		glPopMatrix();

	}
	else if (height < heightLimit && height > 0.1f) {
		glColor3f(0.0, 0.7f, 0.0f);
		sceneThreeDisplayQuad();
		glColor3f(1.0, 1.0f, 1.0f);

		glPopMatrix();

	}

	glBindTexture(GL_TEXTURE_2D, 0);

}

void sceneThreeCallToBranch(float x, float y, float z) {

	glPushMatrix();
	float redius = 0.04f;
	sceneThreeFlag = 0;

	glTranslatef(x, y, z);
	glRotatef(45.0, 0.0f, 1.0f, 0.0f);

	glRotatef(270.0f, 1.0f, 0.0f, 0.0f);
	sceneThreeBranch(sceneThreeHeight, redius);
	glPopMatrix();
}

void sceneThreeCircle(float redius) {

	glBegin(GL_POLYGON);
	for (float i = 0; i < 360; i += 0.5f) {

		glVertex3f(redius * cos(i), redius * sin(i), 0.0f);

	}
	glEnd();

}
void sceneThreeBase(float b) {

	//glColor3f(0.7f, 0.7f, 0.7f);
	glPushMatrix();
	glRotatef(270.0f, 1.0f, 0.0f, 0.0f);

	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_base);
	gluCylinder(quadric, b, b, 0.3f, 50, 50);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glColor3f(0.7f, 0.7f, 0.7f);
	glPushMatrix();
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	sceneThreeCircle(0.5f);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 0.3f, 0.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	sceneThreeCircle(0.5f);
	glPopMatrix();

	glColor3f(1.0f, 1.0f, 1.0f);

}

void sceneThreeVillage_trees(float b) {

	// 1 tree
	glPushMatrix();
	glTranslatef(1.0f, -0.25f, -6.0f);
	glPushMatrix();
	glTranslatef(-2.5f, 0.5f - b, 15.0f);
	glScalef(1.5f, 1.5f, 1.5f);
	glPushMatrix();
	sceneThreeCallToBranch(0.0f, 0.0f, 0.0f);
	glPopMatrix();

	glPushMatrix();
	//PointDebug();
	glTranslatef(0.0f, -(b * 0.60), 0.0f);
	sceneThreeBase(b);
	glPopMatrix();
	glPopMatrix();
	glPopMatrix();

	// 2 tree
	glPushMatrix();
	glTranslatef(8.0f, -0.25f, -15.0f);
	glPushMatrix();
	glTranslatef(2.5f, 0.5f - b, 0.0f);
	glScalef(1.5f, 1.5f, 1.5f);
	glPushMatrix();
	sceneThreeCallToBranch(0.0f, 0.0f, 0.0f);
	glPopMatrix();

	glPushMatrix();
	//PointDebug();
	glTranslatef(0.0f, -(b * 0.60), 0.0f);
	sceneThreeBase(b);
	glPopMatrix();
	glPopMatrix();
	glPopMatrix();

}

// CubeMap
void sceneThreeCubeMap() {

	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_front);

	glBegin(GL_QUADS);

	//front face
	//glColor3f(1.0f, 0.0f, 0.0f);	


	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, 0);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_right);

	glBegin(GL_QUADS);
	//right face
	//glColor3f(0.0f, 1.0f, 0.0f);
	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_right);

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glBindTexture(GL_TEXTURE_2D, 0);

	glEnd();


	//back face
	//glColor3f(0.0f, 0.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_back);

	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glBindTexture(GL_TEXTURE_2D, 0);

	glEnd();
	//left face
	//cyan color
	//glColor3f(0.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_left);

	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, 0);

	glEnd();


	//top surface
	//megeta color
	//glColor3f(1.0f, 0.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_top);

	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, 0);
	glEnd();

	//botttom surface
	//yellow color
	//glColor3f(1.0f, 1.0f, 0.0f);
	glBindTexture(GL_TEXTURE_2D, sceneThreeTexture_bottom);

	glBegin(GL_QUADS);

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, 0);

	glEnd();

}

int countForScene = 0;

void sceneTwoDisplay() {

	// variable declaration
	float b = 0.5f;

	int flag = 0;				// for home texture
	float width1 = 1.25;
	float height1 = 0.65f;
	float zLength1 = 1.0f;

	glClearColor(0.7f, 0.7f, 0.7f, 1.0f);

	// Fog
	glFogf(GL_FOG_START, sceneTwoUp); // How far away does the fog start
	glFogf(GL_FOG_END, sceneTwoDown); // How far away does the fog stop

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -6.0f);
	glRotatef(sceneTwoR, 0.0f, -1.0f, 0.0f);

	//if (z >= -12.400073) {

	gluLookAt(sceneTwoX, sceneTwoY, sceneTwoZ,
		sceneTwoX, sceneTwoY, sceneTwoZ - 100,
		0.0f, 1.0f, 0.0f);
	//fprintf(gpFile, "z = %f\n",z);

	// grass
	glPushMatrix();
	glScalef(40.0f, 0.0f, 50.0f);
	sceneTwoGround(2);
	glPopMatrix();


	// road
	glPushMatrix();
	glTranslatef(0.0f, 0.01f, 0.0f);
	glScalef(2.0f, 0.0f, 30.0f);
	sceneTwoGround(1);
	glPopMatrix();

	glPushMatrix();
	sceneTwoTrees(b);
	glPopMatrix();

	glPushMatrix();
	sceneTwoMark();
	glPopMatrix();

	glPushMatrix();
	sceneTwoTrees2D();
	glPopMatrix();

	glPushMatrix();
	sceneTwoTemple();
	glPopMatrix();

	glPushMatrix();
	sceneTwoTerrain();
	glPopMatrix();

	glPushMatrix();
	sceneTwoBoard();
	glPopMatrix();

	glPushMatrix();
	sceneTwoDisplayBirds();
	glPopMatrix();
}

void sceneThreeDisplay() {

	// variables
	//main room

	int flag = 0;				// for home texture
	float width1 = 1.25;
	float height1 = 0.65f;
	float zLength1 = 1.0f;
	float b = 0.5f;


	// code
	glClearColor(135.0f / 255.0f, 206.0 / 255.0f, 235.0 / 255.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -6.0f);

	//glClearColor(backR / 255.0f, backG / 255.0f, backB / 255.0f, 1.0f);

	if (sceneThreeX <= 3.0f) {
		gluLookAt(sceneThreeX, sceneThreeY, sceneThreeZ,
			3.0, 0.500, 11.500,
			0.0f, 1.0f, 0.0f);

	}
	else {

		gluLookAt(sceneThreeX, sceneThreeY, sceneThreeZ,
			3.0, 0.500, -11.500,
			0.0f, 1.0f, 0.0f);

	}
	/*gluLookAt(x, y, z,
			x, y, z - 10,
			0.0f, 1.0f, 0.0f);*/


	//glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	glPushMatrix();
	glTranslatef(2.0f, 3.2f, -2.0f);
	glScalef(100.0f, 4.0f, 100.0f);
	sceneThreeGround(1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(2.0f, 5.5f, -25.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glScalef(25.0f, 4.0f, 6.0f);
	sceneThreeGround(2);
	glPopMatrix();



	//glPushMatrix();
	//glScalef(100.0f, 100.0f, 100.0f);
	//cubeMap();
	//glPopMatrix();

	glPushMatrix();
	glTranslatef(7.0f, 0.0f, 2.0f);
	glRotatef(45.0f, 0.0f, -1.0f, 0.0f);
	sceneThreeHouse1();			// blue
	glPopMatrix();


	glPushMatrix();
	glRotatef(45.0f, 0.0f, 1.0f, 0.0f);
	glTranslatef(-4.0f, 0.0f, 4.0f);
	sceneThreeHouse2();			// chocklet
	glPopMatrix();

	glPushMatrix();
	glRotatef(45.0f, 0.0f, 1.0f, 0.0f);
	glTranslatef(-1.0f, -0.1f, 0.0f);
	sceneThreeHouse3();			// white
	glPopMatrix();

	glPushMatrix();
	glRotatef(45.0f, 0.0f, -1.4f, 0.0f);
	glTranslatef(3.0f, 0.0f, -6.0f);
	sceneThreeHouse4();			// green();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 0.0f, -8.0f);
	sceneThreeHome();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(4.0f, 0.0f, 5.5f);
	sceneThreeHouse5();			// yellow();
	glPopMatrix();


	glPushMatrix();
	glTranslatef(-10.0f, 0.0f, -2.0f);
	sceneThreeHouse2();			// choklet();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-6.0f, 0.0f, -2.0f);
	sceneThreeHouse4();			// green();
	glPopMatrix();

	glPushMatrix();
	glRotatef(45.0f, 0.0f, -1.0f, 0.0f);
	glTranslatef(6.0f, -0.1f, -13.0f);
	sceneThreeHouse3();			// white
	glPopMatrix();

	glPushMatrix();
	glRotatef(45.0f, 0.0f, -1.0f, 0.0f);
	glTranslatef(12.0f, -0.1f, 4.0f);
	sceneThreeHouse3();			// white
	glPopMatrix();

	// right x+ front
	glPushMatrix();
	glTranslatef(16.0f, -0.1f, 0.0f);
	glRotatef(45.0f, 0.0f, -1.0f, 0.0f);
	sceneThreeHouse3();			// white
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-5.0f, 0.0f, -10.0f);
	sceneThreeHouse2();			// choklet();
	glPopMatrix();


	// Ledy
	glPushMatrix();
	glTranslatef(2.8f, -0.4f, -6.0f - 8.0f);
	sceneThreeLeady();
	glPopMatrix();

	// Cart back
	glPushMatrix();
	glTranslatef(-0.4f, -0.1f, -10.0f);
	glRotatef(140.0f, 0.0f, 1.0f, 0.0f);
	glScalef(0.90f, 0.70f, 0.80f);
	sceneThreeCart();
	glPopMatrix();

	//Cart front
	glPushMatrix();
	glTranslatef(8.6f, -0.1f, 6.0f);
	glRotatef(40.0f, 0.0f, 1.0f, 0.0f);
	glScalef(0.90f, 0.70f, 0.80f);
	sceneThreeCart();
	glPopMatrix();


	//Tree
	glPushMatrix();
	sceneThreeVillage_trees(b);
	glPopMatrix();

	//// Terrain
	glPushMatrix();
	sceneThreeTerrain();
	glPopMatrix();

}

void drawText(char* szText) {

	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	GLfloat materialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };

	glEnable(GL_LIGHTING);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
	glListBase(nFontList);
	glCallLists(strlen(szText), GL_UNSIGNED_BYTE, szText);
	glPopMatrix();
	glDisable(GL_LIGHTING);
}


void sceneZeroDisplay() {
	
	lightDiffuse[0] = lightDiffuseValue;
	lightDiffuse[1] = lightDiffuseValue;
	lightDiffuse[2] = lightDiffuseValue;
	if(lightDiffuseValue <= 1.0)
		lightDiffuseValue += 0.001f;
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);

	glPushMatrix();
	glTranslatef(-3.5f, 0.0f, 0.0f);
	drawText("ASTROMEDICOMP");
	glTranslatef(0.0f, -1.0f, 0.0f);
	drawText("Presents");
	glPopMatrix();

}

void sceneTitle() {
	
	glPushMatrix();
	glTranslatef(0.2f, -0.1f, 0.0f);
	glScalef(1.50f, 1.50f, 1.50f);
	glBindTexture(GL_TEXTURE_2D, texture_title);
	glBegin(GL_QUADS);
	//front face
	//glColor3f(1.0f, 0.0f, 0.0f);	

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

}

// Font 
// End Credit
void developedCredit() {

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glPushMatrix();
	glTranslatef(devX, devY, devZ);
	glScalef(0.75f, 0.75f, 0.75f);
	drawText("Developed By ");
	glPopMatrix();

	glPushMatrix();
	glTranslatef(nameX, nameY, nameZ);
	glScalef(0.75f, 0.75f, 0.75f);
	drawText("Abhishek Laxman Shirke");
	glPopMatrix();
	//glTranslatef(0.0f, -1.0f, 0.0f);
	//drawText("Presents");
	glPopMatrix();

}

void technologyCredit() {

	void SetupRC(HDC hDC, char* fontName);

	SetupRC(ghdc, "Black Pro");
	glPushMatrix();
	glTranslatef(techX, techY, techZ);
	glScalef(0.40f, 0.40f, 0.40f);
	//drawText("Technology Used .");
	glTranslatef(-4.0f, -0.7f, 0.0f);
	drawText("Windowing												: Win32 SDK");
	glTranslatef(0.0f, -0.7f, 0.0f);
	drawText("Rendering");
	glTranslatef(7.85f, 00.0f, 0.0f);
	drawText(": OpenGL");
	glTranslatef(-7.85f, -0.7f, 0.0f);
	drawText("Language");
	glTranslatef(7.85f, 00.0f, 0.0f);
	drawText(": C");
	glTranslatef(-7.85f, -0.7f, 0.0f);
	drawText("Data Structure Used");
	glTranslatef(7.85f, 00.0f, 0.0f);
	drawText(": LinkedList");
	glTranslatef(-7.85f, -0.7f, 0.0f);
	drawText("Group Name");
	glTranslatef(7.85f, 0.0f, 0.0f);
	drawText(": Frustum");
	glTranslatef(-7.85f, -0.7f, 0.0f);
	drawText("Group Leader");
	glTranslatef(7.85f, 0.0f, 0.0f);
	drawText(": Hrituja Hedau");
	glTranslatef(-7.85f, -0.7f, 0.0f);
	drawText("Narration By");
	glTranslatef(7.85f, 0.0f, 0.0f);
	drawText(": Vaibhavi Dhopate");


	//glTranslatef(0.0f, -0.7f, 0.0f);
	//drawText("L");
	glPopMatrix();

}

float thanksLightVal = 0.0f;
void specialThanks() {
	void SetupRC(HDC hDC, char* fontName);

	SetupRC(ghdc, "Black Pro");
	lightDiffuse[0] = thanksLightVal;
	lightDiffuse[1] = thanksLightVal;
	lightDiffuse[2] = thanksLightVal;
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);

	glTranslatef(-1.3f, 1.0f, 0.0f);
	glPushMatrix();
	glScalef(0.40f, 0.40f, 0.40f);
	drawText("Special Thanks");
	glTranslatef(-1.0f, -1.0f, 0.0f);
	glScalef(1.20f, 1.20f, 1.20f);
	drawText("Dr. Vijay D. Gokhale Sir");
	glTranslatef(0.0f, -0.7f, 0.0f);
	drawText("Dr. Rama V. Gokhale Madam");
	glTranslatef(0.0f, -0.7f, 0.0f);
	drawText("Mr. Shashikant D. Bagal Sir");
	glTranslatef(0.0f, -0.7f, 0.0f);
	drawText("Pradnya V. Gokhale Madam");
	glTranslatef(0.0f, -0.7f, 0.0f);
	drawText("Hrituja R. Hedau");
	glTranslatef(0.0f, -0.7f, 0.0f);
	drawText("Vaibhavi A. Dhopate");

	glPopMatrix();
}


void display(void) {
	
	// variables
	//main room

	int flag = 0;				// for home texture
	float width1 = 1.25;
	float height1 = 0.65f;
	float zLength1 = 1.0f;
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);
	
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -6.0f);
	
	if (sceneTitleDisplayFLag == 1) {
		
		sceneTitle();
	
	} else if (sceneZeroDisplayFlag == 1) {
		
		sceneZeroDisplay();
		
	} else if (sceneOneDisplayFlag == 1) {
		
		sceneOneDisplay();
	
	} else if (sceneTwoRoleDisplayFlag == 1) {

		sceneTwoRoleDisplay();


	} else if (sceneTwoDisplayFlag == 1) {
	
		//Scene Two
		glEnable(GL_FOG);
		sceneTwoDisplay();
	
	} else if (sceneThreeDisplayFlag == 1) {
		
		// Scene Three
		glDisable(GL_FOG);
		sceneThreeDisplay();
	
	} else if (nameCreditFlag == 1) {
		
		glPushMatrix();
		developedCredit();
		glPopMatrix();
	} else if (technologyCreditFlag == 1) {

		glPushMatrix();
		technologyCredit();
		glPopMatrix();

	}
	else if (specialThanksFlag == 1) {

		glPushMatrix();
		specialThanks();
		glPopMatrix();

	} else if (scenesEndFlag == 1) {
		
		glPushMatrix();
		scenesEnd();
		glPopMatrix();

	
	}
	else if (sceneReturnRoleDisplayFlag == 1) {
		
		glPushMatrix();
		sceneReturnRoleDisplay();
		glPopMatrix();
	
	}
	
	SwapBuffers(ghdc);


}


float updatez = 0.0f;
int bendCount = 0;


int zeroSceneEndCount = 0;
int titleEndCount = 0;
int firstSceneEndCount = 0;
int scensEndCount = 0;
int secondSceneStartCount = 0;
int secondSceneEndCount = 0;
int thirdSceneEndCount = 0;
int technologyCreditEndCount = 0;
int nameCreditCount = 0;
int specialThanksCount = 0;
int sceneReturnEndCount = 0;

void update(void) {

	// code

	if (sceneZeroDisplayFlag == 1) {

		if (lightDiffuseValue >= 1.0f) {

			zeroSceneEndCount++;

		}

		if (zeroSceneEndCount == 50) {

			sceneZeroDisplayFlag = 0;
			sceneTitleDisplayFLag = 1;


		}

	}


	if (sceneTitleDisplayFLag == 1) {

		titleEndCount++;

		if (titleEndCount == 170) {

			sceneTitleDisplayFLag = 0;
			sceneOneDisplayFlag = 1;

		}

	}


	if (sceneOneDisplayFlag == 1) {
		if (sceneOneFullBodyMove < -4.549979 && sceneOneStartKeyFlag == 1) {

			//fprintf(gpFile, "%f\n", fullBodyMove);
			sceneOnez -= 0.007f;
			//fprintf(gpFile, "z = %f\n", z);

		}

		if (sceneOneStartKeyFlag == 1 && sceneOneFullBodyMove < 4.22f) {

			if (sceneOneFullBodyMove > 2.0)
				sceneOneFullBodyMove = sceneOneFullBodyMove + 0.008f;
			else {
				sceneOneFullBodyMove = sceneOneFullBodyMove + 0.01f;
			}

			if (sceneOneUpperLeg < 40) {
				sceneOneUpperLeg = (sceneOneUpperLeg + 1) % 360;
				sceneOneShoulderL = (sceneOneShoulderL - 1) % 360;
				sceneOneShoulderR = (sceneOneShoulderR + 1) % 360;
				sceneOneElbowL = (sceneOneElbowL - 1) % 360;
				sceneOneElbowR = (sceneOneElbowR - 1) % 360;
				if (sceneOneLowerLeg < 40)
					sceneOneLowerLeg = ((sceneOneLowerLeg + 3) % 360);

			}
			else {

				sceneOneUpperLeg = -40;
				sceneOneLowerLeg = 0;
				sceneOneShoulderL = 40;
				sceneOneElbowL = 0;

				sceneOneShoulderR = -40;
				sceneOneElbowR = 0;

			}
		}

		if (sceneOneZFLag == 1) {


			sceneOnex -= updatez;

			if (sceneOnex < 10.0f) {
				updatez -= 0.007f;
			}



		}

		fprintf(gpFile, "fullBodyMove = %f\n", sceneOneFullBodyMove);
		if (sceneOneFullBodyMove > 1.0 && sceneOneFullBodyMove < 4.0f) {

			sceneOneZBodyMove -= 0.004f;
			sceneOneWholeBodyAngle += 0.2;
			//fprintf(gpFile, "wholeBodyAngle = %f\n", wholeBodyAngle);

		}

		if (sceneOneFullBodyMove > 4.22f) {
			// for starting to bend
			bendCount++;

		}
		if (bendCount > 70) {

			sceneOneBendFlag = 1;
		}

		if (sceneOneBendFlag == 1) {

			if (sceneOneBendAngle < 53.0f)
				sceneOneBendAngle += 0.1f;
			else {

				firstSceneEndCount++;

			}

			sceneOneUpperLeg = -20.0f;
			sceneOneLowerLeg = -20.0f;

			sceneOneShoulderL = -40;
			sceneOneShoulderR = -60;
			sceneOneElbowL = -40;
			sceneOneElbowR = -60;

			sceneOneWholeBodyAngle = 10.0f;
			//wholeBodyAngle = -5.0f;


		}

		if (firstSceneEndCount == 70) {

			sceneOneDisplayFlag = 0;
			sceneTwoRoleDisplayFlag = 1;

		}

		if (busX >= -11.0f)
			busX -= 0.05f;

		fprintf(gpFile, "busX = %f\n", busX);
		if (busX <= -6.999991) {
			
			sceneOneStartKeyFlag = 1;
		
		}

		fprintf(gpFile, "sceneOneFullBodyMove = %f | sceneOneStartKeyFlag %d | sceneOnez %f| sceneOneZFLag %d | sceneOnex %f | updatez %f | bendCount %d| sceneOneZBodyMove = %f\n\n\n", sceneOneFullBodyMove, sceneOneStartKeyFlag, sceneOnez, sceneOneZFLag, sceneOnex, updatez, bendCount, sceneOneZBodyMove);
	}

	if (scenesEndFlag == 1) {


		if (sceneOneFullBodyMove < -4.549979 && sceneOneStartKeyFlag == 1) {

			//fprintf(gpFile, "%f\n", fullBodyMove);
			sceneOnez -= 0.007f;
			//fprintf(gpFile, "z = %f\n", z);

		}

		if (sceneOneStartKeyFlag == 1 && sceneOneFullBodyMove < 4.22f) {

			if (sceneOneFullBodyMove > 2.0)
				sceneOneFullBodyMove = sceneOneFullBodyMove + 0.008f;
			else {
				sceneOneFullBodyMove = sceneOneFullBodyMove + 0.01f;
			}

			if (sceneOneUpperLeg < 40) {
				sceneOneUpperLeg = (sceneOneUpperLeg + 1) % 360;
				sceneOneShoulderL = (sceneOneShoulderL - 1) % 360;
				sceneOneShoulderR = (sceneOneShoulderR + 1) % 360;
				sceneOneElbowL = (sceneOneElbowL - 1) % 360;
				sceneOneElbowR = (sceneOneElbowR - 1) % 360;
				if (sceneOneLowerLeg < 40)
					sceneOneLowerLeg = ((sceneOneLowerLeg + 3) % 360);

			}
			else {

				sceneOneUpperLeg = -40;
				sceneOneLowerLeg = 0;
				sceneOneShoulderL = 40;
				sceneOneElbowL = 0;

				sceneOneShoulderR = -40;
				sceneOneElbowR = 0;

			}
		}

		if (sceneOneZFLag == 1) {


			sceneOnex -= updatez;

			if (sceneOnex < 10.0f) {
				updatez -= 0.007f;
			}



		}

		fprintf(gpFile, "fullBodyMove = %f\n", sceneOneFullBodyMove);
		if (sceneOneFullBodyMove > 1.0 && sceneOneFullBodyMove < 4.0f) {

			sceneOneZBodyMove -= 0.004f;
			sceneOneWholeBodyAngle += 0.2;
			//fprintf(gpFile, "wholeBodyAngle = %f\n", wholeBodyAngle);

		}

		if (sceneOneFullBodyMove > 4.22f) {
			// for starting to bend
			bendCount++;

		}
		if (bendCount > 70) {

			sceneOneBendFlag = 1;
		}

		if (sceneOneBendFlag == 1) {

			if (sceneOneBendAngle < 53.0f)
				sceneOneBendAngle += 0.1f;
			else {

				scensEndCount++;

			}

			sceneOneUpperLeg = -20.0f;
			sceneOneLowerLeg = -20.0f;

			sceneOneShoulderL = -40;
			sceneOneShoulderR = -60;
			sceneOneElbowL = -40;
			sceneOneElbowR = -60;

			sceneOneWholeBodyAngle = 10.0f;
			//wholeBodyAngle = -5.0f;


		}

		if (scensEndCount == 300) {

			scenesEndFlag = 0;
			nameCreditFlag = 1;

		}

	}
	if (sceneTwoRoleDisplayFlag == 1) {

		if (sceneTwoImagex >= -3.0f) {

			sceneTwoImagex -= 0.01f;

		}

		if (sceneTwoImagex <= -3.0) {

			secondSceneStartCount++;


		}

		if (secondSceneStartCount == 10) {

			sceneTwoRoleDisplayFlag = 0;
			sceneTwoDisplayFlag = 1;

		}

	}
	if (sceneTwoDisplayFlag == 1) {
		fprintf(gpFile, "sceneTwoRoleDisplayFlag = %d\n", sceneTwoRoleDisplayFlag);

		if (sceneTwoZ >= -14) {

			sceneTwoZ -= 0.02f;

		}

		if (sceneTwoZ <= -12.400073) {

			if (sceneTwoR < 270.0f && sceneTwoRotateFLag == 0)
				sceneTwoR += 0.5f;
			else {
				sceneTwoRotateFLag = 1;
				fprintf(gpFile, "r = %f\n", sceneTwoR);
				sceneTwoR = 270.0f;
			}
		}

		if (sceneTwoRotateFLag == 1) {

			if (sceneTwoX < 10.0f)
				sceneTwoX += 0.03f;

			if (sceneTwoX >= 10.0f) {

				secondSceneEndCount++;
				fprintf(gpFile, "secondSceneEndCount = %d\n", secondSceneEndCount);

			}
		}



		if (sceneTwoBirdZ <= 30.0f && sceneTwoThreeBirdDisplayFlag == 1) {
			sceneTwoBirdZ -= 0.05f;

			if (sceneTwoBirdZ >= 29.30f) {

				sceneTwoThreeBirdDisplayFlag = 0;
			}

		}

		if (sceneTwoBirdRotate > -270.0f) {

			sceneTwoBirdRotate -= 0.2f;

		}


		//if (mainBirdX >= 4.649998 && mainBirdY <= 0.850002 && mainBirdZ <= -0.199906) {


		if (sceneTwoMainBirdX <= 4.649998 || sceneTwoMainBirdY >= 0.850002 || sceneTwoMainBirdZ >= -0.199906) {


			sceneTwoMainBirdX += 0.0107f;
			sceneTwoMainBirdY -= 0.003f;
			sceneTwoMainBirdZ -= 0.036f;

			if (sceneTwoWingFlag > 0.0f && sceneTwoFlagBird == 0) {

				sceneTwoWingFlag -= 0.05f;

				if (sceneTwoWingFlag <= 0.01)
					sceneTwoFlagBird = 1;

			}
			else {

				sceneTwoWingFlag += 0.05f;
				if (sceneTwoWingFlag >= 1.3)
					sceneTwoFlagBird = 0;

			}

		}

		if (secondSceneEndCount == 70) {

			sceneTwoDisplayFlag = 0;
			//sceneTwoRoleDisplayFlag = 1;
			sceneThreeDisplayFlag = 1;

		}

	}

	if (sceneThreeDisplayFlag == 1) {


		// code
		if (sceneThreeX < 3.0f)
			sceneThreeX += 0.02f;
		if (sceneThreeX > 3.0f && sceneThreeZ >= -11.0) {

			sceneThreeZ -= 0.03f;
			/*backR = 254.0 / 255.0f;
			backG = 105.0 / 255.0f;
			backB = 0.5f / 255.0f;*/


			fprintf(gpFile, "r = %f g = %f b = %f\n", sceneThreeBackR, sceneThreeBackG, sceneThreeBackB);

		}

		if (sceneThreeZ <= -11.0 && sceneThreeBagColorFLag == 0) {

			if (sceneThreeBackR <= 254.0) {

				sceneThreeBackR += 0.8;
			}

			if (sceneThreeBackG >= 105.0) {

				sceneThreeBackG -= 0.3f;
			}

			if (sceneThreeBackB >= 0.0) {

				sceneThreeBackB -= 0.8f;
			}

			if (sceneThreeBackR >= 254.200 && sceneThreeBackG <= 117.799 && sceneThreeBackB <= -0.200) {

				sceneThreeBagColorFLag = 1;


			}

			fprintf(gpFile, "backR = %f backG = %f backB = %f\n", sceneThreeBackR, sceneThreeBackG, sceneThreeBackB);

			fprintf(gpFile, "bagColorFLag = %d\n", sceneThreeBagColorFLag);


			fprintf(gpFile, "z = %f\n", sceneThreeZ);


		}

		if (sceneThreeBagColorFLag == 1) {

			if (sceneThreeBagColorCount < 150)
				sceneThreeBagColorCount++;

		}

		if (sceneThreeBagColorCount == 150) {

			if (sceneThreeBackR >= 0.0 || sceneThreeBackG >= 0.0 || sceneThreeBackB >= 0.0f || sceneThreeBlueR > 0.0f || sceneThreeBlueG >= 0.0f || sceneThreeBlueB >= 0.0f) {
				sceneThreeBackR -= 1.5f;
				sceneThreeBackG -= 1.0f;
				sceneThreeBackB -= 1.0;

				sceneThreeBlueR -= 1.5f;
				sceneThreeBlueG -= 1.5f;
				sceneThreeBlueB -= 1.5f;

			}
			else {

				thirdSceneEndCount++;
				fprintf(gpFile, "thirdSceneEndCount = %d\n", thirdSceneEndCount);

			}


		}


		if (thirdSceneEndCount == 150) {

			sceneThreeDisplayFlag = 0;
			//sceneOneFullBodyMove= 4.224184;
			sceneOneFullBodyMove = 4.224184;
			sceneOneStartKeyFlag = 1;
			sceneOnez = -3.000000;
			sceneOneZFLag = 1;
			sceneOnex = 10.004985;
			updatez = -5.004985;
			bendCount = 487;
			busX = -11.0f;
			sceneOneBendAngle = 54.0f;
			sceneOneZBodyMove = -1.399996;

			//sceneOneDisplayFlag = 1;
			sceneReturnRoleDisplayFlag = 1;
			//scenesEndFlag = 1;
			//sceneOneDisplayFlag = 1;
			//nameCreditFlag = 1;

		}
	}

	if (sceneReturnRoleDisplayFlag == 1) {
		
		if (sceneReturnImagex >= -11.0f) {

			sceneReturnImagex -= 0.03f;

		}

		if (sceneReturnImagex <= -11.0) {

			sceneReturnEndCount++;


		}

		if (sceneReturnEndCount == 10) {


			sceneReturnRoleDisplayFlag = 0;
			//sceneOneFullBodyMove= 4.224184;
			sceneOneFullBodyMove = 4.224184;
			sceneOneStartKeyFlag = 1;
			sceneOnez = -3.000000;
			sceneOneZFLag = 1;
			sceneOnex = 10.004985;
			updatez = -5.004985;
			bendCount = 487;
			busX = -11.0f;
			sceneOneBendAngle = 54.0f;
			sceneOneZBodyMove = -1.399996;

			//sceneOneDisplayFlag = 1;
			scenesEndFlag = 1;
			//sceneTwoRoleDisplayFlag = 0;
			//sceneTwoDisplayFlag = 1;

		}
	
	}
	
	if (nameCreditFlag == 1) {
		
		if (devX <= -3.5f)
			devX += 0.04;

		if (nameX >= -3.3 && devX >= -3.5f) {
			nameX -= 0.04f;
		}
		if (nameX <= -3.3) {

			nameCreditCount++;

		}

		if (nameCreditCount == 50) {
			technologyCreditFlag = 1;
			nameCreditFlag = 0;
		}
	
	}

	if (technologyCreditFlag == 1) {
		
		if (techY <= 1.0f) {

			techY += 0.05f;

		}

		if (techY >= 1.0f) {

			technologyCreditEndCount++;

		}


		if (technologyCreditEndCount == 70) {

			specialThanksFlag = 1;
			technologyCreditFlag = 0;

		}
	
	}

	if (specialThanksFlag == 1) {

		if (thanksLightVal <= 1.0f) {

			thanksLightVal += 0.0f;
			fprintf(gpFile, "thanksLightVal = %f\n", thanksLightVal);
		}

		if (thanksLightVal >= 0) {

			specialThanksCount++;

		}

		if (specialThanksCount == 150) {
			
			specialThanksFlag = 0;
		
		}

	}

}

void uninitialize(void) {

	// Variable declaration
	int linkedListNode = 0;
	int temp_linkedListNode = 0;

	// function declarations
	void ToggleFullScreen(void);

	// code
	if (gbFullScreen) {

		ToggleFullScreen();

	}

	if (wglGetCurrentContext() == ghrc) {

		wglMakeCurrent(NULL, NULL);

	}

	if (ghrc) {

		wglDeleteContext(ghrc);
		ghrc = NULL;

	}

	if (ghdc) {

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

	}


	if (ghwnd) {

		DestroyWindow(ghwnd);
		ghwnd = NULL;

	}
	// Delete The Textures
	if (texture_building1) {

		glDeleteTextures(1, &texture_building1);
	}

	if (texture_building2) {

		glDeleteTextures(1, &texture_building2);
	}

	if (texture_building3) {

		glDeleteTextures(1, &texture_building3);
	}


	if (texture_building4) {

		glDeleteTextures(1, &texture_building4);
	}
	
	if (texture_building5) {

		glDeleteTextures(1, &texture_building5);
	}

	
	if (texture_building6) {

		glDeleteTextures(1, &texture_building6);
	
	}

	if (texture_building7) {

		glDeleteTextures(1, &texture_building7);

	}

	if (texture_cg1) {

		glDeleteTextures(1, &texture_cg1);

	}

	if (texture_cg2) {

		glDeleteTextures(1, &texture_cg2);

	}

	if (texture_shirt) {

		glDeleteTextures(1, &texture_shirt);

	}

	if (texture_pant) {

		glDeleteTextures(1, &texture_pant);

	}

	if (texture_head) {

		glDeleteTextures(1, &texture_head);

	}

	// c1
	if (sceneOneTexture_shirt1) {

		glDeleteTextures(1, &sceneOneTexture_shirt1);

	}

	if (sceneOneTexture_pant1) {

		glDeleteTextures(1, &sceneOneTexture_pant1);

	}

	if (sceneOneTexture_shirt2) {

		glDeleteTextures(1, &sceneOneTexture_pant1);

	}


	if (sceneOneTexture_pant2) {

		glDeleteTextures(1, &sceneOneTexture_pant2);

	}

	if (sceneOneTexture_shirt3) {

		glDeleteTextures(1, &sceneOneTexture_shirt3);

	}

	if (sceneOneTexture_pant3) {

		glDeleteTextures(1, &sceneOneTexture_pant3);

	}

	if (sceneOneTexture_shirt4) {

		glDeleteTextures(1, &sceneOneTexture_shirt4);

	}

	if (sceneOneTexture_pant4) {

		glDeleteTextures(1, &sceneOneTexture_pant4);

	}

	if (sceneOneTexture_bioscope) {

		glDeleteTextures(1, &sceneOneTexture_bioscope);

	}

	if (sceneOneTexture_woodbar) {

		glDeleteTextures(1, &sceneOneTexture_woodbar);

	}

	if (sceneOneTexture_bioscope_poster) {

		glDeleteTextures(1, &sceneOneTexture_bioscope_poster);

	}

	if (sceneTwoTexture_road) {

		glDeleteTextures(1, &sceneTwoTexture_road);

	}

	if (sceneTwoTexture_grass) {

		glDeleteTextures(1, &sceneTwoTexture_grass);

	}

	if (sceneTwoTexture_leaf) {

		glDeleteTextures(1, &sceneTwoTexture_leaf);

	}

	if (sceneTwoTexture_trunk) {

		glDeleteTextures(1, &sceneTwoTexture_trunk);

	}

	if (sceneTwoTexture_base) {

		glDeleteTextures(1, &sceneTwoTexture_base);

	}

	if (sceneTwoTexture_mark) {

		glDeleteTextures(1, &sceneTwoTexture_mark);

	}

	if (sceneTwoTexture_pole) {

		glDeleteTextures(1, &sceneTwoTexture_pole);

	}

	if (sceneTwoTexture_stares) {

		glDeleteTextures(1, &sceneTwoTexture_stares);

	}

	if (sceneTwoTexture_top) {

		glDeleteTextures(1, &sceneTwoTexture_top);

	}

	if (sceneTwoTexture_upper) {

		glDeleteTextures(1, &sceneTwoTexture_upper);

	}

	if (sceneTwoTexture_god) {

		glDeleteTextures(1, &sceneTwoTexture_god);

	}

	if (sceneTwoTexture_side) {

		glDeleteTextures(1, &sceneTwoTexture_side);

	}

	if (sceneTwoTexture_water) {

		glDeleteTextures(1, &sceneTwoTexture_water);

	}

	if (sceneTwoTexture_Leaves) {

		glDeleteTextures(1, &sceneTwoTexture_Leaves);

	}

	if (sceneTwoTexture_woodbar) {

		glDeleteTextures(1, &sceneTwoTexture_woodbar);

	}

	if (sceneTwoTexture_bioscope) {

		glDeleteTextures(1, &sceneTwoTexture_bioscope);

	}

	if (sceneTwoTexture_bird_middle) {

		glDeleteTextures(1, &sceneTwoTexture_bird_middle);

	}

	if (villageBoard) {

		glDeleteTextures(1, &villageBoard);

	}

	if (texture_leady) {

		glDeleteTextures(1, &texture_leady);

	}

	if (sceneThreeTexture_Fence) {

		glDeleteTextures(1, &sceneThreeTexture_Fence);

	}

	if (sceneThreeTexture_roof) {

		glDeleteTextures(1, &sceneThreeTexture_roof);

	}

	if (sceneThreeTexture_room) {

		glDeleteTextures(1, &sceneThreeTexture_room);

	}

	if (sceneThreeTexture_room1) {

		glDeleteTextures(1, &sceneThreeTexture_room1);

	}

	if (sceneThreeTexture_wall1) {

		glDeleteTextures(1, &sceneThreeTexture_wall1);

	}

	if (sceneThreeTexture_wall2) {

		glDeleteTextures(1, &sceneThreeTexture_wall2);

	}

	if (sceneThreeTexture_h2s) {

		glDeleteTextures(1, &sceneThreeTexture_h2s);

	}

	if (sceneThreeTexture_h2md) {

		glDeleteTextures(1, &sceneThreeTexture_h2md);

	}

	if (sceneThreeTexture_h3s) {

		glDeleteTextures(1, &sceneThreeTexture_h3s);

	}

	if (sceneThreeTexture_h3md) {

		glDeleteTextures(1, &sceneThreeTexture_h3md);

	}

	if (sceneThreeTexture_h4s) {

		glDeleteTextures(1, &sceneThreeTexture_h4s);

	}

	if (sceneThreeTexture_h4md) {

		glDeleteTextures(1, &sceneThreeTexture_h4md);

	}


	if (sceneThreeTexture_h5s) {

		glDeleteTextures(1, &sceneThreeTexture_h5s);

	}

	if (sceneThreeTexture_h5md) {

		glDeleteTextures(1, &sceneThreeTexture_h5md);

	}

	if (sceneThreeTexture_h5sf) {

		glDeleteTextures(1, &sceneThreeTexture_h5sf);

	}	

	if (sceneThreeTexture_g1) {

		glDeleteTextures(1, &sceneThreeTexture_g1);

	}

	if (sceneThreeTexture_Fence) {

		glDeleteTextures(1, &sceneThreeTexture_Fence);

	}

	if (sceneThreeTexture_roof) {

		glDeleteTextures(1, &sceneThreeTexture_roof);

	}

	if (sceneThreeTexture_room) {

		glDeleteTextures(1, &sceneThreeTexture_room);

	}

	if (sceneThreeTexture_room1) {

		glDeleteTextures(1, &sceneThreeTexture_room1);

	}

	if (sceneThreeTexture_sadi) {

		glDeleteTextures(1, &sceneThreeTexture_sadi);

	}

	if (sceneThreeTexture_sadi_lower) {

		glDeleteTextures(1, &sceneThreeTexture_sadi_lower);

	}

	if (sceneThreeTexture_sadi_triangle) {

		glDeleteTextures(1, &sceneThreeTexture_sadi_triangle);

	}

	if (sceneThreeTexture_arm) {

		glDeleteTextures(1, &sceneThreeTexture_arm);

	}

	if (sceneThreeTexture_bars) {

		glDeleteTextures(1, &sceneThreeTexture_bars);

	}


	if (sceneThreeTexture_cart_base) {

		glDeleteTextures(1, &sceneThreeTexture_cart_base);

	}

	if (sceneThreeTexture_bar) {

		glDeleteTextures(1, &sceneThreeTexture_bar);

	}

	if (sceneThreeTexture_trunk) {

		glDeleteTextures(1, &sceneThreeTexture_trunk);

	}

	if (sceneThreeTexture_base) {

		glDeleteTextures(1, &sceneThreeTexture_base);

	}

	if (sceneThreeTexture_Leaves) {

		glDeleteTextures(1, &sceneThreeTexture_Leaves);

	}

	if (sceneThreeTexture_right) {

		glDeleteTextures(1, &sceneThreeTexture_right);

	}

	if (sceneThreeTexture_top) {

		glDeleteTextures(1, &sceneThreeTexture_top);

	}

	if (sceneThreeTexture_front) {

		glDeleteTextures(1, &sceneThreeTexture_front);

	}

	if (sceneThreeTexture_left) {

		glDeleteTextures(1, &sceneThreeTexture_left);

	}

	if (sceneThreeTexture_left) {

		glDeleteTextures(1, &sceneThreeTexture_left);

	}

	if (sceneThreeTexture_bottom) {

		glDeleteTextures(1, &sceneThreeTexture_bottom);

	}

	if (sceneThreeTexture_back) {

		glDeleteTextures(1, &sceneThreeTexture_back);

	}

	glDeleteTextures(1, &texture_building1);
	glDeleteTextures(1, &texture_building2);

	linkedListNode = countNode();
	while (linkedListNode) {
		
		deleteNode(linkedListNode);
		linkedListNode -= 1;
	
	}

	if (gpFile) {

		fprintf(gpFile, "Log File Successfully Closed\n");
		fclose(gpFile);
		gpFile = NULL;

	}

}

BOOL LoadGLTexture(GLuint* texture, TCHAR ImageResourceID[]) {

	
	// variable declaration

	HBITMAP hBitMap = NULL;
	BITMAP bmp;
	BOOL bResult = FALSE;

	// code
	hBitMap = (HBITMAP)LoadImage(GetModuleHandle(NULL),
		ImageResourceID,
		IMAGE_BITMAP,
		0,
		0,
		LR_CREATEDIBSECTION);

	if (hBitMap) {
	
		bResult = TRUE;
		GetObject(hBitMap, sizeof(bmp), &bmp);

		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

		glGenTextures(1, texture);

		glBindTexture(GL_TEXTURE_2D, *texture);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		// create the texture
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);

		glBindTexture(GL_TEXTURE_2D, 0);

		DeleteObject(hBitMap);

	}
	
	return bResult;
}

BOOL NewLoadGLTexture(GLuint* texture, const char* filename)
{
	// Variable declaration
	int width, height;
	int image_type;

	unsigned char* textureImageData;
	BOOL bResult = FALSE;

	// code
	textureImageData = stbi_load(filename, &width, &height, &image_type, 0);
	if (textureImageData)
	{
		fprintf(gpFile, "Image Laoded\n");

		bResult = TRUE;
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		if (image_type == 4)
		{
			fprintf(gpFile, "Image is RGBA type\n");
			gluBuild2DMipmaps(GL_TEXTURE_2D, 4, width, height, GL_RGBA, GL_UNSIGNED_BYTE, textureImageData);
			glBindTexture(GL_TEXTURE_2D, 0);
		}
		else if (image_type == 3)
		{
			fprintf(gpFile, "Image is RGB type\n");
			gluBuild2DMipmaps(GL_TEXTURE_2D, 3, width, height, GL_RGB, GL_UNSIGNED_BYTE, textureImageData);
			glBindTexture(GL_TEXTURE_2D, 0);
		}
	}
	else
	{
		fprintf(gpFile, "Image Not Laoded\n");
		return FALSE;
	}
	return bResult;
}

void SetupRC(HDC hDC, char* fontName) {
	// Set up the font characteristics
	HFONT hFont;
	GLYPHMETRICSFLOAT agmf[128]; // Throw away
	LOGFONT logfont;

	logfont.lfHeight = -10;
	logfont.lfWidth = 0;
	logfont.lfEscapement = 0;
	logfont.lfOrientation = 0;
	logfont.lfWeight = FW_BOLD;
	logfont.lfItalic = FALSE;
	logfont.lfUnderline = FALSE;
	logfont.lfStrikeOut = FALSE;
	logfont.lfCharSet = ANSI_CHARSET;
	logfont.lfOutPrecision = OUT_DEFAULT_PRECIS;
	logfont.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	logfont.lfQuality = DEFAULT_QUALITY;
	logfont.lfPitchAndFamily = DEFAULT_PITCH;
	//strcpy(logfont.lfFaceName, "Arial");
	//strcpy(logfont.lfFaceName, "Algerian");
	strcpy(logfont.lfFaceName, fontName);
	// Create the font and display list
	hFont = CreateFontIndirect(&logfont);
	SelectObject(hDC, hFont);
	// Create display lists for glyphs 0 through 128 with 0.1 extrusion
	// and default deviation. The display list numbering starts at 1000
	// (it could be any number).
	nFontList = glGenLists(128);
	wglUseFontOutlines(hDC, 0, 128, nFontList, 0.0f, 0.2f,
		WGL_FONT_POLYGONS, agmf);
	DeleteObject(hFont);
}


