#pragma once

struct Node {

	float x;
	float y;
	float z;
	struct Node* next;

};

extern struct Node* createNode(float x, float y, float z);
extern void addNode(float x, float y, float z);
extern int countNode();
extern int deleteNode(int count);
extern struct Node* getNode(int index);
extern void displayNode();


