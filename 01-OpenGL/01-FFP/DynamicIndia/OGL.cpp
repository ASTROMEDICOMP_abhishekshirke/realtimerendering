// header files
#include <windows.h>
#include "OGL.h"

#include <stdio.h>				//for file io functions
#include <stdlib.h>				//for exit()
#include <math.h>


// OpenGL header files
#include <GL/gl.h>
#include <GL/glu.h>


// OpenGL Libraries
#pragma comment (lib, "OpenGL32.lib")
#pragma comment (lib, "glu32.lib")
#pragma comment (lib, "winmm.lib")




#define WIN_WIDTH	800			
#define WIN_HEIGHT	600
#define PI			3.142


// Global Funtion Declaration
LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

//Global variable declaration
HWND ghwnd;
BOOL gbFullScreen = FALSE;
FILE *gpFile = NULL;
BOOL gbActiveWindow = FALSE;
BOOL bDone = FALSE;
int iRetVal = 0;
HDC ghdc = NULL;
HGLRC ghrc = NULL;


// Entry Point Function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {

	// function declarations
	int initialize(void);
	void display(void);
	void update(void);
	void uninitialize(void);

	// variable declarations 
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");
	//int width = 800, height = 600;
	int SysWidth = GetSystemMetrics(SM_CXSCREEN);
	int SysHeight = GetSystemMetrics(SM_CYSCREEN);

	// code

	if (fopen_s(&gpFile, "Log.txt", "w") != 0) {

		MessageBox(NULL, TEXT("Creation Of Log File Failed Exiting..."), TEXT("File I/O Error"), MB_OK);
		exit(0);

	}
	else {

		fprintf(gpFile, "Log File Successfully Opned\n");

	}


	// initalization of WNDCLASSEX structure

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;													//handle
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);					//handle
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));								//handle	
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);									//handle
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));


	// registering above wndclass
	RegisterClassEx(&wndclass);

	// create the window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("OGL Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(SysWidth / 2) - WIN_WIDTH / 2,
		(SysHeight / 2) - WIN_HEIGHT / 2,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	// initialize
	iRetVal = initialize();
	if (iRetVal == -1) {

		fprintf(gpFile, "ChoosePixelFormat falied\n");
		uninitialize();

	}
	else if (iRetVal == -2) {

		fprintf(gpFile, "SetPixelFormat falied\n");
		uninitialize();
	}
	else if (iRetVal == -3) {

		fprintf(gpFile, "wglCreateContext falied\n");
		uninitialize();
	}
	else if (iRetVal == -4) {

		fprintf(gpFile, "making OpenGL context as current context failed\n");
			uninitialize();
	
	} else {
		
		fprintf(gpFile, "call to initalize successfull\n");
	}


	// show window
	ShowWindow(hwnd, iCmdShow);

	//Foregrounding and focusing the window
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	
	PlaySound(MAKEINTRESOURCE(SONG), NULL, SND_RESOURCE | SND_ASYNC);

	// GameLoop
	while (bDone == FALSE) {
		
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			
			if (msg.message == WM_QUIT) {
				
				bDone = TRUE;
			
			} else {
				
				TranslateMessage(&msg);
				DispatchMessage(&msg);

			}
		
		} else {
		
			if (gbActiveWindow == TRUE) {
				
				// Render the scene
				display();

				// update the scene
				update();
			
			}
		}
	
	
	}
		
	uninitialize();													//change done
	return ((int) msg.wParam);
}

// Callback Function
LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	
	int width, height;
	TCHAR str[255];

	// function declarations
	void ToggleFullScreen(void);
	void resize(int, int);
	//void uninitialize(void);

	// code
	switch (iMsg) {
		
		case WM_SETFOCUS:
			gbActiveWindow = TRUE;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = FALSE;
			break;
		case WM_ERASEBKGND:
			return 0;
		case WM_CHAR:
			switch (wParam) {
				
				case 27:
					PostQuitMessage(0);
					break;
				case 'F':
				case 'f':
					ToggleFullScreen();
					break;
				
				default:
					break;
			}
			break;
		case WM_SIZE:
			resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_CLOSE:
			DestroyWindow(hWnd);
			break;
		case WM_DESTROY:
			//uninitialize();
			PostQuitMessage(0);
			break;
		
		default:
			break;
	}

	return (DefWindowProc(hWnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void) {

	//Variable Declarations
	static DWORD dwStyle;
	static WINDOWPLACEMENT wp;
	MONITORINFO mi;

	//code
	wp.length = sizeof(WINDOWPLACEMENT);

	if (gbFullScreen == FALSE) {

		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) {

			mi.cbSize = sizeof(MONITORINFO);

			if (GetWindowPlacement(ghwnd, &wp) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {

				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}

			ShowCursor(FALSE);
			gbFullScreen = TRUE;

		}

	}
	else {

		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wp);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
		gbFullScreen = FALSE;
	}
}

int initialize(void) {
	
	// function declarations
	// warmup resize
	void resize(int, int);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	// code
	// Initialization of PIXELFORMATDESCRIPTOR structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 0;
	pfd.cDepthBits = 32;							//24 can also be done

	// Get DC
	ghdc = GetDC(ghwnd);

	// Choose pixel format
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
		return -1;
	
	// Set the choosen pixel format
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
		return -2;

	// Create OpenGL Rendaring Context
	ghrc = wglCreateContext(ghdc);
	
	if (ghrc == NULL)
		return -3;

	// make the rendaring context as a current context 
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
		return -4;

	// Here starts OpenGL code
	// Clear the screen using black color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	// Depth related changes
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// opetional function
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);



	//warmup resize call
	resize(WIN_WIDTH, WIN_HEIGHT);
	return 0;
}

void resize(int width, int height) {
	

	//variable declaration

	// code
	if (height == 0)			// to avoid "/ by 0" in future calls
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);																			
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

	 
}

struct Point {

	float xCo;
	float yCo;


};


void letterF(float x, float y) {

	float xCo = x;
	float yCo = y;

	glBegin(GL_LINES);
	
	glVertex3f(xCo, yCo, 0.0f);
	
	xCo += 0.25f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = x;
	yCo = y;

	yCo -= 0.15f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo += 0.25f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = x;
	yCo = y;
	glVertex3f(xCo, yCo, 0.0f);

	yCo -= 0.4F;
	glVertex3f(xCo, yCo, 0.0f);


	glEnd();

}


void letterA(float x, float y) {

	float xCo = x;
	float yCo = y;

	glBegin(GL_LINES);
	glVertex3f(xCo, yCo, 0.0f);

	xCo -= 0.2f;
	yCo -= 0.45f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = x;
	yCo = y;
	glVertex3f(xCo, yCo, 0.0f);

	xCo += 0.2f;
	yCo -= 0.45f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = x;
	yCo = y;

	xCo -= 0.1f;
	yCo -= 0.25f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo += 0.2f;
	glVertex3f(xCo, yCo, 0.0f);


	glEnd();

}

void letterI(float x, float y) {
	
	float xCo = x;
	float yCo = y;

	glColor3f(0.0f, 0.0f, 0.0f);
	
	glLineWidth(2.0f);
	glBegin(GL_LINES);
	glVertex3f(xCo, yCo, 0.0f);

	xCo += 0.4f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo -= 0.2f;
	glVertex3f(xCo, yCo, 0.0f);

	yCo = yCo - 0.4f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo -= 0.2f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo += 0.4f;
	glVertex3f(xCo, yCo, 0.0f);

	glEnd();


		 
}
void drawPlane() {

	float xCo = 0.0f;
	float yCo = 0.0f;

	
	

	static struct Point arr[255];
	
	//glPointSize(3.0f);
	//glBegin(GL_POLYGON);
	//glBegin(GL_POINTS);

	xCo = 0.0f;
	yCo = 1.5f;

	arr[0].xCo = 0.0f;
	arr[0].yCo = 1.5f;

	//glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 0.1f;
	yCo = yCo - 0.2f;
	
	arr[1].xCo = arr[0].xCo - 0.1f;
	arr[1].yCo = arr[0].yCo - 0.2f;

	//glVertex3f(xCo, yCo, 0.0f);

	yCo = yCo - 0.5f;
	
	arr[2].xCo = arr[1].xCo;
	arr[2].yCo = arr[1].yCo - 0.5f;

	//glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 0.2f;
	yCo = yCo - 0.6f;

	arr[3].xCo = arr[2].xCo - 0.2f;
	arr[3].yCo = arr[2].yCo - 0.6f;

	//glVertex3f(xCo, yCo, 0.0f);

	yCo = yCo - 0.4f;


	arr[4].xCo = arr[3].xCo;
	arr[4].yCo = arr[3].yCo - 0.4f;

	//glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 0.4f;
	yCo = yCo - 0.2f;
	
	arr[5].xCo = arr[4].xCo - 0.4f;
	arr[5].yCo = arr[4].yCo - 0.2f;

	//glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 0.1f;
	yCo = yCo - 0.2f;
	

	arr[6].xCo = arr[5].xCo - 0.1f;
	arr[6].yCo = arr[5].yCo - 0.2f;

	//glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 0.6f;
	yCo = yCo - 0.3f;
	
	arr[7].xCo = arr[6].xCo - 0.6f;
	arr[7].yCo = arr[6].yCo - 0.3f;

	//glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 0.1f;
	yCo = yCo - 0.3f;
	
	arr[8].xCo = arr[7].xCo - 0.1f;
	arr[8].yCo = arr[7].yCo - 0.3f;

	//glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 1.2f;
	yCo = yCo + 0.2f;
	
	arr[9].xCo = arr[8].xCo + 1.2f;
	arr[9].yCo = arr[8].yCo + 0.2f;

	//glVertex3f(xCo, yCo, 0.0f);

	yCo = yCo - 0.4f;
	
	arr[10].xCo = arr[9].xCo;
	arr[10].yCo = arr[9].yCo - 0.4f;

	//glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 0.5f;
	yCo = yCo - 0.4f;
	
	arr[11].xCo = arr[10].xCo - 0.5f;
	arr[11].yCo = arr[10].yCo - 0.4f;

	//glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.1f;
	yCo = yCo - 0.2f;
	
	arr[12].xCo = arr[11].xCo + 0.1f;
	arr[12].yCo = arr[11].yCo - 0.2f;

	//glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.3f;
	yCo = yCo + 0.1f;
	
	arr[13].xCo = arr[12].xCo + 0.3f;
	arr[13].yCo = arr[12].yCo + 0.1f;

	
	//glVertex3f(xCo, yCo, 0.0f);

	yCo = yCo + 0.1f;
	
	arr[14].xCo = arr[13].xCo;
	arr[14].yCo = arr[13].yCo + 0.1f;

	//glVertex3f(xCo, yCo, 0.0f);
	
	xCo = xCo + 0.1f;
	yCo = yCo + 0.1f;
	
	arr[15].xCo = arr[14].xCo + 0.1f;
	arr[15].yCo = arr[14].yCo + 0.1f;

	//glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 0.05f;
	yCo = yCo - 0.3f;

	arr[16].xCo = arr[15].xCo - 0.05f;
	arr[16].yCo = arr[15].yCo - 0.3f;

	
	//glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.1f;

	arr[17].xCo = arr[16].xCo + 0.1f;
	arr[17].yCo = arr[16].yCo;

	
	//glVertex3f(xCo, yCo, 0.0f);
	
	xCo = xCo + 0.2f;
	yCo = yCo + 0.3f;
	
	arr[18].xCo = arr[17].xCo + 0.2f;
	arr[18].yCo = arr[17].yCo + 0.3f;

	//glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.3f;
	
	arr[19].xCo = arr[18].xCo + 0.3f;
	arr[19].yCo = arr[18].yCo;

	//glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.15f;
	yCo = yCo - 0.3f;
	
	arr[20].xCo = arr[19].xCo + 0.15f;
	arr[20].yCo = arr[19].yCo - 0.3f;

	//glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.1f;
	
	arr[21].xCo = arr[20].xCo + 0.1f;
	arr[21].yCo = arr[20].yCo;

	//glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.05f;
	yCo = yCo + 0.3f;
	
	arr[22].xCo = arr[21].xCo + 0.05f;
	arr[22].yCo = arr[21].yCo + 0.3f;

	//glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.1f;
	yCo = yCo - 0.1f;
	
	arr[23].xCo = arr[22].xCo + 0.1f;
	arr[23].yCo = arr[22].yCo - 0.1f;

	//glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.02f;
	yCo = yCo - 0.1f;
	
	arr[24].xCo = arr[23].xCo + 0.02f;
	arr[24].yCo = arr[23].yCo - 0.1f;

	//glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.25f;
	yCo = yCo - 0.1f;
	
	arr[25].xCo = arr[24].xCo + 0.25f;
	arr[25].yCo = arr[24].yCo - 0.1f;

	
	//glVertex3f(xCo, yCo, 0.0f);

	/*glColor3f(186.0f / 255.0f, 226.0f / 255.0f, 238.0f / 255.0f);
	for (int i = 0; i < 19; i++) {
		
		glVertex3f(arr[i].xCo, arr[i].yCo, 0.0f);

	
	}
	

	for (int i = 19; i >= 0; i--) {

		glVertex3f(-arr[i].xCo, arr[i].yCo, 0.0f);
	
	}

	glEnd();*/

	glScalef(1.30f, 1.30f, 1.30f);
	glColor3f(186.0f / 255.0f, 226.0f / 255.0f, 238.0f / 255.0f);
	glBegin(GL_POLYGON);

	for (int i = 19; i >= 0; i--) {

		glVertex3f(-arr[i].xCo, arr[i].yCo, 0.0f);

	}

	glEnd();

	glBegin(GL_POLYGON);

	for (int i = 19; i >= 0; i--) {

		glVertex3f(arr[i].xCo, arr[i].yCo, 0.0f);

	}

	glEnd();

	float letterTranslate = 0.6f;
	float flagSize = 0.3f;
	//glLoadIdentity();
	//glTranslatef(-0.05f, 0.0f, -4.0f);
	//glScalef(0.25f, 0.25f, 0.25f);
	glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
	//glTranslatef(-0.4f, 0.0f, 0.0f);

	letterI(-0.7f - letterTranslate, 0.2f);

	letterA(0.0f - letterTranslate, 0.23f);

	letterF(0.35f - letterTranslate, 0.2f);

	glPointSize(2.0f);
	glBegin(GL_QUADS);

	xCo = -2.0f;
	yCo = 0.3f;
	// orange
	glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 1.3f + flagSize;
	glVertex3f(xCo, yCo, 0.0f);

	yCo = yCo - 0.2f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 1.5f - flagSize;
	glVertex3f(xCo, yCo, 0.0f);
	
	// white
	glColor3f(255.0 / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	xCo = xCo + 0.05f;
	//yCo = yCo - 0.2f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 1.55f + flagSize;
	glVertex3f(xCo, yCo, 0.0f);

	yCo = yCo - 0.2f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 1.55f - flagSize;
	glVertex3f(xCo, yCo, 0.0f);

	// green
	xCo = xCo - 0.1f;
	glColor3f(19.0 / 255.0f, 136.0f / 255.0f, 8.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 1.465f + flagSize;
	glVertex3f(xCo, yCo, 0.0f);

	yCo = yCo - 0.2f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 1.35f - flagSize;
	glVertex3f(xCo, yCo, 0.0f);

	glEnd();

	glBegin(GL_POINTS);

	
	
	glEnd();

}

void displayI(float iStartx, float iStarty, float width, float height) {

	// 255-153-51 orange
	// 255-255-255 white
	// 19-136-8 green

	//float width = 0.5f;
	float xCo = 0.0f;
	float yCo = 0.0f;
	//iStart = -1.5f;

	glBegin(GL_QUADS);
	// upper line
	xCo = iStartx;
	yCo = iStarty;
	glColor3f(255.0 / 255.0, 153.0 / 255.0, 51.0 / 255.0);
	glVertex3f(xCo, yCo, 0.0f);

	//xCo = -2.0f;
	xCo = xCo - width;
	yCo = iStarty;
	glVertex3f(xCo, yCo, 0.0f);

	//xCo = -2.0f;
	//xCo = xCo - width;
	yCo = iStarty - 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = iStartx;
	yCo = iStarty - 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	// middle line
	xCo = iStartx - width / 2.0f + 0.05f;
	yCo = iStarty - 0.1f;

	glColor3f(255.0 / 255.0, 255.0 / 255.0f, 255.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = iStartx - width / 2.0f - 0.05f;
	yCo = iStarty - 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = iStartx - width / 2.0f - 0.05f;
	yCo = (iStarty - height) + 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = iStartx - width / 2.0f + 0.05f;
	yCo = (iStarty - height) + 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	//Below line

	xCo = iStartx;
	yCo = (iStarty - height) + 0.1f;
	glColor3f(19.0 / 255.0f, 136.0f / 255.0f, 8.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	//xCo = -2.0f;
	xCo = xCo - width;
	yCo = (iStarty - height) + 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	//xCo = -2.0f;
	//xCo = xCo - width;
	yCo = (iStarty - height);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = iStartx;
	yCo = (iStarty - height);
	glVertex3f(xCo, yCo, 0.0f);

	// above color cube
	xCo = iStartx - width / 2.0f + 0.05f;
	yCo = iStarty - 0.1f;
	glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = iStartx - width / 2.0f - 0.05f;
	yCo = iStarty - 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = iStartx - width / 2.0f - 0.05f;
	yCo = (iStarty - 0.5f);
	glColor3f(255.0 / 255.0, 255.0 / 255.0f, 255.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = iStartx - width / 2.0f + 0.05f;
	yCo = (iStarty - 0.5f);
	glVertex3f(xCo, yCo, 0.0f);


	// below color cube
	xCo = iStartx - width / 2.0f - 0.05f;
	yCo = (iStarty - height) + 0.1f;
	glColor3f(19.0 / 255.0f, 136.0f / 255.0f, 8.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = iStartx - width / 2.0f + 0.05f;
	yCo = (iStarty - height) + 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = iStartx - width / 2.0f + 0.05f;
	yCo = iStarty - 0.5f;
	glColor3f(255.0 / 255.0, 255.0 / 255.0f, 255.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = iStartx - width / 2.0f - 0.05f;
	yCo = iStarty - 0.5f;
	glVertex3f(xCo, yCo, 0.0f);

	glEnd();


}

void displayN(float nStartx, float nStarty, float width, float height) {

	float xCo = 0.0f;
	float yCo = 0.0f;


	glBegin(GL_QUADS);

	// left standing line
	xCo = nStartx;
	yCo = nStarty;
	glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = nStartx + 0.1f;
	glVertex3f(xCo, yCo, 0.0f);


	yCo = nStarty - height;

	glVertex3f(xCo, yCo, 0.0f);

	xCo = nStartx;
	glVertex3f(xCo, yCo, 0.0f);

	// cross line
	xCo = nStartx + 0.1f;
	yCo = nStarty;
	glColor3f(255.0 / 255.0, 255.0 / 255.0f, 255.0f / 255.0f);
	//glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	yCo = nStarty - 0.2f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = nStartx + width - 0.1f;
	yCo = nStarty - height;


	glVertex3f(xCo, yCo, 0.0f);

	yCo = nStarty - (height - 0.2f);
	glVertex3f(xCo, yCo, 0.0f);


	// right stright line
	xCo = nStartx + width - 0.1f;
	yCo = nStarty - height;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = nStartx + width;
	glVertex3f(xCo, yCo, 0.0f);

	yCo = nStarty;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = nStartx + width - 0.1f;
	glVertex3f(xCo, yCo, 0.0f);


	// right line above color 
	xCo = nStartx + width;
	yCo = nStarty;
	glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	yCo = nStarty - 0.2f;
	//glColor3f(255.0 / 255.0, 255.0 / 255.0f, 255.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = nStartx + width;
	glVertex3f(xCo, yCo, 0.0f);

	// right line middle color
	xCo = nStartx + width;
	yCo = nStarty - 0.2f;
	glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	yCo = yCo - 0.1f;
	glColor3f(255.0 / 255.0, 255.0 / 255.0f, 255.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = nStartx + width;
	glVertex3f(xCo, yCo, 0.0f);




	// right line below color
	xCo = nStartx + width;
	yCo = nStarty - 0.4f;

	glColor3f(255.0 / 255.0, 255.0 / 255.0f, 255.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 0.1f;
	glVertex3f(xCo, yCo, 0.0f);


	yCo = nStarty - height;
	glColor3f(19.0 / 255.0f, 136.0f / 255.0f, 8.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.1f;
	glVertex3f(xCo, yCo, 0.0f);


	// above color cube
	xCo = nStartx + 0.1f;
	yCo = nStarty - 0.2f;
	glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = nStartx;
	yCo = nStarty - 0.2f;
	glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = nStartx;
	yCo = (nStarty - 0.5f);
	glColor3f(255.0 / 255.0, 255.0 / 255.0f, 255.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = nStartx + 0.1f;
	yCo = (nStarty - 0.5f);
	glVertex3f(xCo, yCo, 0.0f);

	// above second
	xCo = nStartx + 0.1f;
	yCo = nStarty - 0.2f;
	glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = nStartx;
	glVertex3f(xCo, yCo, 0.0f);

	yCo = yCo - 0.25f;

	glColor3f(255.0 / 255.0, 255.0 / 255.0f, 255.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo += 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	// below color cube

	xCo = nStartx + 0.1f;
	glColor3f(255.0 / 255.0, 255.0 / 255.0f, 255.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	yCo = nStarty - height;
	glColor3f(19.0 / 255.0f, 136.0f / 255.0f, 8.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = nStartx + 0.1f;
	glVertex3f(xCo, yCo, 0.0f);


	// cross line above color cube
	xCo = nStartx + 0.1f;
	yCo = nStarty;

	glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	yCo = nStarty - 0.2f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.15f;
	yCo = yCo - 0.35f;

	glColor3f(255.0 / 255.0, 255.0 / 255.0f, 255.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	yCo = yCo + 0.2f;
	glVertex3f(xCo, yCo, 0.0f);

	// cross line below color cube

	xCo = nStartx + 0.1 + 0.15f;
	yCo = nStarty - 0.2f - 0.35f;

	glVertex3f(xCo, yCo, 0.0f);

	yCo = yCo + 0.2f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = nStartx + width - 0.1f;
	yCo = nStarty - (height - 0.2f);

	glColor3f(19.0 / 255.0f, 136.0f / 255.0f, 8.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	yCo = nStarty - height;
	glVertex3f(xCo, yCo, 0.0f);

	glEnd();

	//////////////////////////


	glEnd();


}

float orangeRColor = 0.0f;
float orangeGColor = 0.0f;
float orangeBColor = 0.0f;

float greenRColor = 0.0f;
float greenGColor = 0.0f;
float greenBColor = 0.0f;


float whiteRColor = 0.0f;
float whiteGColor = 0.0f;
float whiteBColor = 0.0f;


int flag1 = 0;
int flag2 = 0;
int flag3 = 0;
int flag4 = 0;
int flag5 = 0;
int flag6 = 0;
int flag7 = 0;
int flag8 = 0;
int flag9 = 0;


int curvePlaneRotateCount = 0;


void displayD(float dStartx, float dStarty, float width, float height) {

	float xCo = 0.0f;
	float yCo = 0.0f;

	// upper horizontal line
	glBegin(GL_QUADS);

	xCo = dStartx - 0.1f;
	yCo = dStarty;

	if (orangeRColor <= 255.0 && orangeGColor <= 153.0 && orangeBColor <= 51.0f && flag1 == 0) {
		
		glColor3f(orangeRColor / 255.0f, orangeGColor / 255.0f, orangeBColor / 255.0f);
	
	} else {
		
		flag1 = 1;
		glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);
		
	}

	//glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);
	xCo = dStartx + width - 0.1f;

	glVertex3f(xCo, yCo, 0.0f);

	yCo = dStarty - 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = dStartx;
	glVertex3f(xCo, yCo, 0.0f);

	// right vertical line
	xCo = dStartx + width - 0.1f;
	yCo = dStarty;

	if (orangeRColor <= 255.0 && orangeGColor <= 153.0 && orangeBColor <= 51.0f && flag2 == 0) {

		glColor3f(orangeRColor / 255.0f, orangeGColor / 255.0f, orangeBColor / 255.0f);

	}
	else {

		flag2 = 1;
		glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);
		
	}

	//glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);
	
	
	
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.1f;
	yCo = yCo - 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	yCo = dStarty - height + 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	// below horizontal line
	xCo = xCo + 0.1f;
	yCo = dStarty - height + 0.1f;
	
	if (greenRColor <= 19.0 && greenGColor <= 136.0 && greenBColor <= 8.0f && flag3 == 0) {

		glColor3f(greenRColor / 255.0f, greenGColor / 255.0f, greenBColor / 255.0f);

	}
	else {

		flag3 = 1;
		glColor3f(19.0 / 255.0f, 136.0f / 255.0f, 8.0f / 255.0f);
	
	
	}
	
	
	//glColor3f(19.0 / 255.0f, 136.0f / 255.0f, 8.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 0.1f;
	yCo = yCo - 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = dStartx - 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = dStartx;
	yCo = yCo + 0.1f;
	glVertex3f(xCo, yCo, 0.0f);


	// middle line
	xCo = dStartx + 0.1f;
	yCo = dStarty - 0.1f;
	glColor3f(255.0 / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	yCo = dStarty - height + 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = dStartx + 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	// right vertical line color above
	xCo = dStartx + width - 0.1f;
	yCo = dStarty;

	if (orangeRColor <= 255.0 && orangeGColor <= 153.0 && orangeBColor <= 51.0f && flag4 == 0) {

		glColor3f(orangeRColor / 255.0f, orangeGColor / 255.0f, orangeBColor / 255.0f);

	}
	else {

		flag4 = 1;
		glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);
	
	}
	//glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.1f;
	yCo = yCo - 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	yCo = yCo - 0.4f;


	glColor3f(255.0 / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	// right vertical line color below
	xCo = dStartx + width - 0.1f;
	yCo = dStarty - 0.5f;
	//glColor3f(255.0 / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);

	glVertex3f(xCo, yCo, 0.0f);

	xCo = dStartx + width;
	glVertex3f(xCo, yCo, 0.0f);

	yCo = dStarty - height;

	if (greenRColor <= 19.0 && greenGColor <= 136.0 && greenBColor <= 8.0f && flag5 == 0) {

		glColor3f(greenRColor / 255.0f, greenGColor / 255.0f, greenBColor / 255.0f);

	}
	else {

		flag5 = 1;
		glColor3f(19.0 / 255.0f, 136.0f / 255.0f, 8.0f / 255.0f);
	

	}
	//glColor3f(19.0 / 255.0f, 136.0f / 255.0f, 8.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = dStartx + width - 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	////////////////
	// middle vertical line color above
	xCo = dStartx + 0.1f;
	yCo = dStarty - 0.1f;


	if (orangeRColor <= 255.0 && orangeGColor <= 153.0 && orangeBColor <= 51.0f && flag6 == 0) {

		glColor3f(orangeRColor / 255.0f, orangeGColor / 255.0f, orangeBColor / 255.0f);

	}
	else {

		flag6 = 1;
		glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);

	}

	//glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	yCo = yCo - 0.3f;

	glColor3f(255.0 / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = dStartx + 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	// middle vertical line color below

	xCo = dStartx + 0.1;
	yCo = yCo;

	glColor3f(255.0 / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	yCo = dStarty - height + 0.1f;

	if (greenRColor <= 19.0 && greenGColor <= 136.0 && greenBColor <= 8.0f && flag7 == 0) {

		glColor3f(greenRColor / 255.0f, greenGColor / 255.0f, greenBColor / 255.0f);

	}
	else {

		flag7 = 1;
		glColor3f(19.0 / 255.0f, 136.0f / 255.0f, 8.0f / 255.0f);
	

	}
	
	//glColor3f(19.0 / 255.0f, 136.0f / 255.0f, 8.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = dStartx + 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	glEnd();


}

float xValue = 0.0f;

void displayA(float aStartx, float aStarty, float width, float height) {

	float xCo = 0.0f;
	float yCo = 0.0f;

	// left line
	glBegin(GL_QUADS);

	xCo = aStartx;
	yCo = aStarty;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = aStartx - 0.1f;
	yCo = aStarty;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = aStartx - 0.1 - 0.3f;
	yCo = aStarty - height;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	glEnd();

	// above left line color 
	glBegin(GL_QUADS);
	xCo = aStartx;
	yCo = aStarty;
	glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = aStartx - 0.1f;
	yCo = aStarty;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 0.065f;
	yCo = yCo - 0.2f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.1f;
	glVertex3f(xCo, yCo, 0.0f);


	// left line (middle) color cube
	xCo = aStartx - 0.1f - 0.065f;
	yCo = aStarty - 0.2f;
	glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	yCo = yCo - 0.15f;
	xCo = xCo - 0.053f;
	glColor3f(255.0 / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 0.1f;
	glVertex3f(xCo, yCo, 0.0f);


	glEnd();

	// below left line color

	glBegin(GL_QUADS);
	xCo = xCo + 0.05f;
	yCo = aStarty - 0.5f;
	glColor3f(255.0 / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 0.1f;
	glVertex3f(xCo, yCo, 0.0f);



	xCo = aStartx - 0.1 - 0.3f;
	yCo = aStarty - height;

	glColor3f(19.0 / 255.0f, 136.0f / 255.0f, 8.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	glEnd();


	glBegin(GL_QUADS);

	// right line
	xCo = aStartx;
	yCo = aStarty;
	glColor3f(255.0 / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = aStartx - 0.04f;
	yCo = aStarty - 0.12f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = aStartx + 0.1f + 0.15f;
	yCo = aStarty - height;

	glVertex3f(xCo, yCo, 0.0f);

	xCo = aStartx + 0.1f + 0.25f;
	glVertex3f(xCo, yCo, 0.0f);

	glEnd();

	// right line upper color
	glBegin(GL_QUADS);
	xCo = aStartx;
	yCo = aStarty;

	glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	/*xCo = aStartx - 0.04f;
	yCo = aStarty - 0.12f;
	*/

	xCo = aStartx - 0.1f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = aStartx - 0.01f;
	yCo = aStarty - 0.2f;

	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.09f;
	glVertex3f(xCo, yCo, 0.0f);

	glEnd();

	// right line upper color second	
	glBegin(GL_QUADS);

	glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 0.09f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.055f;
	yCo = yCo - 0.15f;

	glColor3f(255.0 / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.09f;
	glVertex3f(xCo, yCo, 0.0f);

	glEnd();


	glBegin(GL_QUADS);

	glColor3f(255.0 / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);
	xCo = xCo - 0.09f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.055f;
	yCo = yCo - 0.15f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.09f;
	glVertex3f(xCo, yCo, 0.0f);

	glEnd();

	// right line middle color cube
	glBegin(GL_QUADS);

	glColor3f(255.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 0.09f;
	glVertex3f(xCo, yCo, 0.0f);


	xCo += 0.151f;
	yCo -= 0.4f;
	glColor3f(19.0 / 255.0f, 136.0f / 255.0f, 8.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo += 0.099f;
	glVertex3f(xCo, yCo, 0.0f);

	glEnd();


	glBegin(GL_QUADS);
	xCo = aStartx - 0.051f;
	yCo = aStarty - 0.1f;

	
	glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.09f;
	glVertex3f(xCo, yCo, 0.0f);


	xCo = xCo + 0.075f;
	yCo = yCo - 0.2f;
	glColor3f(255.0 / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 0.082f;
	glVertex3f(xCo, yCo, 0.0f);

	glEnd();
	
	// right line below color

	glBegin(GL_QUADS);
	glColor3f(255.0 / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	glVertex3f(xCo, yCo, 0.0f);
	xCo = xCo + 0.082f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo + 0.082f;
	yCo = yCo - 0.2f;
	glVertex3f(xCo, yCo, 0.0f);

	xCo = xCo - 0.089f;
	glVertex3f(xCo, yCo, 0.0f);

	glEnd();

	
	if (curvePlaneRotateCount > 1190) {

		// middle line
		glTranslatef(0.135f, 0.04f, 0.0f);
		glScalef(0.90, 0.90, 0.90f);


		// horizontal line orange color
		glTranslatef(0.008f, 0.02f, 0.0f);
		glBegin(GL_QUADS);
		xCo = aStartx - 0.15f;
		yCo = aStarty - 0.45f;
		glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);
		glVertex3f(xCo, yCo, 0.0f);

		xCo = xCo - 0.02f;
		yCo = aStarty - 0.49f;

		//glColor3f(255.0 / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
		glColor3f(255.0 / 255.0, 153.0 / 255.0f, 51.0f / 255.0f);

		glVertex3f(xCo, yCo, 0.0f);

		static int count1 = 0;

		if (count1 < 20)
			xCo += xValue;
		else
			xCo = aStartx + 0.09f;


		count1++;
		glVertex3f(xCo, yCo, 0.0f);

		xCo = xCo - 0.02f;

		yCo = aStarty - 0.45f;
		glVertex3f(xCo, yCo, 0.0f);


		glEnd();


		// horizontal line white color
		glTranslatef(-0.008f, -0.04f, 0.0f);
		glBegin(GL_QUADS);
		xCo = aStartx - 0.15f;
		yCo = aStarty - 0.45f;
		glColor3f(255.0 / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
		//glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 0.0f / 255.0f);
		glVertex3f(xCo, yCo, 0.0f);

		xCo = xCo - 0.02f;
		yCo = aStarty - 0.49f;

		glVertex3f(xCo, yCo, 0.0f);

		if (count1 < 20)
			xCo += xValue;
		else
			xCo = aStartx + 0.113f;

		glVertex3f(xCo, yCo, 0.0f);

		xCo = xCo - 0.02f;

		yCo = aStarty - 0.45f;
		glVertex3f(xCo, yCo, 0.0f);


		glEnd();


		glTranslatef(-0.017f, -0.04f, 0.0f);
		// horizontal green orange color
		glBegin(GL_QUADS);
		xCo = aStartx - 0.15f;
		yCo = aStarty - 0.45f;
		//glColor3f(255.0 / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
		glColor3f(19.0 / 255.0f, 136.0f / 255.0f, 8.0f / 255.0f);

		glVertex3f(xCo, yCo, 0.0f);

		xCo = xCo - 0.02f;
		yCo = aStarty - 0.49f;

		glVertex3f(xCo, yCo, 0.0f);

		//static int count1 = 0;

		if (count1 < 20)
			xCo += xValue;
		else
			xCo = aStartx + 0.145f;

		glVertex3f(xCo, yCo, 0.0f);

		xCo = xCo - 0.02f;

		yCo = aStarty - 0.45f;

		glColor3f(19.0 / 255.0f, 136.0f / 255.0f, 8.0f / 255.0f);
		glVertex3f(xCo, yCo, 0.0f);
		glColor3f(255.0 / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);


		glEnd();

	}

}

// for firstAnimation()
float startAnimationX = -3.0f;
float planeRotate = 0.0f;
int startingAnimationCount = 0;


void firstAnimation(float y) {
	

	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -4.0f);
	glTranslatef(startAnimationX, y, 0.0f);
	glScalef(0.15f, 0.15f, 0.15f);
	//glRotatef(90.0f, 0.0f, 0.0f, -1.0f);

	glRotatef(270.0f, 0.0f, 0.0, 1.0f);
	glRotatef(planeRotate, 0.0f, 1.0f, 0.0f);

	drawPlane();


	glLoadIdentity();
	glTranslatef(-0.5f, 0.3f, -4.0f);
	glTranslatef(startAnimationX, y, 0.0f);
	glScalef(0.15f, 0.15f, 0.15f);
	//glRotatef(90.0f, 0.0f, 0.0f, -1.0f);

	glRotatef(270.0f, 0.0f, 0.0, 1.0f);
	glRotatef(planeRotate, 0.0f, 1.0f, 0.0f);

	drawPlane();

	glLoadIdentity();
	glTranslatef(-0.5f, -0.3f, -4.0f);
	glTranslatef(startAnimationX, y, 0.0f);
	glScalef(0.15f, 0.15f, 0.15f);
	//glRotatef(90.0f, 0.0f, 0.0f, -1.0f);

	glRotatef(270.0f, 0.0f, 0.0, 1.0f);
	glRotatef(planeRotate, 0.0f, 1.0f, 0.0f);

	drawPlane();


}


struct Point curveArray[2160];
int saveIndex = 0;

int calculatePlanePath() {
	
	int index = 0;

	float x = 0.0f;
	float y = 0.0f;
	float xCo = 0.0f;
	float yCo = 0.0f;
	float redius = 3.0f;


	x = 0.0f;
	y = 2.4f;

	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -4.0f);

	// left curve
	//glBegin(GL_LINES);
	glColor3f(255.0 / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	for (float i = 180.0f; i < 233.0f; i += 0.1f) {

		//if (firstLoop < 235.0f) {
		xCo = x + redius * cos(i * (PI / 180.0f));
		yCo = y + redius * sin(i * (PI / 180.0f));

		curveArray[index].xCo = xCo;
		curveArray[index].yCo = yCo;
		if (i < 232.9)
			saveIndex = index;
		index++;

		glVertex3f(xCo, yCo, 0.0f);
	}
	//}
	//glEnd();

	// middle stright line
	//glBegin(GL_LINES);

	xCo = x + redius * cos(233 * (PI / 180.0f));
	yCo = y + redius * sin(233 * (PI / 180.0f));
	glVertex3f(xCo, yCo, 0.0f);


	curveArray[index].xCo = xCo;
	curveArray[index].yCo = yCo;
	index++;

	//float xCoLimitValue = xCo + 3.6f;
	float xCoLimitValue = xCo + 3.49f;

	for (int i = 0; xCo <= xCoLimitValue; i++) {

		xCo = xCo + 0.005f;
		curveArray[index].xCo = xCo;
		curveArray[index].yCo = yCo;
		index++;

	}
	//xCo += 3.0f;
	glVertex3f(xCo, yCo, 0.0f);

	//glEnd();
	
	for (int i = saveIndex; i > 0; i--) {
		
		curveArray[index].xCo = -curveArray[i].xCo - 0.12f;
		curveArray[index].yCo = curveArray[i].yCo;

		glVertex3f(curveArray[index].xCo, curveArray[index].yCo, 0.0f);
		index++;
	
	}
	//glEnd();

	return index;
}



float iTranslate = -2.0f;
float nTranslate = 3.5f;
float dTranslate = -10.0f;
float iSecTranstate = -2.5f;
float aTranslate = 3.0f;
float rotateAngle1 = 180.0f;

float rotateAngle2 = 0.0f;


float firstLoop = 180.0f;
float secondLoop = 307.0f;

int lettersCount = 0;

float strightLineX = -3.5f;


int commonLettersCount = 260;
int calulateFlag = 0;

void display(void) {

	float xCo = 0.0f;
	float yCo = 0.0f;
	float x = 0.0f;
	float y = 0.0f;

	float scaleValue = 1.5f;



	
	static int index = 0;
	static int arrIndex = 0.0f;

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);

	////////////////////////////////////////////////////////////////////////////////////////////////
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -4.0f);

	
	
	// three plane animation
	firstAnimation(0.0f);

	//////////////////////////////////
	
	index = calculatePlanePath();


	float commonTransalteValue = 0.3f;

	if (startingAnimationCount > 270) {

		glLoadIdentity();

		//glTranslatef(-0.5f, 0.0f, 0.0f);
		glTranslatef(iTranslate - commonTransalteValue, 0.0f, -6.0f);
		glScalef(scaleValue, scaleValue, scaleValue);
		displayI(-1.5f, 0.45f, 0.5f, 0.9f);


		glLoadIdentity();
		glTranslatef(0.3f, 0.0f, -6.0f);

		//if (lettersCount > 30 + commonLettersCount) {

		glTranslatef(0.5f - commonTransalteValue, nTranslate, 0.0f);
		glScalef(scaleValue, scaleValue, scaleValue);
		displayN(-1.3f, 0.45f, 0.5f, 0.9f);


		if (lettersCount > 500 + commonLettersCount) {

			glLoadIdentity();
			glTranslatef(0.5f - commonTransalteValue, iSecTranstate, -6.0f);
			glScalef(scaleValue, scaleValue, scaleValue);
			displayI(0.8f, 0.45f, 0.5f, 0.9f);


		}
			
		if (lettersCount > 750 + commonLettersCount) {
			
			glLoadIdentity();
			glTranslatef(aTranslate - commonTransalteValue, 0.0f, -6.0f);
			glScalef(scaleValue, scaleValue, scaleValue);
			displayA(1.4f, 0.45f, 0.5f, 0.9f);

		
		}

	
	}

	if (lettersCount > 380 + commonLettersCount) {

		glLoadIdentity();
		glTranslatef(0.6f - commonTransalteValue, 0.0f, -6.0f);
		glScalef(scaleValue, scaleValue, scaleValue);
		displayD(-0.45f, 0.45f, 0.5f, 0.9f);


	}
	
	
	
	

	if (lettersCount > 910 + commonLettersCount && curvePlaneRotateCount < 1650) {
	//if (lettersCount > 0) {

		// top plane
		if (curvePlaneRotateCount < 2000) {
			glLoadIdentity();
			glTranslatef(0.2f, 0.0f, -4.0f);
			glTranslatef(curveArray[arrIndex].xCo, curveArray[arrIndex].yCo, 0.0f);
			glScalef(0.15f, 0.15f, 0.15f);
			//glRotatef(90.0f, 0.0f, 0.0f, -1.0f);

			glRotatef(rotateAngle1, 0.0f, 0.0, -1.0f);

			drawPlane();

		}
		// bottom plane
		if (curvePlaneRotateCount < 2000) {
			
			glLoadIdentity();
			glTranslatef(0.2f, 0.0f, -4.0f);
			glTranslatef(curveArray[arrIndex].xCo, -curveArray[arrIndex].yCo, 0.0f);
			glScalef(0.15f, 0.15f, 0.15f);
			//glRotatef(90.0f, 0.0f, 0.0f, -1.0f);

			glRotatef(rotateAngle2, 0.0f, 0.0, 1.0f);
			drawPlane();

		}
		// middle plane
		

		glLoadIdentity();
		glTranslatef(0.2f, 0.0f, -4.0f);
		glTranslatef(strightLineX, 0.0f, 0.0f);
		glScalef(0.15f, 0.15f, 0.15f);
		//glRotatef(90.0f, 0.0f, 0.0f, -1.0f);

		glRotatef(270.0f, 0.0f, 0.0, 1.0f);

		drawPlane();
	
		if (arrIndex < index) {

			arrIndex += 1;

		}

	}

	SwapBuffers(ghdc);


}



void update(void) {
	
	// code
	
	if(curvePlaneRotateCount > 1190)
		xValue += 0.005f;
	
	if (lettersCount != 0 &&  lettersCount < 30 + commonLettersCount) {
		
		iTranslate += 0.01f;
	
	} else if (lettersCount >= 30 + commonLettersCount && lettersCount < 380 + commonLettersCount) {
		
		nTranslate -= 0.01f;
	
	} else if(lettersCount > 380 + commonLettersCount) {
		
		if ((flag1 == 0 || flag2 == 0 || flag3 == 0 || flag4 == 0 || flag5 == 0 || flag6 == 0 || flag7 == 0) && lettersCount != 0) {

			orangeRColor += 1.0f;
			orangeGColor += 0.6f;
			orangeBColor += 0.2f;

			greenRColor += 0.1f;
			greenGColor += 1.7f;
			greenBColor += 0.1f;

			whiteRColor += 0.1f;
			whiteGColor += 0.1f;
			whiteBColor += 0.1f;

		}
	
	
	}	
	
	if (lettersCount > 500 + commonLettersCount && lettersCount < 750 + commonLettersCount) {

		iSecTranstate += 0.01f;

	} else if (lettersCount > 750 + commonLettersCount && lettersCount < 910 + commonLettersCount + 110) {
	
		aTranslate -= 0.01f;
	
	}


	if (curvePlaneRotateCount != 0 && curvePlaneRotateCount < 400) {
		
		rotateAngle1 -= 0.1f;
		rotateAngle2 -= 0.1f;
	
	}
	else if (curvePlaneRotateCount > 400 && curvePlaneRotateCount < 525) {
		

		rotateAngle1 -= 0.35f;
		rotateAngle2 -= 0.35f;

	
	}
	
	
	//else if(curvePlaneRotateCount > 870 && curvePlaneRotateCount < 960){

	else if (curvePlaneRotateCount > 1550 && curvePlaneRotateCount < 1650) {
		
		rotateAngle1 -= 0.1f;
		rotateAngle2 -= 0.1f;

	
	}
	else if(curvePlaneRotateCount > 1160 && curvePlaneRotateCount < 1550) {
	
		rotateAngle1 -= 0.2f;
		rotateAngle2 -= 0.2f;

	} else if(curvePlaneRotateCount > 500 && curvePlaneRotateCount < 1160){
	
		rotateAngle1 = 90.0f;
		rotateAngle2 = 270.0f;

	}


	
	if (curvePlaneRotateCount > 50 && curvePlaneRotateCount < 390) {
	
		strightLineX += 0.003f;


	} else if (curvePlaneRotateCount > 390 && curvePlaneRotateCount < 2000) {
			
		strightLineX += 0.005f;

	
	}
	
		
	// for middle plane

	if (startingAnimationCount > 270) {
	
		lettersCount++;


	}

	//if(lettersCount > 0)
	if(lettersCount > 910 + commonLettersCount)
		curvePlaneRotateCount++;

	// staring plane animation
	startAnimationX += 0.03f;





	startingAnimationCount += 1;
	planeRotate += 1.5f;



	/*if (animationCount < 150) {
		
		startAnimationX += 0.01f;

	} else if (animationCount > 150 && animationCount < 250) {
	
		startAnimationX += 0.02f;


	}
	else if (animationCount > 250 && animationCount < 400) {
		
		startAnimationX += 0.03f;

	
	}*/


}

void uninitialize(void) {
	
	// function declarations
	void ToggleFullScreen(void);
	
	// code
	if (gbFullScreen) {

		ToggleFullScreen();

	}

	if (wglGetCurrentContext() == ghrc) {
		
		wglMakeCurrent(NULL, NULL);

	}

	if (ghrc) {
		
		wglDeleteContext(ghrc);
		ghrc = NULL;
	
	}

	if (ghdc) {
		
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	
	}


	if (ghwnd) {
		
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	
	}

	if (gpFile) {

		fprintf(gpFile, "Log File Successfully Closed\n");
		fclose(gpFile);
		gpFile = NULL;

	}

}

