// header files
#include <windows.h>
#include "OGL.h"

#include <stdio.h>				//for file io functions
#include <stdlib.h>				//for exit()

// OpenGL header files
#include <GL/glew.h>			// This line must be above #include <GL/gl.h> (Compulsory)
#include <GL/gl.h>
#include "vmath.h"

using namespace vmath;

// OpenGL Libraries

#pragma comment (lib, "OpenGL32.lib")
#pragma comment (lib, "glew32.lib")

#define WIN_WIDTH	800			
#define WIN_HEIGHT	600



// Global Funtion Declaration
LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

//Global variable declaration
HWND ghwnd;
BOOL gbFullScreen = FALSE;
FILE *gpFile = NULL;
BOOL gbActiveWindow = FALSE;
BOOL bDone = FALSE;
int iRetVal = 0;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

// Programmable pipeline related gloabal variables
GLuint shaderProgramObject;

enum {

	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTURE0

};

GLfloat anglePyramid = 0.0f;
GLfloat angleCube = 0.0f;

GLuint vao_pyramid;
GLuint vao_cube;

GLuint vbo_pyramid_position;
GLuint vbo_pyramid_texcoord;
GLuint vbo_cube_position;
GLuint vbo_cube_texcoord;

GLuint texture_stone;
GLuint texture_kundali;

GLuint mvpMatrixUniform;
GLuint textureSamplerUniform;

mat4 perspectiveProjectionMatrix;


// Entry Point Function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {

	// function declarations
	int initialize(void);
	void display(void);
	void update(void);
	void uninitialize(void);

	// variable declarations 
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");
	//int width = 800, height = 600;
	int SysWidth = GetSystemMetrics(SM_CXSCREEN);
	int SysHeight = GetSystemMetrics(SM_CYSCREEN);

	// code

	if (fopen_s(&gpFile, "Log.txt", "w") != 0) {

		MessageBox(NULL, TEXT("Creation Of Log File Failed Exiting..."), TEXT("File I/O Error"), MB_OK);
		exit(0);

	}
	else {

		fprintf(gpFile, "Log File Successfully Opned\n");

	}


	// initalization of WNDCLASSEX structure

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;													//handle
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);					//handle
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));								//handle	
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);									//handle
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));


	// registering above wndclass
	RegisterClassEx(&wndclass);

	// create the window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("OGL Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(SysWidth / 2) - WIN_WIDTH / 2,
		(SysHeight / 2) - WIN_HEIGHT / 2,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	// initialize
	iRetVal = initialize();
	if (iRetVal == -1) {

		fprintf(gpFile, "ChoosePixelFormat falied\n");
		uninitialize();

	}
	else if (iRetVal == -2) {

		fprintf(gpFile, "SetPixelFormat falied\n");
		uninitialize();
	}
	else if (iRetVal == -3) {

		fprintf(gpFile, "wglCreateContext falied\n");
		uninitialize();
	}
	else if (iRetVal == -4) {

		fprintf(gpFile, "making OpenGL context as current context failed\n");
		uninitialize();
	
	}
	else if (iRetVal == -5) {
		
		fprintf(gpFile, "glewInit() failed");
		uninitialize();
	
	}
	else if (iRetVal == -6) {

		fprintf(gpFile, "LoadGLTexture() failed");
		uninitialize();

	}
	else {
		
		fprintf(gpFile, "call to initalize successfull\n");
	}


	// show window
	ShowWindow(hwnd, iCmdShow);

	//Foregrounding and focusing the window
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	

	// GameLoop
	while (bDone == FALSE) {
		
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			
			if (msg.message == WM_QUIT) {
				
				bDone = TRUE;
			
			} else {
				
				TranslateMessage(&msg);
				DispatchMessage(&msg);

			}
		
		} else {
		
			if (gbActiveWindow == TRUE) {
				
				// Render the scene
				display();

				// update the scene
				update();
			
			}
		}
	
	
	}
		
	uninitialize();													//change done
	return ((int) msg.wParam);
}

// Callback Function
LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	
	int width, height;
	TCHAR str[255];

	// function declarations
	void ToggleFullScreen(void);
	void resize(int, int);
	//void uninitialize(void);

	// code
	switch (iMsg) {
		
		case WM_SETFOCUS:
			gbActiveWindow = TRUE;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = FALSE;
			break;
		case WM_ERASEBKGND:
			return 0;
		case WM_CHAR:
			switch (wParam) {
				
				case 27:
					PostQuitMessage(0);
					break;
				case 'F':
				case 'f':
					ToggleFullScreen();
					break;
				
				default:
					break;
			}
			break;
		case WM_SIZE:
			resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_CLOSE:
			DestroyWindow(hWnd);
			break;
		case WM_DESTROY:
			//uninitialize();
			PostQuitMessage(0);
			break;
		
		default:
			break;
	}

	return (DefWindowProc(hWnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void) {

	//Variable Declarations
	static DWORD dwStyle;
	static WINDOWPLACEMENT wp;
	MONITORINFO mi;

	//code
	wp.length = sizeof(WINDOWPLACEMENT);

	if (gbFullScreen == FALSE) {

		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) {

			mi.cbSize = sizeof(MONITORINFO);

			if (GetWindowPlacement(ghwnd, &wp) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {

				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}

			ShowCursor(FALSE);
			gbFullScreen = TRUE;

		}

	}
	else {

		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wp);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
		gbFullScreen = FALSE;
	}
}

int initialize(void) {

	// function declarations
	// warmup resize
	void resize(int, int);
	void printGLInfo(void);
	void uninitialize(void);
	BOOL LoadGLTexture(GLuint*, TCHAR[]);


	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	// code
	// Initialization of PIXELFORMATDESCRIPTOR structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 0;
	pfd.cDepthBits = 32;							//24 can also be done

	// Get DC
	ghdc = GetDC(ghwnd);

	// Choose pixel format
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
		return -1;

	// Set the choosen pixel format
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
		return -2;

	// Create OpenGL Rendaring Context
	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
		return -3;

	// make the rendaring context as a current context 
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
		return -4;

	// glew initialization
	if (glewInit() != GLEW_OK)
		return -5;

	// Print OpenGL Info 
	printGLInfo();

	// Vertex shader (Step 1)
	const GLchar* vertexShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"in vec4 a_position;" \
		"in vec2 a_texcoord;" \
		"uniform mat4 u_mvpMatrix;" \
		"out vec2 a_texcoord_out;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvpMatrix * a_position;" \
		"a_texcoord_out = a_texcoord;" \
		"}";

	// (Step 2)
	GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// (Step 3)
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// (Step 4)
	glCompileShader(vertexShaderObject);

	// (Step 5)
	GLint status;
	GLint infoLogLength;
	char* log = NULL;

	//(a)
	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE) {				// Error is present

		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0) {

			log = (char*)malloc(infoLogLength);

			if (log != NULL) {

				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", log);
				free(log);
				uninitialize();
			}
		}

	}

	// Fragment shader
	const GLchar* fragmentShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"in vec2 a_texcoord_out;" \
		"uniform sampler2D u_textureSampler;" \
		"out vec4 FragColor;" \
		"vec2 Scale = vec2(10.0, 10.0);" \
		"vec2 Threshold = vec2(0.13, 0.13);" \
		"void main(void)" \
		"{" \
		"float ss = fract(a_texcoord_out.s * Scale.s);" \
		"float tt = fract(a_texcoord_out.t * Scale.t);" \
		"if((ss > a_texcoord_out.s) && (tt > Threshold.t)) discard;" \
		"FragColor = texture(u_textureSampler, a_texcoord_out);" \
		"}";

	GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	glCompileShader(fragmentShaderObject);

	status = 0;
	infoLogLength = 0;
	log = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status);

	if (status == FALSE) {

		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0) {

			log = (char*)malloc(infoLogLength);
			if (log != NULL) {

				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", log);
				free(log);
				uninitialize();

			}


		}

	}

	// Shader program object.
	shaderProgramObject = glCreateProgram();

	// attach desired shader to 
	glAttachShader(shaderProgramObject, vertexShaderObject);
	glAttachShader(shaderProgramObject, fragmentShaderObject);

	// Step A)-1
	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "a_position");				// andhar
	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_TEXTURE0, "a_texcoord");

	glLinkProgram(shaderProgramObject);

	status = 0;
	infoLogLength = 0;
	log = NULL;

	glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status);

	if (status == GL_FALSE) {

		glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0) {

			log = (char*)malloc(infoLogLength);
			if (log != NULL) {

				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);
				fprintf(gpFile, "Shader program link error log : %s\n", log);
				free(log);
				uninitialize();

			}


		}


	}

	mvpMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_mvpMatrix");
	textureSamplerUniform = glGetUniformLocation(shaderProgramObject, "u_textureSampler");

	// declaration of vertex data arrays;
	const GLfloat pyramidPosition[] = {

		// front
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

		// right
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,

		// back
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,

		// left
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f

	};

	const GLfloat pyramidTexcoord[] = {

		0.5, 1.0, // front-top
		0.0, 0.0, // front-left
		1.0, 0.0, // front-right

		0.5, 1.0, // right-top
		1.0, 0.0, // right-left
		0.0, 0.0, // right-right

		0.5, 1.0, // back-top
		1.0, 0.0, // back-left
		0.0, 0.0, // back-right

		0.5, 1.0, // left-top
		0.0, 0.0, // left-left
		1.0, 0.0, // left-right


	};

	const GLfloat cubePosition[] = {


		// top
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		// bottom
		1.0f, -1.0f, -1.0f,
	   -1.0f, -1.0f, -1.0f,
	   -1.0f, -1.0f,  1.0f,
		1.0f, -1.0f,  1.0f,

		// front
		1.0f, 1.0f, 1.0f,
	   -1.0f, 1.0f, 1.0f,
	   -1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

		// back
		1.0f, 1.0f, -1.0f,
	   -1.0f, 1.0f, -1.0f,
	   -1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		// right
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,

		// left
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,


	};

	const GLfloat cubeTexcoord[] = {

		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,

		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,

		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,

		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,

		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,

		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,

	};

	// recording concept 
	// vao and vbo related code
	// Pyramid
	glGenVertexArrays(1, &vao_pyramid);
	glBindVertexArray(vao_pyramid);
	// very important
	// vbo for postion
	glGenBuffers(1, &vbo_pyramid_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramid_position);
	
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidPosition), pyramidPosition, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	// vbo for texcoord pyramid
	glGenBuffers(1, &vbo_pyramid_texcoord);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramid_texcoord);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidTexcoord), pyramidTexcoord, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0);
	
	// unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// Cube
	glGenVertexArrays(1, &vao_cube);
	glBindVertexArray(vao_cube);
	// very important
	// vbo for postion
	glGenBuffers(1, &vbo_cube_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_position);

	glBufferData(GL_ARRAY_BUFFER, sizeof(cubePosition), cubePosition, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	// Cube color
	glGenBuffers(1, &vbo_cube_texcoord);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_texcoord);

	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTexcoord), cubeTexcoord, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	// Depth related changes
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);


	if (LoadGLTexture(&texture_stone, MAKEINTRESOURCE(IDBITMAP_STONE)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_stone");
		uninitialize();
		return -5;

	}

	if (LoadGLTexture(&texture_kundali, MAKEINTRESOURCE(IDBITMAP_KUNDALI)) == FALSE) {

		fprintf(gpFile, "LoadGLTexture failed for texture_kundali");
		uninitialize();
		return -6;

	}

	glEnable(GL_TEXTURE_2D);

	// Here starts OpenGL code
	// Clear the screen using black color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	perspectiveProjectionMatrix = mat4::identity();

	//warmup resize call
	resize(WIN_WIDTH, WIN_HEIGHT);
	return 0;
}

void printGLInfo(void) {

	// local variable declarations
	GLint numExtensions = 0;

	// code
	fprintf(gpFile, "OpenGL Vendor : %s\n", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer : %s\n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);

	fprintf(gpFile, "Number of suported extensions : %d\n", numExtensions);
	for (int i = 0; i < numExtensions; i++) {
		
		fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS, i));
	
	}

}

void resize(int width, int height) {
	

	//variable declaration

	// code
	if (height == 0)			// to avoid "/ by 0" in future calls
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);


}

void display(void) {
	
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// use the shader program object
	glUseProgram(shaderProgramObject);

	// Pyramid

	// transformations
	mat4 translationMatrix = mat4::identity();
	mat4 rotationMatrix = mat4::identity();
	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();
	
	translationMatrix = vmath::translate(-1.5f, 0.0f, -6.0f);
	rotationMatrix = vmath::rotate(anglePyramid, 0.0f, 1.0f, 0.0f);

	// order is very important
	modelViewMatrix = translationMatrix * rotationMatrix;

	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(mvpMatrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_stone);
	glUniform1i(textureSamplerUniform, 0);

	glBindVertexArray(vao_pyramid);

	glDrawArrays(GL_TRIANGLES, 0, 12);
	
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);


	// cube

	// transformations
	translationMatrix = mat4::identity();
	mat4 scaleMatrix = mat4::identity();
	mat4 rotationMatrix_x = mat4::identity();
	mat4 rotationMatrix_y = mat4::identity();
	mat4 rotationMatrix_z = mat4::identity();
	rotationMatrix = mat4::identity();
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	translationMatrix = vmath::translate(1.5f, 0.0f, -6.0f);
	scaleMatrix = vmath::scale(0.75f, 0.75f, 0.75f);
	rotationMatrix_x = vmath::rotate(angleCube, 1.0f, 0.0f, 0.0f);
	rotationMatrix_y = vmath::rotate(angleCube, 0.0f, 1.0f, 0.0f);
	rotationMatrix_z = vmath::rotate(angleCube, 0.0f, 0.0f, 1.0f);

	rotationMatrix = rotationMatrix_x * rotationMatrix_y * rotationMatrix_z;

	modelViewMatrix = translationMatrix * scaleMatrix * rotationMatrix;

	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(mvpMatrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_kundali);
	glUniform1i(textureSamplerUniform, 0);

	glBindVertexArray(vao_cube);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);

	// unuse the shader program object
	glUseProgram(0);

	SwapBuffers(ghdc);

}

void update(void) {
	
	anglePyramid = anglePyramid + 1.0f;

	if (anglePyramid >= 360.0f) {
		
		anglePyramid = 0.0f;
 
	}
	angleCube = angleCube + 1.0f;
	if (angleCube >= 360.0f) {
		
		angleCube = 0.0f;
	}

}

void uninitialize(void) {

	// function declarations
	void ToggleFullScreen(void);

	// code
	if (gbFullScreen) {

		ToggleFullScreen();

	}
	
	if (texture_kundali) {
		
		glDeleteTextures(1, &texture_kundali);
	
	}

	if (texture_stone) {
		
		glDeleteTextures(1, &texture_stone);
	
	}

	// Deletion and uninitialization of vbo_color
	if (vbo_pyramid_texcoord) {

		glDeleteBuffers(1, &vbo_pyramid_texcoord);
		vbo_pyramid_texcoord = 0;
	
	}
	
	// Deletion and uninitialization of vbo
	if (vbo_pyramid_position) {
	
		glDeleteBuffers(1, &vbo_pyramid_position);
		vbo_pyramid_position = 0;
	}


	// Deletion and uninitialization of vao
	if (vao_pyramid) {
			
		glDeleteVertexArrays(1, &vao_pyramid);
		vao_pyramid = 0;
	}

	if (vbo_cube_texcoord) {
		
		glDeleteBuffers(1, &vbo_cube_texcoord);

	}
	if (vbo_cube_position) {
		
		glDeleteBuffers(1, &vbo_cube_position);
		vbo_cube_position = 0;
	}

	if (vao_cube) {
		
		glDeleteVertexArrays(1, &vao_cube);
		vao_cube = 0;
	}

	// shader uninitialization
	if (shaderProgramObject) {
		
		GLsizei numAttachedShaders;

		glUseProgram(shaderProgramObject);
		glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);
	
		GLuint* shaderObjects = NULL;

		shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));
		
		// filling this empty buffer 
		glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);

		for (GLsizei i = 0; i < numAttachedShaders; i++) {
			
			glDetachShader(shaderProgramObject, shaderObjects[i]);
			glDeleteShader(shaderObjects[i]);
			shaderObjects[i] = 0;
		
		}
		free(shaderObjects);
		shaderObjects = NULL;
		glUseProgram(0);
		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;

	}

	if (wglGetCurrentContext() == ghrc) {
		
		wglMakeCurrent(NULL, NULL);

	}

	if (ghrc) {
		
		wglDeleteContext(ghrc);
		ghrc = NULL;
	
	}

	if (ghdc) {
		
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	
	}


	if (ghwnd) {
		
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	
	}

	if (gpFile) {

		fprintf(gpFile, "Log File Successfully Closed\n");
		fclose(gpFile);
		gpFile = NULL;

	}

}


BOOL LoadGLTexture(GLuint* texture, TCHAR ImageResourceID[]) {


	// variable declaration

	HBITMAP hBitMap = NULL;
	BITMAP bmp;
	BOOL bResult = FALSE;

	// code
	hBitMap = (HBITMAP)LoadImage(GetModuleHandle(NULL),
		ImageResourceID,
		IMAGE_BITMAP,
		0,
		0,
		LR_CREATEDIBSECTION);

	if (hBitMap) {

		bResult = TRUE;
		GetObject(hBitMap, sizeof(bmp), &bmp);

		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);				// Changed 1 from 4 for better performance in PP

		glGenTextures(1, texture);							//2nd param is array

		glBindTexture(GL_TEXTURE_2D, *texture);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		// create the texture
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.bmBits);
		glGenerateMipmap(GL_TEXTURE_2D);
		
		glBindTexture(GL_TEXTURE_2D, 0);

		DeleteObject(hBitMap);


	}

	return bResult;
}

