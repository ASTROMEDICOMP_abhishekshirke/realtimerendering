// header files
#include <windows.h>
#include "OGL.h"

#include <stdio.h>				//for file io functions
#include <stdlib.h>				//for exit()
#include <math.h>

// OpenGL header files
#include <GL/glew.h>			// This line must be above #include <GL/gl.h> (Compulsory)
#include <GL/gl.h>
#include "vmath.h"

using namespace vmath;

// OpenGL Libraries

#pragma comment (lib, "OpenGL32.lib")
#pragma comment (lib, "glew32.lib")

#define WIN_WIDTH	800			
#define WIN_HEIGHT	600



// Global Funtion Declaration
LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

//Global variable declaration
HWND ghwnd;
BOOL gbFullScreen = FALSE;
FILE *gpFile = NULL;
BOOL gbActiveWindow = FALSE;
BOOL bDone = FALSE;
int iRetVal = 0;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

// Programmable pipeline related gloabal variables
GLuint shaderProgramObject;

enum {

	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTURE0

};

GLuint vao;
GLuint vao_vericalLine;
GLuint vao_square;
GLuint vao_triangle;
GLuint vao_circle;

GLuint vbo_position;
GLuint vbo_postionVertical;
GLuint vbo_color;
GLuint vbo_square;
GLuint vbo_triangle;
GLuint vbo_circle;

GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint projectionMatrixUniform;
GLuint colorUniform;

mat4 perspectiveProjectionMatrix;

struct point {

	float x;
	float y;

};

float theta = 0.0f;

// Entry Point Function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {

	// function declarations
	int initialize(void);
	void display(void);
	void update(void);
	void uninitialize(void);

	// variable declarations 
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");
	//int width = 800, height = 600;
	int SysWidth = GetSystemMetrics(SM_CXSCREEN);
	int SysHeight = GetSystemMetrics(SM_CYSCREEN);

	// code

	if (fopen_s(&gpFile, "Log.txt", "w") != 0) {

		MessageBox(NULL, TEXT("Creation Of Log File Failed Exiting..."), TEXT("File I/O Error"), MB_OK);
		exit(0);

	}
	else {

		fprintf(gpFile, "Log File Successfully Opned\n");

	}


	// initalization of WNDCLASSEX structure

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;													//handle
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);					//handle
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));								//handle	
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);									//handle
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));


	// registering above wndclass
	RegisterClassEx(&wndclass);

	// create the window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("OGL Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(SysWidth / 2) - WIN_WIDTH / 2,
		(SysHeight / 2) - WIN_HEIGHT / 2,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	// initialize
	iRetVal = initialize();
	if (iRetVal == -1) {

		fprintf(gpFile, "ChoosePixelFormat falied\n");
		uninitialize();

	}
	else if (iRetVal == -2) {

		fprintf(gpFile, "SetPixelFormat falied\n");
		uninitialize();
	}
	else if (iRetVal == -3) {

		fprintf(gpFile, "wglCreateContext falied\n");
		uninitialize();
	}
	else if (iRetVal == -4) {

		fprintf(gpFile, "making OpenGL context as current context failed\n");
		uninitialize();
	
	}
	else if (iRetVal == -5) {
		
		fprintf(gpFile, "glewInit() failed");
		uninitialize();
	
	}
	else {
		
		fprintf(gpFile, "call to initalize successfull\n");
	}


	// show window
	ShowWindow(hwnd, iCmdShow);

	//Foregrounding and focusing the window
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	

	// GameLoop
	while (bDone == FALSE) {
		
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			
			if (msg.message == WM_QUIT) {
				
				bDone = TRUE;
			
			} else {
				
				TranslateMessage(&msg);
				DispatchMessage(&msg);

			}
		
		} else {
		
			if (gbActiveWindow == TRUE) {
				
				// Render the scene
				display();

				// update the scene
				update();
			
			}
		}
	
	
	}
		
	uninitialize();													//change done
	return ((int) msg.wParam);
}

// Callback Function
LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	
	int width, height;
	TCHAR str[255];

	// function declarations
	void ToggleFullScreen(void);
	void resize(int, int);
	//void uninitialize(void);

	// code
	switch (iMsg) {
		
		case WM_SETFOCUS:
			gbActiveWindow = TRUE;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = FALSE;
			break;
		case WM_ERASEBKGND:
			return 0;
		case WM_CHAR:
			switch (wParam) {
				
				case 27:
					PostQuitMessage(0);
					break;
				case 'F':
				case 'f':
					ToggleFullScreen();
					break;
				
				default:
					break;
			}
			break;
		case WM_SIZE:
			resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_CLOSE:
			DestroyWindow(hWnd);
			break;
		case WM_DESTROY:
			//uninitialize();
			PostQuitMessage(0);
			break;
		
		default:
			break;
	}

	return (DefWindowProc(hWnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void) {

	//Variable Declarations
	static DWORD dwStyle;
	static WINDOWPLACEMENT wp;
	MONITORINFO mi;

	//code
	wp.length = sizeof(WINDOWPLACEMENT);

	if (gbFullScreen == FALSE) {

		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) {

			mi.cbSize = sizeof(MONITORINFO);

			if (GetWindowPlacement(ghwnd, &wp) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {

				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}

			ShowCursor(FALSE);
			gbFullScreen = TRUE;

		}

	}
	else {

		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wp);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
		gbFullScreen = FALSE;
	}
}

int initialize(void) {

	// function declarations
	// warmup resize
	void resize(int, int);
	void printGLInfo(void);
	void uninitialize(void);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	// code
	// Initialization of PIXELFORMATDESCRIPTOR structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 0;
	pfd.cDepthBits = 32;							//24 can also be done

	// Get DC
	ghdc = GetDC(ghwnd);

	// Choose pixel format
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
		return -1;

	// Set the choosen pixel format
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
		return -2;

	// Create OpenGL Rendaring Context
	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
		return -3;

	// make the rendaring context as a current context 
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
		return -4;

	// glew initialization
	if (glewInit() != GLEW_OK)
		return -5;

	// Print OpenGL Info 
	printGLInfo();

	// Vertex shader (Step 1)
	const GLchar* vertexShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"in vec4 a_position;" \
		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" \
		"}";

	// (Step 2)
	GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// (Step 3)
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// (Step 4)
	glCompileShader(vertexShaderObject);

	// (Step 5)
	GLint status;
	GLint infoLogLength;
	char* log = NULL;

	//(a)
	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE) {				// Error is present

		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0) {

			log = (char*)malloc(infoLogLength);

			if (log != NULL) {

				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", log);
				free(log);
				uninitialize();
			}
		}

	}

	// Fragment shader
	const GLchar* fragmentShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"uniform vec4 u_color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = u_color;" \
		"}";

	GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	glCompileShader(fragmentShaderObject);

	status = 0;
	infoLogLength = 0;
	log = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status);

	if (status == FALSE) {

		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0) {

			log = (char*)malloc(infoLogLength);
			if (log != NULL) {

				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", log);
				free(log);
				uninitialize();

			}


		}

	}

	// Shader program object.
	shaderProgramObject = glCreateProgram();

	// attach desired shader to 
	glAttachShader(shaderProgramObject, vertexShaderObject);
	glAttachShader(shaderProgramObject, fragmentShaderObject);

	// Step A)-1
	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "a_position");				// andhar

	glLinkProgram(shaderProgramObject);

	status = 0;
	infoLogLength = 0;
	log = NULL;

	glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status);

	if (status == GL_FALSE) {

		glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0) {

			log = (char*)malloc(infoLogLength);
			if (log != NULL) {

				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);
				fprintf(gpFile, "Shader program link error log : %s\n", log);
				free(log);
				uninitialize();

			}


		}


	}

	modelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_modelMatrix");
	viewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
	projectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");

	colorUniform = glGetUniformLocation(shaderProgramObject, "u_color");

	// declaration of vertex data arrays;
	const GLfloat lineVerticesHorizontal[] = {

		-3.0f, 0.0f, 0.0f,
		3.0f, 0.0f, 0.0f
	};

	const GLfloat lineVerticesVertical[] = {

		0.0f, 1.75f, 0.0f,
		0.0f, -1.75f, 0.0f

	};


	const GLfloat square[] = {

		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
	};

	const GLfloat triangle[] = {

		/*0.0, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f*/

		0.000796f, 1.000000f, 1.000000f,
		-0.708513f, -0.705698f, 1.000000f,
		0.705133f, -0.709075f, 1.000000f
	};

	float circle[] = {
	
		1.000000,0.000000,0.000000,
		0.999848,0.017444,0.000000,
		0.999391,0.034882,0.000000,
		0.998631,0.052309,0.000000,
		0.997567,0.069721,0.000000,
		0.996199,0.087112,0.000000,
		0.994527,0.104476,0.000000,
		0.992554,0.121808,0.000000,
		0.990278,0.139103,0.000000,
		0.987701,0.156356,0.000000,
		0.984823,0.173561,0.000000,
		0.981646,0.190713,0.000000,
		0.978170,0.207808,0.000000,
		0.974396,0.224839,0.000000,
		0.970326,0.241802,0.000000,
		0.965960,0.258691,0.000000,
		0.961301,0.275501,0.000000,
		0.956349,0.292228,0.000000,
		0.951106,0.308866,0.000000,
		0.945573,0.325409,0.000000,
		0.939753,0.341854,0.000000,
		0.933647,0.358194,0.000000,
		0.927257,0.374426,0.000000,
		0.920584,0.390544,0.000000,
		0.913632,0.406543,0.000000,
		0.906401,0.422418,0.000000,
		0.898895,0.438164,0.000000,
		0.891115,0.453778,0.000000,
		0.883064,0.469253,0.000000,
		0.874744,0.484585,0.000000,
		0.866158,0.499770,0.000000,
		0.857309,0.514803,0.000000,
		0.848198,0.529679,0.000000,
		0.838830,0.544394,0.000000,
		0.829206,0.558943,0.000000,
		0.819330,0.573323,0.000000,
		0.809204,0.587528,0.000000,
		0.798832,0.601554,0.000000,
		0.788218,0.615396,0.000000,
		0.777363,0.629052,0.000000,
		0.766272,0.642516,0.000000,
		0.754948,0.655785,0.000000,
		0.743393,0.668854,0.000000,
		0.731613,0.681720,0.000000,
		0.719610,0.694378,0.000000,
		0.707388,0.706825,0.000000,
		0.694951,0.719057,0.000000,
		0.682302,0.731070,0.000000,
		0.669446,0.742861,0.000000,
		0.656386,0.754425,0.000000,
		0.643126,0.765760,0.000000,
		0.629671,0.776862,0.000000,
		0.616024,0.787727,0.000000,
		0.602189,0.798353,0.000000,
		0.588172,0.808736,0.000000,
		0.573975,0.818873,0.000000,
		0.559604,0.828760,0.000000,
		0.545062,0.838396,0.000000,
		0.530354,0.847776,0.000000,
		0.515485,0.856898,0.000000,
		0.500460,0.865760,0.000000,
		0.485282,0.874358,0.000000,
		0.469956,0.882690,0.000000,
		0.454487,0.890753,0.000000,
		0.438880,0.898546,0.000000,
		0.423139,0.906065,0.000000,
		0.407270,0.913308,0.000000,
		0.391277,0.920273,0.000000,
		0.375164,0.926958,0.000000,
		0.358938,0.933361,0.000000,
		0.342602,0.939481,0.000000,
		0.326162,0.945314,0.000000,
		0.309623,0.950859,0.000000,
		0.292989,0.956116,0.000000,
		0.276267,0.961081,0.000000,
		0.259460,0.965754,0.000000,
		0.242574,0.970133,0.000000,
		0.225615,0.974217,0.000000,
		0.208587,0.978004,0.000000,
		0.191495,0.981494,0.000000,
		0.174345,0.984685,0.000000,
		0.157142,0.987576,0.000000,
		0.139892,0.990167,0.000000,
		0.122598,0.992456,0.000000,
		0.105268,0.994444,0.000000,
		0.087905,0.996129,0.000000,
		0.070516,0.997511,0.000000,
		0.053105,0.998589,0.000000,
		0.035678,0.999363,0.000000,
		0.018240,0.999834,0.000000,
		0.000796,1.000000,0.000000,
		-0.016647,0.999861,0.000000,
		-0.034086,0.999419,0.000000,
		-0.051514,0.998672,0.000000,
		-0.068927,0.997622,0.000000,
		-0.086318,0.996268,0.000000,
		-0.103684,0.994610,0.000000,
		-0.121017,0.992650,0.000000,
		-0.138314,0.990388,0.000000,
		-0.155569,0.987825,0.000000,
		-0.172777,0.984961,0.000000,
		-0.189932,0.981797,0.000000,
		-0.207029,0.978335,0.000000,
		-0.224063,0.974575,0.000000,
		-0.241029,0.970518,0.000000,
		-0.257922,0.966166,0.000000,
		-0.274736,0.961520,0.000000,
		-0.291466,0.956581,0.000000,
		-0.308108,0.951351,0.000000,
		-0.324656,0.945832,0.000000,
		-0.341105,0.940025,0.000000,
		-0.357451,0.933932,0.000000,
		-0.373688,0.927555,0.000000,
		-0.389811,0.920895,0.000000,
		-0.405815,0.913955,0.000000,
		-0.421696,0.906737,0.000000,
		-0.437448,0.899244,0.000000,
		-0.453068,0.891476,0.000000,
		-0.468549,0.883437,0.000000,
		-0.483888,0.875130,0.000000,
		-0.499080,0.866556,0.000000,
		-0.514120,0.857718,0.000000,
		-0.529004,0.848620,0.000000,
		-0.543726,0.839263,0.000000,
		-0.558283,0.829651,0.000000,
		-0.572670,0.819786,0.000000,
		-0.586883,0.809672,0.000000,
		-0.600917,0.799311,0.000000,
		-0.614769,0.788707,0.000000,
		-0.628433,0.777864,0.000000,
		-0.641906,0.766783,0.000000,
		-0.655184,0.755469,0.000000,
		-0.668262,0.743926,0.000000,
		-0.681137,0.732156,0.000000,
		-0.693805,0.720163,0.000000,
		-0.706262,0.707951,0.000000,
		-0.718503,0.695524,0.000000,
		-0.730527,0.682884,0.000000,
		-0.742327,0.670038,0.000000,
		-0.753902,0.656987,0.000000,
		-0.765248,0.643736,0.000000,
		-0.776360,0.630289,0.000000,
		-0.787237,0.616651,0.000000,
		-0.797873,0.602825,0.000000,
		-0.808267,0.588816,0.000000,
		-0.818416,0.574627,0.000000,
		-0.828314,0.560263,0.000000,
		-0.837961,0.545729,0.000000,
		-0.847353,0.531029,0.000000,
		-0.856488,0.516168,0.000000,
		-0.865361,0.501149,0.000000,
		-0.873971,0.485978,0.000000,
		-0.882315,0.470659,0.000000,
		-0.890391,0.455196,0.000000,
		-0.898196,0.439595,0.000000,
		-0.905727,0.423861,0.000000,
		-0.912983,0.407997,0.000000,
		-0.919961,0.392009,0.000000,
		-0.926659,0.375902,0.000000,
		-0.933075,0.359681,0.000000,
		-0.939207,0.343350,0.000000,
		-0.945054,0.326915,0.000000,
		-0.950613,0.310380,0.000000,
		-0.955882,0.293751,0.000000,
		-0.960861,0.277032,0.000000,
		-0.965547,0.260229,0.000000,
		-0.969939,0.243347,0.000000,
		-0.974037,0.226390,0.000000,
		-0.977837,0.209365,0.000000,
		-0.981341,0.192277,0.000000,
		-0.984545,0.175129,0.000000,
		-0.987451,0.157929,0.000000,
		-0.990055,0.140680,0.000000,
		-0.992358,0.123389,0.000000,
		-0.994360,0.106059,0.000000,
		-0.996059,0.088698,0.000000,
		-0.997454,0.071310,0.000000,
		-0.998546,0.053900,0.000000,
		-0.999335,0.036473,0.000000,
		-0.999819,0.019036,0.000000,
		-0.999999,0.001593,0.000000,
		-0.999874,-0.015851,0.000000,
		-0.999446,-0.033290,0.000000,
		-0.998713,-0.050719,0.000000,
		-0.997676,-0.068132,0.000000,
		-0.996336,-0.085525,0.000000,
		-0.994693,-0.102892,0.000000,
		-0.992746,-0.120227,0.000000,
		-0.990498,-0.137526,0.000000,
		-0.987949,-0.154782,0.000000,
		-0.985098,-0.171992,0.000000,
		-0.981948,-0.189150,0.000000,
		-0.978499,-0.206250,0.000000,
		-0.974753,-0.223287,0.000000,
		-0.970710,-0.240256,0.000000,
		-0.966371,-0.257152,0.000000,
		-0.961738,-0.273970,0.000000,
		-0.956813,-0.290704,0.000000,
		-0.951596,-0.307350,0.000000,
		-0.946090,-0.323903,0.000000,
		-0.940296,-0.340357,0.000000,
		-0.934216,-0.356707,0.000000,
		-0.927852,-0.372949,0.000000,
		-0.921205,-0.389077,0.000000,
		-0.914278,-0.405087,0.000000,
		-0.907073,-0.420974,0.000000,
		-0.899592,-0.436732,0.000000,
		-0.891837,-0.452358,0.000000,
		-0.883810,-0.467846,0.000000,
		-0.875515,-0.483191,0.000000,
		-0.866953,-0.498390,0.000000,
		-0.858127,-0.513437,0.000000,
		-0.849041,-0.528328,0.000000,
		-0.839695,-0.543058,0.000000,
		-0.830095,-0.557622,0.000000,
		-0.820242,-0.572017,0.000000,
		-0.810139,-0.586238,0.000000,
		-0.799789,-0.600281,0.000000,
		-0.789197,-0.614140,0.000000,
		-0.778364,-0.627813,0.000000,
		-0.767294,-0.641295,0.000000,
		-0.755991,-0.654582,0.000000,
		-0.744458,-0.667670,0.000000,
		-0.732698,-0.680554,0.000000,
		-0.720715,-0.693231,0.000000,
		-0.708513,-0.705698,0.000000,
		-0.696095,-0.717949,0.000000,
		-0.683466,-0.729982,0.000000,
		-0.670628,-0.741793,0.000000,
		-0.657587,-0.753379,0.000000,
		-0.644345,-0.764735,0.000000,
		-0.630907,-0.775858,0.000000,
		-0.617278,-0.786745,0.000000,
		-0.603460,-0.797393,0.000000,
		-0.589459,-0.807798,0.000000,
		-0.575279,-0.817958,0.000000,
		-0.560923,-0.827868,0.000000,
		-0.546397,-0.837527,0.000000,
		-0.531704,-0.846930,0.000000,
		-0.516849,-0.856076,0.000000,
		-0.501838,-0.864962,0.000000,
		-0.486674,-0.873584,0.000000,
		-0.471361,-0.881940,0.000000,
		-0.455905,-0.890028,0.000000,
		-0.440311,-0.897846,0.000000,
		-0.424582,-0.905390,0.000000,
		-0.408724,-0.912658,0.000000,
		-0.392742,-0.919649,0.000000,
		-0.376640,-0.926360,0.000000,
		-0.360424,-0.932789,0.000000,
		-0.344098,-0.938934,0.000000,
		-0.327667,-0.944793,0.000000,
		-0.311137,-0.950365,0.000000,
		-0.294512,-0.955648,0.000000,
		-0.277797,-0.960640,0.000000,
		-0.260998,-0.965339,0.000000,
		-0.244119,-0.969745,0.000000,
		-0.227166,-0.973856,0.000000,
		-0.210144,-0.977670,0.000000,
		-0.193058,-0.981187,0.000000,
		-0.175913,-0.984406,0.000000,
		-0.158715,-0.987324,0.000000,
		-0.141468,-0.989943,0.000000,
		-0.124179,-0.992260,0.000000,
		-0.106851,-0.994275,0.000000,
		-0.089491,-0.995988,0.000000,
		-0.072104,-0.997397,0.000000,
		-0.054695,-0.998503,0.000000,
		-0.037269,-0.999305,0.000000,
		-0.019832,-0.999803,0.000000,
		-0.002389,-0.999997,0.000000,
		0.015055,-0.999887,0.000000,
		0.032494,-0.999472,0.000000,
		0.049924,-0.998753,0.000000,
		0.067338,-0.997730,0.000000,
		0.084731,-0.996404,0.000000,
		0.102100,-0.994774,0.000000,
		0.119436,-0.992842,0.000000,
		0.136737,-0.990607,0.000000,
		0.153996,-0.988072,0.000000,
		0.171208,-0.985235,0.000000,
		0.188368,-0.982099,0.000000,
		0.205471,-0.978663,0.000000,
		0.222510,-0.974930,0.000000,
		0.239483,-0.970901,0.000000,
		0.256383,-0.966575,0.000000,
		0.273204,-0.961956,0.000000,
		0.289942,-0.957044,0.000000,
		0.306593,-0.951841,0.000000,
		0.323149,-0.946348,0.000000,
		0.339608,-0.940567,0.000000,
		0.355963,-0.934500,0.000000,
		0.372210,-0.928149,0.000000,
		0.388343,-0.921515,0.000000,
		0.404359,-0.914600,0.000000,
		0.420251,-0.907408,0.000000,
		0.436016,-0.899939,0.000000,
		0.451648,-0.892196,0.000000,
		0.467142,-0.884182,0.000000,
		0.482494,-0.875899,0.000000,
		0.497699,-0.867350,0.000000,
		0.512753,-0.858536,0.000000,
		0.527651,-0.849461,0.000000,
		0.542388,-0.840128,0.000000,
		0.556961,-0.830539,0.000000,
		0.571364,-0.820697,0.000000,
		0.585593,-0.810606,0.000000,
		0.599643,-0.800267,0.000000,
		0.613512,-0.789686,0.000000,
		0.627193,-0.778864,0.000000,
		0.640684,-0.767805,0.000000,
		0.653980,-0.756512,0.000000,
		0.667076,-0.744989,0.000000,
		0.679970,-0.733240,0.000000,
		0.692657,-0.721267,0.000000,
		0.705133,-0.709075,0.000000,
		0.717395,-0.696667,0.000000,
		0.729438,-0.684047,0.000000,
		0.741259,-0.671219,0.000000,
		0.752855,-0.658187,0.000000,
		0.764221,-0.644954,0.000000,
		0.775355,-0.631525,0.000000,
		0.786254,-0.617904,0.000000,
		0.796912,-0.604095,0.000000,
		0.807329,-0.590102,0.000000,
		0.817499,-0.575930,0.000000,
		0.827421,-0.561582,0.000000,
		0.837091,-0.547063,0.000000,
		0.846507,-0.532378,0.000000,
		0.855664,-0.517531,0.000000,
		0.864562,-0.502527,0.000000,
		0.873196,-0.487369,0.000000,
		0.881565,-0.472063,0.000000,
		0.889665,-0.456614,0.000000,
		0.897495,-0.441025,0.000000,
		0.905051,-0.425303,0.000000,
		0.912332,-0.409451,0.000000,
		0.919336,-0.393474,0.000000,
		0.926059,-0.377378,0.000000,
		0.932501,-0.361167,0.000000,
		0.938660,-0.344845,0.000000,
		0.944532,-0.328420,0.000000,
		0.950117,-0.311893,0.000000,
		0.955413,-0.295273,0.000000,
		0.960418,-0.278562,0.000000,
		0.965131,-0.261766,0.000000,
		0.969550,-0.244891,0.000000,
		0.973675,-0.227942,0.000000,
		0.977503,-0.210922,0.000000,
		0.981033,-0.193840,0.000000,
		0.984265,-0.176697,0.000000,
		0.987198,-0.159501,0.000000,
		0.989830,-0.142257,0.000000,
		0.992161,-0.124969,0.000000,
		0.994190,-0.107643,0.000000,
		0.995916,-0.090285,0.000000,
		0.997339,-0.072898,0.000000,
		0.998459,-0.055490,0.000000,
		0.999275,-0.038065,0.000000,
		0.999787,-0.020628,0.000000,
	
	};
	// recording concept 
	// vao and vbo related code
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	// very important
	// vbo for postion
	glGenBuffers(1, &vbo_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
	
	glBufferData(GL_ARRAY_BUFFER, sizeof(lineVerticesHorizontal), lineVerticesHorizontal, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// Vertical Line
	glGenVertexArrays(1, &vao_vericalLine);
	glBindVertexArray(vao_vericalLine);
	// vbo for vertical lines
	glGenBuffers(1, &vbo_postionVertical);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_postionVertical);

	glBufferData(GL_ARRAY_BUFFER, sizeof(lineVerticesVertical), lineVerticesVertical, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// Square
	glGenVertexArrays(1, &vao_square);
	glBindVertexArray(vao_square);

	glGenBuffers(1, &vbo_square);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_square);
	glBufferData(GL_ARRAY_BUFFER, sizeof(square), square, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// Triangle
	glGenVertexArrays(1, &vao_triangle);
	glBindVertexArray(vao_triangle);

	glGenBuffers(1, &vbo_triangle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_triangle);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// Circle
	glGenVertexArrays(1, &vao_circle);
	glBindVertexArray(vao_circle);

	glGenBuffers(1, &vbo_circle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_circle);
	glBufferData(GL_ARRAY_BUFFER, sizeof(circle), circle, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	// Depth related changes
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);


	// Here starts OpenGL code
	// Clear the screen using black color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	perspectiveProjectionMatrix = mat4::identity();

	//warmup resize call
	resize(WIN_WIDTH, WIN_HEIGHT);
	return 0;
}

void printGLInfo(void) {

	// local variable declarations
	GLint numExtensions = 0;

	// code
	fprintf(gpFile, "OpenGL Vendor : %s\n", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer : %s\n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);

	fprintf(gpFile, "Number of suported extensions : %d\n", numExtensions);
	for (int i = 0; i < numExtensions; i++) {
		
		fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS, i));
	
	}

}

void resize(int width, int height) {
	

	//variable declaration

	// code
	if (height == 0)			// to avoid "/ by 0" in future calls
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);


}

void display(void) {
	
	// variables
	// For Horizontal Lines
	float top = 1.63f;
	float yDist = 0.0825f;

	// For Vertical Lines 
	float left = -2.92f;
	float xDist = 0.146f;

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// use the shader program object
	glUseProgram(shaderProgramObject);

	// transformations
	mat4 translationMatrix = mat4::identity();
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f);

	modelMatrix = translationMatrix;
	
	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
	// Horizontal Lines
	for (int i = 1; i < 41; i++) {
		
		if (i == 42 / 2) {
			
			glUniform4f(colorUniform, 1.0f, 0.0f, 0.0f, 1.0f);

		
		} else {
		
			glUniform4f(colorUniform, 0.0f, 0.0f, 1.0f, 1.0f);

		}
		translationMatrix = vmath::translate(0.0f, top, -4.0f);
		top = top - yDist;
		modelMatrix = translationMatrix;
		glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
		glBindVertexArray(vao);
		glDrawArrays(GL_LINES, 0, 2);
		glBindVertexArray(0);
	}

	// Vertical Lines
	for (int i = 1; i < 41; i++) {
	
		if (i == 42 / 2) {

			glUniform4f(colorUniform, 0.0f, 1.0f, 0.0f, 1.0f);


		}
		else {

			glUniform4f(colorUniform, 0.0f, 0.0f, 1.0f, 1.0f);

		}

		translationMatrix = vmath::translate(left, 0.0f, -4.0f);
		left = left + xDist;
		modelMatrix = translationMatrix;
		glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
		glBindVertexArray(vao_vericalLine);
		glDrawArrays(GL_LINES, 0, 2);
		glBindVertexArray(0);

	}

	// Rectangle
	translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f);
	modelMatrix = translationMatrix;
	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniform4f(colorUniform, 1.0f, 1.0f, 0.0f, 1.0f);
	glBindVertexArray(vao_square);
	glDrawArrays(GL_LINE_LOOP, 0, 4);
	glBindVertexArray(0);
		
	// Triangle
	translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f);
	modelMatrix = translationMatrix;
	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniform4f(colorUniform, 1.0f, 1.0f, 0.0f, 1.0f);
	glBindVertexArray(vao_triangle);
	glDrawArrays(GL_LINE_LOOP, 0, 3);
	glBindVertexArray(0);

	/*translationMatrix = vmath::translate(2.92f, 0.0f, -4.0f);
	modelMatrix = translationMatrix;
	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glBindVertexArray(vao_vericalLine);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);*/

	// Circle
	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix;
	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniform4f(colorUniform, 1.0f, 1.0f, 0.0f, 1.0f);
	glBindVertexArray(vao_circle);
	glDrawArrays(GL_POINTS, 0, 360);
	glBindVertexArray(0);

	/*translationMatrix = vmath::translate(2.92f, 0.0f, -4.0f);
	modelMatrix = translationMatrix;
	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glBindVertexArray(vao_vericalLine);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);*/


	// Here there should be drawing code

	// unuse the shader program object
	glUseProgram(0);

	SwapBuffers(ghdc);

}

void update(void) {
	
}

void uninitialize(void) {

	// function declarations
	void ToggleFullScreen(void);

	// code
	if (gbFullScreen) {

		ToggleFullScreen();

	}
	
	// Deletion and uninitialization of vbo_color
	if (vbo_color) {

		glDeleteBuffers(1, &vbo_color);
		vbo_color = 0;
	
	}
	
	// Deletion and uninitialization of vbo
	if (vbo_position) {
	
		glDeleteBuffers(1, &vbo_position);
		vbo_position = 0;
	}


	// Deletion and uninitialization of vao
	if (vao) {
			
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	// shader uninitialization
	if (shaderProgramObject) {
		
		GLsizei numAttachedShaders;

		glUseProgram(shaderProgramObject);
		glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);
	
		GLuint* shaderObjects = NULL;

		shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));
		
		// filling this empty buffer 
		glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);

		for (GLsizei i = 0; i < numAttachedShaders; i++) {
			
			glDetachShader(shaderProgramObject, shaderObjects[i]);
			glDeleteShader(shaderObjects[i]);
			shaderObjects[i] = 0;
		
		}
		free(shaderObjects);
		shaderObjects = NULL;
		glUseProgram(0);
		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;

	}

	if (wglGetCurrentContext() == ghrc) {
		
		wglMakeCurrent(NULL, NULL);

	}

	if (ghrc) {
		
		wglDeleteContext(ghrc);
		ghrc = NULL;
	
	}

	if (ghdc) {
		
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	
	}


	if (ghwnd) {
		
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	
	}

	if (gpFile) {

		fprintf(gpFile, "Log File Successfully Closed\n");
		fclose(gpFile);
		gpFile = NULL;

	}

}

