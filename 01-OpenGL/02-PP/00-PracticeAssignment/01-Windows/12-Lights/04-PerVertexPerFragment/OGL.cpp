// header files
#include <windows.h>
#include "OGL.h"

#include <stdio.h>				//for file io functions
#include <stdlib.h>				//for exit()

// OpenGL header files
#include <GL/glew.h>			// This line must be above #include <GL/gl.h> (Compulsory)
#include <GL/gl.h>
#include "vmath.h"
#include "Sphere.h"				// For sphere


using namespace vmath;

// OpenGL Libraries

#pragma comment (lib, "OpenGL32.lib")
#pragma comment (lib, "glew32.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH	800			
#define WIN_HEIGHT	600



// Global Funtion Declaration
LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

//Global variable declaration
HWND ghwnd;
BOOL gbFullScreen = FALSE;
FILE *gpFile = NULL;
BOOL gbActiveWindow = FALSE;
BOOL bDone = FALSE;
int iRetVal = 0;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

// Programmable pipeline related gloabal variables
GLuint shaderProgramObjectPerVertex;
GLuint shaderProgramObjectPerFragment;

enum {

	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTURE0

};

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint projectionMatrixUniform;

GLuint laUniformPerVertexLight;
GLuint ldUniformPerVertexLight;
GLuint lsUniformPerVertexLight;
GLuint lightPositionUniformPerVertexLight;

GLuint kaUniformPerVertexLight;
GLuint kdUniformPerVertexLight;
GLuint ksUniformPerVertexLight;
GLuint materialShininessUniformPerVertexLight;

GLuint lighitingEnabledUniformPerVertexLight;

GLuint laUniformPerFragmentLight;
GLuint ldUniformPerFragmentLight;
GLuint lsUniformPerFragmentLight;
GLuint lightPositionUniformPerFragmentLight;

GLuint kaUniformPerFragmentLight;
GLuint kdUniformPerFragmentLight;
GLuint ksUniformPerFragmentLight;
GLuint materialShininessUniformPerFragmentLight;

GLuint lighitingEnabledUniformPerFragmentLight;

mat4 perspectiveProjectionMatrix;

BOOL bLightPerVertex = FALSE;
BOOL bLightPerFragment = FALSE;
int perVertexLight = 0;
int perFragmentLight = 0;

GLfloat LightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat LightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat LightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat LightPosition[] = { 100.0f, 100.0f, 100.0f, 1.0f };

GLfloat MaterialAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat MaterialDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat MaterialSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat MaterialShininess = 50.0f;

// Sphere
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gNumVertices;
GLuint	gNumElements;

// Entry Point Function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {

	// function declarations
	int initialize(void);
	void display(void);
	void update(void);
	void uninitialize(void);

	// variable declarations 
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");
	//int width = 800, height = 600;
	int SysWidth = GetSystemMetrics(SM_CXSCREEN);
	int SysHeight = GetSystemMetrics(SM_CYSCREEN);

	// code

	if (fopen_s(&gpFile, "Log.txt", "w") != 0) {

		MessageBox(NULL, TEXT("Creation Of Log File Failed Exiting..."), TEXT("File I/O Error"), MB_OK);
		exit(0);

	}
	else {

		fprintf(gpFile, "Log File Successfully Opned\n");

	}


	// initalization of WNDCLASSEX structure

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;													//handle
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);					//handle
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));								//handle	
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);									//handle
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));


	// registering above wndclass
	RegisterClassEx(&wndclass);

	// create the window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("OGL Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(SysWidth / 2) - WIN_WIDTH / 2,
		(SysHeight / 2) - WIN_HEIGHT / 2,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	// initialize
	iRetVal = initialize();
	if (iRetVal == -1) {

		fprintf(gpFile, "ChoosePixelFormat falied\n");
		uninitialize();

	}
	else if (iRetVal == -2) {

		fprintf(gpFile, "SetPixelFormat falied\n");
		uninitialize();
	}
	else if (iRetVal == -3) {

		fprintf(gpFile, "wglCreateContext falied\n");
		uninitialize();
	}
	else if (iRetVal == -4) {

		fprintf(gpFile, "making OpenGL context as current context failed\n");
		uninitialize();
	
	}
	else if (iRetVal == -5) {
		
		fprintf(gpFile, "glewInit() failed");
		uninitialize();
	
	}
	else {
		
		fprintf(gpFile, "call to initalize successfull\n");
	}


	// show window
	ShowWindow(hwnd, iCmdShow);

	//Foregrounding and focusing the window
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	

	// GameLoop
	while (bDone == FALSE) {
		
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			
			if (msg.message == WM_QUIT) {
				
				bDone = TRUE;
			
			} else {
				
				TranslateMessage(&msg);
				DispatchMessage(&msg);

			}
		
		} else {
		
			if (gbActiveWindow == TRUE) {
				
				// Render the scene
				display();

				// update the scene
				update();
			
			}
		}
	
	
	}
		
	uninitialize();													//change done
	return ((int) msg.wParam);
}

// Callback Function
LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	
	int width, height;
	TCHAR str[255];

	// function declarations
	void ToggleFullScreen(void);
	void resize(int, int);
	//void uninitialize(void);

	// code
	switch (iMsg) {
		
		case WM_SETFOCUS:
			gbActiveWindow = TRUE;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = FALSE;
			break;
		case WM_ERASEBKGND:
			return 0;
		case WM_CHAR:
			switch (wParam) {
				
				case 'Q':
				case 'q':
					PostQuitMessage(0);
					break;
				case 27:
					ToggleFullScreen();
					break;
				case 'F':
				case 'f':
					if (perFragmentLight == 0) {
						
						perFragmentLight = 1;
						perVertexLight = 0;
					
					} else {
					
						perFragmentLight = 0;

					}
					if (bLightPerFragment == FALSE) {
						
						bLightPerFragment = TRUE;
						bLightPerVertex = FALSE;
					
					} else {
						
						bLightPerFragment = FALSE;
					}

					break;
				case 'L':
				case 'l':

					if (perVertexLight == 0) {
						
						perVertexLight = 1;
						perFragmentLight = 0;
					
					} else {
					
						perVertexLight = 0;

					}

					if (bLightPerVertex == FALSE) {

						bLightPerVertex = TRUE;
						bLightPerFragment = FALSE;


					}
					else {

						bLightPerVertex = FALSE;
					}
					break;
				case 'V':
				case 'v':
					if (perVertexLight == 0) {

						perVertexLight = 1;
						perFragmentLight = 0;

					}
					else {

						perVertexLight = 0;

					}
					if (bLightPerVertex == FALSE) {

						bLightPerVertex = TRUE;
						bLightPerFragment = FALSE;
					}
					else {

						bLightPerVertex = FALSE;
					}
					break;
				default:
					break;
			}
			break;
		case WM_SIZE:
			resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_CLOSE:
			DestroyWindow(hWnd);
			break;
		case WM_DESTROY:
			//uninitialize();
			PostQuitMessage(0);
			break;
		
		default:
			break;
	}

	return (DefWindowProc(hWnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void) {

	//Variable Declarations
	static DWORD dwStyle;
	static WINDOWPLACEMENT wp;
	MONITORINFO mi;

	//code
	wp.length = sizeof(WINDOWPLACEMENT);

	if (gbFullScreen == FALSE) {

		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) {

			mi.cbSize = sizeof(MONITORINFO);

			if (GetWindowPlacement(ghwnd, &wp) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {

				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}

			ShowCursor(FALSE);
			gbFullScreen = TRUE;

		}

	}
	else {

		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wp);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
		gbFullScreen = FALSE;
	}
}

int initialize(void) {

	// function declarations
	// warmup resize
	void resize(int, int);
	void printGLInfo(void);
	void uninitialize(void);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	// code
	// Initialization of PIXELFORMATDESCRIPTOR structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 0;
	pfd.cDepthBits = 32;							//24 can also be done

	// Get DC
	ghdc = GetDC(ghwnd);

	// Choose pixel format
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
		return -1;

	// Set the choosen pixel format
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
		return -2;

	// Create OpenGL Rendaring Context
	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
		return -3;

	// make the rendaring context as a current context 
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
		return -4;

	// glew initialization
	if (glewInit() != GLEW_OK)
		return -5;


	// Sphere
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();


	// Print OpenGL Info 
	printGLInfo();

	// Vertex shader (Step 1)
	const GLchar* vertexShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"in vec3 a_normal;" \
		"in vec4 a_position;" \
		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec4 u_lightPosition;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_materailShininess;" \
		"uniform int u_lighitingEnabled;" \
		"out vec3 phong_ads_light;" \
		"void main(void)" \
		"{" \
		"if(u_lighitingEnabled == 1) {" \
			"vec3 ambient = u_la * u_ka;" \
			"vec4 eyeCoodinate = u_viewMatrix * u_modelMatrix * a_position;" \
			"mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" \
			"vec3 transformedNormals = normalize(normalMatrix * a_normal);" \
			"vec3 lightDirection = normalize(vec3(u_lightPosition) - eyeCoodinate.xyz);" \
			"vec3 diffuse = u_ld * u_kd * max(dot(lightDirection, transformedNormals), 0.0);" \
			"vec3 reflectionVector = reflect(-lightDirection, transformedNormals);" \
			"vec3 viewerVector = normalize(-eyeCoodinate.xyz);" \
			"vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector, viewerVector), 0.0), u_materailShininess);" \
			"phong_ads_light = ambient + diffuse + specular;" \
		"}" \
		"else {" \
			"phong_ads_light = vec3(1.0, 1.0, 1.0f);" \
		"}"
		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" \
		"}";

	// (Step 2)
	GLuint vertexShaderObjectPerVertex = glCreateShader(GL_VERTEX_SHADER);

	// (Step 3)
	glShaderSource(vertexShaderObjectPerVertex, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// (Step 4)
	glCompileShader(vertexShaderObjectPerVertex);

	// (Step 5)
	GLint status;
	GLint infoLogLength;
	char* log = NULL;

	//(a)
	glGetShaderiv(vertexShaderObjectPerVertex, GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE) {				// Error is present

		glGetShaderiv(vertexShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0) {

			log = (char*)malloc(infoLogLength);

			if (log != NULL) {

				GLsizei written;
				glGetShaderInfoLog(vertexShaderObjectPerVertex, infoLogLength, &written, log);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", log);
				free(log);
				uninitialize();
			}
		}

	}

	// Fragment shader
	const GLchar* fragmentShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"in vec3 phong_ads_light;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(phong_ads_light, 1.0);" \
		"}";

	GLuint fragmentShaderObjectPerVertex = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShaderObjectPerVertex, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	glCompileShader(fragmentShaderObjectPerVertex);

	status = 0;
	infoLogLength = 0;
	log = NULL;

	glGetShaderiv(fragmentShaderObjectPerVertex, GL_COMPILE_STATUS, &status);

	if (status == FALSE) {

		glGetShaderiv(fragmentShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0) {

			log = (char*)malloc(infoLogLength);
			if (log != NULL) {

				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObjectPerVertex, infoLogLength, &written, log);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", log);
				free(log);
				uninitialize();

			}


		}

	}

	// Shader program object.
	shaderProgramObjectPerVertex = glCreateProgram();

	// attach desired shader to 
	glAttachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);
	glAttachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);

	// Step A)-1
	glBindAttribLocation(shaderProgramObjectPerVertex, AMC_ATTRIBUTE_POSITION, "a_position");				// andhar
	glBindAttribLocation(shaderProgramObjectPerVertex, AMC_ATTRIBUTE_NORMAL, "a_normal");				// andhar
	
	
	glLinkProgram(shaderProgramObjectPerVertex);

	status = 0;
	infoLogLength = 0;
	log = NULL;

	glGetProgramiv(shaderProgramObjectPerVertex, GL_LINK_STATUS, &status);

	if (status == GL_FALSE) {

		glGetProgramiv(shaderProgramObjectPerVertex, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0) {

			log = (char*)malloc(infoLogLength);
			if (log != NULL) {

				GLsizei written;
				glGetProgramInfoLog(shaderProgramObjectPerVertex, infoLogLength, &written, log);
				fprintf(gpFile, "Shader program link error log : %s\n", log);
				free(log);
				uninitialize();

			}


		}


	}

	modelMatrixUniform = glGetUniformLocation(shaderProgramObjectPerVertex, "u_modelMatrix");
	viewMatrixUniform = glGetUniformLocation(shaderProgramObjectPerVertex, "u_viewMatrix");
	projectionMatrixUniform = glGetUniformLocation(shaderProgramObjectPerVertex, "u_projectionMatrix");

	laUniformPerVertexLight = glGetUniformLocation(shaderProgramObjectPerVertex, "u_la");
	ldUniformPerVertexLight = glGetUniformLocation(shaderProgramObjectPerVertex, "u_ld");
	lsUniformPerVertexLight = glGetUniformLocation(shaderProgramObjectPerVertex, "u_ls");
	lightPositionUniformPerVertexLight = glGetUniformLocation(shaderProgramObjectPerVertex, "u_lightPosition");
	kaUniformPerVertexLight = glGetUniformLocation(shaderProgramObjectPerVertex, "u_ka");
	kdUniformPerVertexLight = glGetUniformLocation(shaderProgramObjectPerVertex, "u_kd");
	ksUniformPerVertexLight = glGetUniformLocation(shaderProgramObjectPerVertex, "u_ks");
	materialShininessUniformPerVertexLight = glGetUniformLocation(shaderProgramObjectPerVertex, "u_materailShininess");
	lighitingEnabledUniformPerVertexLight = glGetUniformLocation(shaderProgramObjectPerVertex, "u_lighitingEnabled");

	
	
	//**************************************Per Fragment Light****************************//
	// Vertex shader (Step 1)
	const GLchar* vertexShaderSourceCodePerFragment =
		"#version 460 core" \
		"\n" \
		"in vec4 a_position;" \
		"in vec3 a_normal;"	\
		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \
		"uniform vec4 u_lightPosition;" \
		"uniform int u_lightingEnabled;" \
		"out vec3 transformedNormals;" \
		"out vec3 lightDirection;" \
		"out vec3 viewerVector;" \
		"void main(void)" \
		"{" \
		"if(u_lightingEnabled == 1)" \
		"{" \
		"vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" \
		"mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" \
		"transformedNormals = normalMatrix * a_normal;" \
		"lightDirection = vec3(u_lightPosition) - eyeCoordinates.xyz;" \
		"viewerVector = -eyeCoordinates.xyz;" \
		"}" \
		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" \
		"}";

	// (Step 2)
	GLuint vertexShaderObjectPerFragment = glCreateShader(GL_VERTEX_SHADER);

	// (Step 3)
	glShaderSource(vertexShaderObjectPerFragment, 1, (const GLchar**)&vertexShaderSourceCodePerFragment, NULL);

	// (Step 4)
	glCompileShader(vertexShaderObjectPerFragment);

	// (Step 5)
	GLint statusPerFragment = NULL;
	GLint infoLogLengthPerFragment;
	char* logPerFragment;
	//(a)
	glGetShaderiv(vertexShaderObjectPerFragment, GL_COMPILE_STATUS, &statusPerFragment);

	if (statusPerFragment == GL_FALSE) {				// Error is present

		glGetShaderiv(vertexShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &infoLogLengthPerFragment);
		if (infoLogLengthPerFragment > 0) {

			log = (char*)malloc(infoLogLengthPerFragment);

			if (log != NULL) {

				GLsizei written;
				glGetShaderInfoLog(vertexShaderObjectPerFragment, infoLogLengthPerFragment, &written, log);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", log);
				free(log);
				uninitialize();
			}
		}

	}

	// Fragment shader
	const GLchar* fragmentShaderSourceCodePerFragment =
		"#version 460 core" \
		"\n" \
		"in vec3 transformedNormals;" \
		"in vec3 lightDirection;" \
		"in vec3 viewerVector;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_matrialShininess;"	\
		"uniform int u_lightingEnabled;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_light;" \
		"if(u_lightingEnabled == 1)" \
		"{" \
		"vec3 ambient = u_la * u_ka;" \
		"vec3 normalized_transformed_normals = normalize(transformedNormals);" \
		"vec3 normalized_lightDirection = normalize(lightDirection);" \
		"vec3 diffuse = u_ld * u_kd * max(dot(normalized_lightDirection, normalized_transformed_normals), 0.0f);" \
		"vec3 reflectionVector = reflect(-normalized_lightDirection, normalized_transformed_normals);" \
		"vec3 normalized_viewerVector = normalize(viewerVector);" \
		"vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector, normalized_viewerVector), 0.0), u_matrialShininess);" \
		"phong_ads_light = ambient + diffuse + specular;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_light = vec3(1.0f, 1.0f, 1.0f);" \
		"}" \
		"FragColor = vec4(phong_ads_light, 1.0f);" \
		"}";

	GLuint fragmentShaderObjectPerFragment = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShaderObjectPerFragment, 1, (const GLchar**)&fragmentShaderSourceCodePerFragment, NULL);

	glCompileShader(fragmentShaderObjectPerFragment);

	statusPerFragment = 0;
	infoLogLengthPerFragment = 0;
	logPerFragment = NULL;

	glGetShaderiv(fragmentShaderObjectPerFragment, GL_COMPILE_STATUS, &statusPerFragment);

	if (statusPerFragment == FALSE) {

		glGetShaderiv(fragmentShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &infoLogLengthPerFragment);
		if (infoLogLengthPerFragment > 0) {

			logPerFragment = (char*)malloc(infoLogLengthPerFragment);
			if (logPerFragment != NULL) {

				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObjectPerFragment, infoLogLengthPerFragment, &written, log);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", logPerFragment);
				free(logPerFragment);
				uninitialize();

			}


		}

	}

	// Shader program object.
	shaderProgramObjectPerFragment = glCreateProgram();

	// attach desired shader to 
	glAttachShader(shaderProgramObjectPerFragment, vertexShaderObjectPerFragment);
	glAttachShader(shaderProgramObjectPerFragment, fragmentShaderObjectPerFragment);

	// Step A)-1
	glBindAttribLocation(shaderProgramObjectPerFragment, AMC_ATTRIBUTE_POSITION, "a_position");				// andhar
	glBindAttribLocation(shaderProgramObjectPerFragment, AMC_ATTRIBUTE_NORMAL, "a_normal");

	glLinkProgram(shaderProgramObjectPerFragment);

	statusPerFragment = 0;
	infoLogLengthPerFragment = 0;
	logPerFragment = NULL;

	glGetProgramiv(shaderProgramObjectPerFragment, GL_LINK_STATUS, &statusPerFragment);

	if (statusPerFragment == GL_FALSE) {

		glGetProgramiv(shaderProgramObjectPerFragment, GL_INFO_LOG_LENGTH, &infoLogLengthPerFragment);
		if (infoLogLengthPerFragment > 0) {

			logPerFragment = (char*)malloc(infoLogLengthPerFragment);
			if (logPerFragment != NULL) {

				GLsizei written;
				glGetProgramInfoLog(shaderProgramObjectPerFragment, infoLogLengthPerFragment, &written, logPerFragment);
				fprintf(gpFile, "Shader program link error log : %s\n", logPerFragment);
				free(logPerFragment);
				uninitialize();

			}


		}


	}

	modelMatrixUniform = glGetUniformLocation(shaderProgramObjectPerFragment, "u_modelMatrix");
	viewMatrixUniform = glGetUniformLocation(shaderProgramObjectPerFragment, "u_viewMatrix");
	projectionMatrixUniform = glGetUniformLocation(shaderProgramObjectPerFragment, "u_projectionMatrix");

	laUniformPerFragmentLight = glGetUniformLocation(shaderProgramObjectPerFragment, "u_la");
	ldUniformPerFragmentLight = glGetUniformLocation(shaderProgramObjectPerFragment, "u_ld");
	lsUniformPerFragmentLight = glGetUniformLocation(shaderProgramObjectPerFragment, "u_ls");
	lightPositionUniformPerFragmentLight = glGetUniformLocation(shaderProgramObjectPerFragment, "u_lightPosition");

	kaUniformPerFragmentLight = glGetUniformLocation(shaderProgramObjectPerFragment, "u_ka");
	kdUniformPerFragmentLight = glGetUniformLocation(shaderProgramObjectPerFragment, "u_kd");
	ksUniformPerFragmentLight = glGetUniformLocation(shaderProgramObjectPerFragment, "u_ks");
	materialShininessUniformPerFragmentLight = glGetUniformLocation(shaderProgramObjectPerFragment, "u_matrialShininess");

	lighitingEnabledUniformPerFragmentLight = glGetUniformLocation(shaderProgramObjectPerFragment, "u_lightingEnabled");

	//**************************************** Common ************************************************//

	// recording concept 
	// vao and vbo related code
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);
	// very important
	// position vbo
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);



	// Depth related changes
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);


	// Here starts OpenGL code
	// Clear the screen using black color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	perspectiveProjectionMatrix = mat4::identity();

	//warmup resize call
	resize(WIN_WIDTH, WIN_HEIGHT);
	return 0;
}

void printGLInfo(void) {

	// local variable declarations
	GLint numExtensions = 0;

	// code
	fprintf(gpFile, "OpenGL Vendor : %s\n", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer : %s\n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);

	fprintf(gpFile, "Number of suported extensions : %d\n", numExtensions);
	for (int i = 0; i < numExtensions; i++) {
		
		fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS, i));
	
	}

}

void resize(int width, int height) {
	

	//variable declaration

	// code
	if (height == 0)			// to avoid "/ by 0" in future calls
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);


}

void display(void) {
	
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	mat4 translationMatrix = mat4::identity();
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	//mat4 modelViewProjectionMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -2.0f);

	modelMatrix = translationMatrix;

	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//******************************************************* Per Fragment Light ***********************************************//
	if (perFragmentLight == 1) {

	glUseProgram(shaderProgramObjectPerFragment);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
	if (bLightPerFragment == TRUE) {

		glUniform1i(lighitingEnabledUniformPerFragmentLight, 1);

		glUniform3fv(laUniformPerFragmentLight, 1, LightAmbient);
		glUniform3fv(ldUniformPerFragmentLight, 1, LightDiffuse);
		glUniform3fv(lsUniformPerFragmentLight, 1, LightSpecular);

		glUniform4fv(lightPositionUniformPerFragmentLight, 1, LightPosition);

		glUniform3fv(kaUniformPerFragmentLight, 1, MaterialAmbient);
		glUniform3fv(kdUniformPerFragmentLight, 1, MaterialDiffuse);
		glUniform3fv(ksUniformPerFragmentLight, 1, MaterialSpecular);
		glUniform1f(materialShininessUniformPerFragmentLight, MaterialShininess);

	}
	else {

		glUniform1i(lighitingEnabledUniformPerFragmentLight, 0);
	}


	}
	// use the shader program object
	//******************************************************* Per Vertex Light ***********************************************//
	 else if (perVertexLight == 1) {

		glUseProgram(shaderProgramObjectPerVertex);
		// transformations
		
		glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
		glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
		if (bLightPerVertex == TRUE) {

			glUniform1i(lighitingEnabledUniformPerVertexLight, 1);

			glUniform3fv(laUniformPerVertexLight, 1, LightAmbient);
			glUniform3fv(ldUniformPerVertexLight, 1, LightDiffuse);
			glUniform3fv(lsUniformPerVertexLight, 1, LightSpecular);

			glUniform4fv(lightPositionUniformPerVertexLight, 1, LightPosition);

			glUniform3fv(kaUniformPerVertexLight, 1, MaterialAmbient);
			glUniform3fv(kdUniformPerVertexLight, 1, MaterialDiffuse);
			glUniform3fv(ksUniformPerVertexLight, 1, MaterialSpecular);
			glUniform1f(materialShininessUniformPerVertexLight, MaterialShininess);

		}
		else {

			glUniform1i(lighitingEnabledUniformPerVertexLight, 0);
		}



	} 
	
	//******************************************************* Default Light ***********************************************//
	else {
	
		glUseProgram(shaderProgramObjectPerVertex);
		
		glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
		glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
		if (bLightPerVertex == TRUE) {

			glUniform1i(lighitingEnabledUniformPerVertexLight, 1);

			glUniform3fv(laUniformPerVertexLight, 1, LightAmbient);
			glUniform3fv(ldUniformPerVertexLight, 1, LightDiffuse);
			glUniform3fv(lsUniformPerVertexLight, 1, LightSpecular);

			glUniform4fv(lightPositionUniformPerVertexLight, 1, LightPosition);

			glUniform3fv(kaUniformPerVertexLight, 1, MaterialAmbient);
			glUniform3fv(kdUniformPerVertexLight, 1, MaterialDiffuse);
			glUniform3fv(ksUniformPerVertexLight, 1, MaterialSpecular);
			glUniform1f(materialShininessUniformPerVertexLight, MaterialShininess);

		}
		else {

			glUniform1i(lighitingEnabledUniformPerVertexLight, 0);
		}

	}

	//******************************************************* Sphere Draw ***********************************************//
	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(ghdc);

}

void update(void) {
	
}

void uninitialize(void) {

	// function declarations
	void ToggleFullScreen(void);

	// code
	if (gbFullScreen) {

		ToggleFullScreen();

	}

	// Deletion and uninitialization of vbo
	if (gVbo_sphere_element) {
	
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

	if (gVbo_sphere_normal) {

		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}

	if (gVbo_sphere_position) {

		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}

	// Deletion and uninitialization of vao
	if (gVao_sphere) {
			
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}




	// shader uninitialization
	if (shaderProgramObjectPerVertex) {
		
		GLsizei numAttachedShaders;

		glUseProgram(shaderProgramObjectPerVertex);
		glGetProgramiv(shaderProgramObjectPerVertex, GL_ATTACHED_SHADERS, &numAttachedShaders);
	
		GLuint* shaderObjects = NULL;

		shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));
		
		// filling this empty buffer 
		glGetAttachedShaders(shaderProgramObjectPerVertex, numAttachedShaders, &numAttachedShaders, shaderObjects);

		for (GLsizei i = 0; i < numAttachedShaders; i++) {
			
			glDetachShader(shaderProgramObjectPerVertex, shaderObjects[i]);
			glDeleteShader(shaderObjects[i]);
			shaderObjects[i] = 0;
		
		}
		free(shaderObjects);
		shaderObjects = NULL;
		glUseProgram(0);
		glDeleteProgram(shaderProgramObjectPerVertex);
		shaderProgramObjectPerVertex = 0;

	}

	if (shaderProgramObjectPerFragment) {
		
		GLsizei numAttachedShaders;

		glUseProgram(shaderProgramObjectPerFragment);
		glGetProgramiv(shaderProgramObjectPerFragment, GL_ATTACHED_SHADERS, &numAttachedShaders);

		GLuint* shaderObjects = NULL;

		shaderObjects = (GLuint *)malloc(numAttachedShaders * sizeof(GLuint));

		// filling this empty buffer
		glGetAttachedShaders(shaderProgramObjectPerFragment, numAttachedShaders, &numAttachedShaders, shaderObjects);

		for (GLsizei i = 0; i < numAttachedShaders; i++) {
			
			glDetachShader(shaderProgramObjectPerFragment, shaderObjects[i]);
			glDeleteShader(shaderObjects[i]);
		
		}

		free(shaderObjects);
		shaderObjects = NULL;
		glUseProgram(0);
		glDeleteProgram(shaderProgramObjectPerFragment);
		shaderProgramObjectPerFragment = 0;
	
	}


	if (wglGetCurrentContext() == ghrc) {
		
		wglMakeCurrent(NULL, NULL);

	}

	if (ghrc) {
		
		wglDeleteContext(ghrc);
		ghrc = NULL;
	
	}

	if (ghdc) {
		
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	
	}


	if (ghwnd) {
		
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	
	}

	if (gpFile) {

		fprintf(gpFile, "Log File Successfully Closed\n");
		fclose(gpFile);
		gpFile = NULL;

	}

}

