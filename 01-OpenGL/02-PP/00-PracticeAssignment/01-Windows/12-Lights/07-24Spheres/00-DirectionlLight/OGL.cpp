// header files
#include <windows.h>
#include "OGL.h"

#include <stdio.h>				//for file io functions
#include <stdlib.h>				//for exit()

// OpenGL header files
#include <GL/glew.h>			// This line must be above #include <GL/gl.h> (Compulsory)
#include <GL/gl.h>
#include "vmath.h"
#include "Sphere.h"				// For sphere


using namespace vmath;

// OpenGL Libraries

#pragma comment (lib, "OpenGL32.lib")
#pragma comment (lib, "glew32.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH	800			
#define WIN_HEIGHT	600



// Global Funtion Declaration
LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

//Global variable declaration
HWND ghwnd;
BOOL gbFullScreen = FALSE;
FILE *gpFile = NULL;
BOOL gbActiveWindow = FALSE;
BOOL bDone = FALSE;
int iRetVal = 0;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

// Programmable pipeline related gloabal variables
GLuint shaderProgramObject;

enum {

	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTURE0

};

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint projectionMatrixUniform;

// Light related
GLuint laUniform;								// la = light ambient
GLuint ldUniform;								// ld = light diffuse
GLuint lsUniform;								// ls = light specular
GLuint lightPositionUniform;

GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;								// k = material
GLuint materialShininessUniform;

GLuint lightingEnabledUniform;

mat4 perspectiveProjectionMatrix;

// Sphere
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gNumVertices;
GLuint	gNumElements;

// Lighting values
GLfloat LightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat LightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat LightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat LightPosition[] = { 100.0f, 100.0f, 100.0f, 1.0f };

GLfloat MaterialAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat MaterialDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat MaterialSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat MaterialShininess = 128.0f;


GLfloat angleForXRotation = 0.0f;
GLfloat angleForYRotation = 0.0f;
GLfloat angleForZRotation = 0.0f;
GLint keyPressed = 0;

BOOL bLight = FALSE;

GLuint xUniform;
GLuint yUniform;
GLuint zUniform;

float x = 0.0f;
float y = 0.0f;
float z = -4.0f;

// Entry Point Function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {

	// function declarations
	int initialize(void);
	void display(void);
	void update(void);
	void uninitialize(void);

	// variable declarations 
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");
	//int width = 800, height = 600;
	int SysWidth = GetSystemMetrics(SM_CXSCREEN);
	int SysHeight = GetSystemMetrics(SM_CYSCREEN);

	// code

	if (fopen_s(&gpFile, "Log.txt", "w") != 0) {

		MessageBox(NULL, TEXT("Creation Of Log File Failed Exiting..."), TEXT("File I/O Error"), MB_OK);
		exit(0);

	}
	else {

		fprintf(gpFile, "Log File Successfully Opned\n");

	}


	// initalization of WNDCLASSEX structure

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;													//handle
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);					//handle
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));								//handle	
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);									//handle
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));


	// registering above wndclass
	RegisterClassEx(&wndclass);

	// create the window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("OGL Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(SysWidth / 2) - WIN_WIDTH / 2,
		(SysHeight / 2) - WIN_HEIGHT / 2,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	// initialize
	iRetVal = initialize();
	if (iRetVal == -1) {

		fprintf(gpFile, "ChoosePixelFormat falied\n");
		uninitialize();

	}
	else if (iRetVal == -2) {

		fprintf(gpFile, "SetPixelFormat falied\n");
		uninitialize();
	}
	else if (iRetVal == -3) {

		fprintf(gpFile, "wglCreateContext falied\n");
		uninitialize();
	}
	else if (iRetVal == -4) {

		fprintf(gpFile, "making OpenGL context as current context failed\n");
		uninitialize();
	
	}
	else if (iRetVal == -5) {
		
		fprintf(gpFile, "glewInit() failed");
		uninitialize();
	
	}
	else {
		
		fprintf(gpFile, "call to initalize successfull\n");
	}


	// show window
	ShowWindow(hwnd, iCmdShow);

	//Foregrounding and focusing the window
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	

	// GameLoop
	while (bDone == FALSE) {
		
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			
			if (msg.message == WM_QUIT) {
				
				bDone = TRUE;
			
			} else {
				
				TranslateMessage(&msg);
				DispatchMessage(&msg);

			}
		
		} else {
		
			if (gbActiveWindow == TRUE) {
				
				// Render the scene
				display();

				// update the scene
				update();
			
			}
		}
	
	
	}
		
	uninitialize();													//change done
	return ((int) msg.wParam);
}

// Callback Function
LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	
	int width, height;
	TCHAR str[255];

	// function declarations
	void ToggleFullScreen(void);
	void resize(int, int);
	//void uninitialize(void);

	// code
	switch (iMsg) {
		
		case WM_SETFOCUS:
			gbActiveWindow = TRUE;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = FALSE;
			break;
		case WM_ERASEBKGND:
			return 0;
		case WM_CHAR:
			switch (wParam) {
				
				case 27:
					PostQuitMessage(0);
					break;
				case 'F':
				case 'f':
					ToggleFullScreen();
					break;
				case 'L':
				case 'l':
					if (bLight == FALSE) {

						bLight = TRUE;

					}
					else {

						bLight = FALSE;

					}
					break;
				case 'X':
					x += 0.5f;
					break;
				case 'x':
					keyPressed = 1;
					angleForXRotation = 0.0f;					// reset
					angleForYRotation = 0.0f;					// reset
					angleForZRotation = 0.0f;					// reset
					x -= 0.5f;
					break;
				case 'Y':
					y += 0.5f;
					break;
				case 'y':
					keyPressed = 2;
					angleForXRotation = 0.0f;					// reset
					angleForYRotation = 0.0f;					// reset
					angleForZRotation = 0.0f;					// reset
					y -= 0.5f;
					break;
				case 'Z':
					z += 0.5f;
					break;
				case 'z':
					keyPressed = 3;
					angleForXRotation = 0.0f;					// reset
					angleForYRotation = 0.0f;					// reset
					angleForZRotation = 0.0f;					// reset
					z -= 0.5f;
					break;
				default:
					break;
			}
			break;
		case WM_SIZE:
			resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_CLOSE:
			DestroyWindow(hWnd);
			break;
		case WM_DESTROY:
			//uninitialize();
			PostQuitMessage(0);
			break;
		
		default:
			break;
	}

	return (DefWindowProc(hWnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void) {

	//Variable Declarations
	static DWORD dwStyle;
	static WINDOWPLACEMENT wp;
	MONITORINFO mi;

	//code
	wp.length = sizeof(WINDOWPLACEMENT);

	if (gbFullScreen == FALSE) {

		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) {

			mi.cbSize = sizeof(MONITORINFO);

			if (GetWindowPlacement(ghwnd, &wp) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {

				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}

			ShowCursor(FALSE);
			gbFullScreen = TRUE;

		}

	}
	else {

		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wp);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
		gbFullScreen = FALSE;
	}
}

int initialize(void) {

	// function declarations
	// warmup resize
	void resize(int, int);
	void printGLInfo(void);
	void uninitialize(void);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	// code
	// Initialization of PIXELFORMATDESCRIPTOR structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 0;
	pfd.cDepthBits = 32;							//24 can also be done

	// Get DC
	ghdc = GetDC(ghwnd);

	// Choose pixel format
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
		return -1;

	// Set the choosen pixel format
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
		return -2;

	// Create OpenGL Rendaring Context
	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
		return -3;

	// make the rendaring context as a current context 
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
		return -4;

	// glew initialization
	if (glewInit() != GLEW_OK)
		return -5;


	// Sphere
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();


	// Print OpenGL Info 
	printGLInfo();

	// Vertex shader (Step 1)
	const GLchar* vertexShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"in vec4 a_position;" \
		"in vec3 a_normal;"	\
		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \
		"uniform vec4 u_lightPosition;" \
		"uniform int u_lightingEnabled;" \
		"out vec3 transformedNormals;" \
		"out vec3 lightDirection;" \
		"out vec3 viewerVector;" \
		"uniform float xUniform;" \
		"uniform float yUniform;" \
		"uniform float zUniform;" \
		"vec3 direction = vec3(xUniform, yUniform, zUniform);" \
		"void main(void)" \
		"{" \
		"if(u_lightingEnabled == 1)" \
		"{" \
		"vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" \
		"mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" \
		"transformedNormals = normalMatrix * a_normal;" \
		"lightDirection = -direction;" \
		"viewerVector = -eyeCoordinates.xyz;" \
		"}" \
		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" \
		"}";

	// (Step 2)
	GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// (Step 3)
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// (Step 4)
	glCompileShader(vertexShaderObject);

	// (Step 5)
	GLint status;
	GLint infoLogLength;
	char* log = NULL;

	//(a)
	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE) {				// Error is present

		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0) {

			log = (char*)malloc(infoLogLength);

			if (log != NULL) {

				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", log);
				free(log);
				uninitialize();
			}
		}

	}

	// Fragment shader
	const GLchar* fragmentShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"in vec3 transformedNormals;" \
		"in vec3 lightDirection;" \
		"in vec3 viewerVector;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_matrialShininess;"	\
		"uniform int u_lightingEnabled;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_light;" \
		"if(u_lightingEnabled == 1)" \
		"{" \
		"vec3 ambient = u_la * u_ka;" \
		"vec3 normalized_transformed_normals = normalize(transformedNormals);" \
		"vec3 normalized_lightDirection = normalize(lightDirection);" \
		"vec3 diffuse = u_ld * u_kd * max(dot(normalized_lightDirection, normalized_transformed_normals), 0.0f);" \
		"vec3 reflectionVector = reflect(-normalized_lightDirection, normalized_transformed_normals);" \
		"vec3 normalized_viewerVector = normalize(viewerVector);" \
		"vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector, normalized_viewerVector), 0.0), u_matrialShininess);" \
		"phong_ads_light = ambient + diffuse + specular;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_light = vec3(1.0f, 1.0f, 1.0f);" \
		"}" \
		"FragColor = vec4(phong_ads_light, 1.0f);" \
		"}";

	GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	glCompileShader(fragmentShaderObject);

	status = 0;
	infoLogLength = 0;
	log = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status);

	if (status == FALSE) {

		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0) {

			log = (char*)malloc(infoLogLength);
			if (log != NULL) {

				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", log);
				free(log);
				uninitialize();

			}


		}

	}

	// Shader program object.
	shaderProgramObject = glCreateProgram();

	// attach desired shader to 
	glAttachShader(shaderProgramObject, vertexShaderObject);
	glAttachShader(shaderProgramObject, fragmentShaderObject);

	// Step A)-1
	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "a_position");		// andhar
	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_NORMAL, "a_normal");
	
	glLinkProgram(shaderProgramObject);

	status = 0;
	infoLogLength = 0;
	log = NULL;

	glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status);

	if (status == GL_FALSE) {

		glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0) {

			log = (char*)malloc(infoLogLength);
			if (log != NULL) {

				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);
				fprintf(gpFile, "Shader program link error log : %s\n", log);
				free(log);
				uninitialize();

			}


		}


	}

	modelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_modelMatrix");
	viewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
	projectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");
	
	laUniform = glGetUniformLocation(shaderProgramObject, "u_la");
	ldUniform = glGetUniformLocation(shaderProgramObject, "u_ld");
	lsUniform = glGetUniformLocation(shaderProgramObject, "u_ls");
	lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_lightPosition");

	kaUniform = glGetUniformLocation(shaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(shaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(shaderProgramObject, "u_ks");
	materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_matrialShininess");

	lightingEnabledUniform = glGetUniformLocation(shaderProgramObject, "u_lightingEnabled");

	xUniform = glGetUniformLocation(shaderProgramObject, "xUniform");
	yUniform = glGetUniformLocation(shaderProgramObject, "yUniform");
	zUniform = glGetUniformLocation(shaderProgramObject, "zUniform");
 

	// recording concept 
	// vao and vbo related code
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);
	// very important
	// position vbo
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);


	// Depth related changes
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);


	// Here starts OpenGL code
	// Clear the screen using black color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	perspectiveProjectionMatrix = mat4::identity();

	//warmup resize call
	resize(WIN_WIDTH, WIN_HEIGHT);
	return 0;
}

void printGLInfo(void) {

	// local variable declarations
	GLint numExtensions = 0;

	// code
	fprintf(gpFile, "OpenGL Vendor : %s\n", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer : %s\n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);

	fprintf(gpFile, "Number of suported extensions : %d\n", numExtensions);
	for (int i = 0; i < numExtensions; i++) {
		
		fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS, i));
	
	}

}

void resize(int width, int height) {
	

	//variable declaration

	// code
	if (height == 0)			// to avoid "/ by 0" in future calls
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);


}

void display(void) {
	
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// use the shader program object
	glUseProgram(shaderProgramObject);

	// transformations
	mat4 translationMatrix = mat4::identity();
	mat4 scaleMatrix = mat4::identity();
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	//mat4 modelViewProjectionMatrix = mat4::identity();

	if (bLight == TRUE) {

		MaterialAmbient[0] = 0.0215; // r
		MaterialAmbient[1] = 0.1745; // g
		MaterialAmbient[2] = 0.0215; // b
		MaterialAmbient[3] = 1.0f;   // a
		
		MaterialDiffuse[0] = 0.07568; // r
		MaterialDiffuse[1] = 0.61424; // g
		MaterialDiffuse[2] = 0.07568; // b
		MaterialDiffuse[3] = 1.0f;    // a

		// specular material
		MaterialSpecular[0] = 0.633;    // r
		MaterialSpecular[1] = 0.727811; // g
		MaterialSpecular[2] = 0.633;    // b
		MaterialSpecular[3] = 1.0f;     // a

		MaterialShininess = 0.6 * 128;

		// Light Postion
		if (keyPressed == 1) {			// x axis rotation

			LightPosition[1] = 50 * cos(angleForXRotation);
			LightPosition[2] = 50 * sin(angleForXRotation);


		}
		else if (keyPressed == 2) {	// y axis rotation

			LightPosition[0] = 50 * cos(angleForYRotation);
			LightPosition[2] = 50 * sin(angleForYRotation);


		}
		else if (keyPressed == 3) {	// z axis rotation

			LightPosition[0] = 50 * cos(angleForZRotation);
			LightPosition[1] = 50 * sin(angleForZRotation);

		}


		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

		glUniform1f(xUniform, x);
		glUniform1f(yUniform, y);
		glUniform1f(zUniform, z);
	
	} else {
		
		glUniform1i(lightingEnabledUniform, 0);

	}
	
	
	// 1 sphere
	translationMatrix = vmath::translate(1.5f, 14.0f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);

	modelMatrix = translationMatrix * scaleMatrix;

	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	// *** bind vao ***
	glBindVertexArray(gVao_sphere);
	
	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);
	glUseProgram(0);
	
	//***********************************************************************************//

	glUseProgram(shaderProgramObject);
	// 2nd sphere
	translationMatrix = vmath::translate(1.5f, 11.5f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);
	modelMatrix = translationMatrix * scaleMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
	
	if (bLight == TRUE) {

		// ambient material
		MaterialAmbient[0] = 0.135;  // r
		MaterialAmbient[1] = 0.2225; // g
		MaterialAmbient[2] = 0.1575; // b
		MaterialAmbient[3] = 1.0f;   // a

		// diffuse material
		MaterialDiffuse[0] = 0.54; // r
		MaterialDiffuse[1] = 0.89; // g
		MaterialDiffuse[2] = 0.63; // b
		MaterialDiffuse[3] = 1.0f; // a

		// specular material
		MaterialSpecular[0] = 0.316228; // r
		MaterialSpecular[1] = 0.316228; // g
		MaterialSpecular[2] = 0.316228; // b
		MaterialSpecular[3] = 1.0f;     // a

		// shininess
		MaterialShininess = 0.1 * 128;

		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

	}
	else {

		glUniform1i(lightingEnabledUniform, 0);

	}


	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);
	glUseProgram(0);
	//***********************************************************************************//

	glUseProgram(shaderProgramObject);

	if (bLight == TRUE) {

		// ambient material
		MaterialAmbient[0] = 0.05375; // r
		MaterialAmbient[1] = 0.05;    // g
		MaterialAmbient[2] = 0.06625; // b
		MaterialAmbient[3] = 1.0f;    // a

		// diffuse material
		MaterialDiffuse[0] = 0.18275; // r
		MaterialDiffuse[1] = 0.17;    // g
		MaterialDiffuse[2] = 0.22525; // b
		MaterialDiffuse[3] = 1.0f;    // a

		// specular material
		MaterialSpecular[0] = 0.332741; // r
		MaterialSpecular[1] = 0.328634; // g
		MaterialSpecular[2] = 0.346435; // b
		MaterialSpecular[3] = 1.0f;     // a

		// shininess
		MaterialShininess = 0.3 * 128;

		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

	}
	else {

		glUniform1i(lightingEnabledUniform, 0);

	}

	// 3rd sphere
	translationMatrix = vmath::translate(1.5f, 9.0f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);
	modelMatrix = translationMatrix * scaleMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);
	

	// 4th sphere
	translationMatrix = vmath::translate(1.5f, 6.5f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);
	modelMatrix = translationMatrix * scaleMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (bLight == TRUE) {

		// ambient material
		MaterialAmbient[0] = 0.25;    // r
		MaterialAmbient[1] = 0.20725; // g
		MaterialAmbient[2] = 0.20725; // b
		MaterialAmbient[3] = 1.0f;    // a

		// diffuse material
		MaterialDiffuse[0] = 1.0;   // r
		MaterialDiffuse[1] = 0.829; // g
		MaterialDiffuse[2] = 0.829; // b
		MaterialDiffuse[3] = 1.0f;  // a

		// specular material
		MaterialSpecular[0] = 0.296648; // r
		MaterialSpecular[1] = 0.296648; // g
		MaterialSpecular[2] = 0.296648; // b
		MaterialSpecular[3] = 1.0f;     // a

		// shininess
		MaterialShininess = 0.088 * 128;

		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

	}
	else {

		glUniform1i(lightingEnabledUniform, 0);

	}

	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);

	// 5th sphere
	translationMatrix = vmath::translate(1.5f, 4.0f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);
	modelMatrix = translationMatrix * scaleMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (bLight == TRUE) {

		// ambient material
		MaterialAmbient[0] = 0.1745;  // r
		MaterialAmbient[1] = 0.01175; // g
		MaterialAmbient[2] = 0.01175; // b
		MaterialAmbient[3] = 1.0f;    // a

		// diffuse material
		MaterialDiffuse[0] = 0.61424; // r
		MaterialDiffuse[1] = 0.04136; // g
		MaterialDiffuse[2] = 0.04136; // b
		MaterialDiffuse[3] = 1.0f;    // a

		// specular material
		MaterialSpecular[0] = 0.727811; // r
		MaterialSpecular[1] = 0.626959; // g
		MaterialSpecular[2] = 0.626959; // b
		MaterialSpecular[3] = 1.0f;     // a

		// shininess
		MaterialShininess = 0.6 * 128;

		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

	}
	else {

		glUniform1i(lightingEnabledUniform, 0);

	}


	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);


	// 6th sphere
	translationMatrix = vmath::translate(1.5f, 1.5f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);
	modelMatrix = translationMatrix * scaleMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (bLight == TRUE) {

		// ambient material
		MaterialAmbient[0] = 0.1;     // r
		MaterialAmbient[1] = 0.18725; // g
		MaterialAmbient[2] = 0.1745;  // b
		MaterialAmbient[3] = 1.0f;    // a

		// diffuse material
		MaterialDiffuse[0] = 0.396;   // r
		MaterialDiffuse[1] = 0.74151; // g
		MaterialDiffuse[2] = 0.69102; // b
		MaterialDiffuse[3] = 1.0f;    // a

		// specular material
		MaterialSpecular[0] = 0.297254; // r
		MaterialSpecular[1] = 0.30829;  // g
		MaterialSpecular[2] = 0.306678; // b
		MaterialSpecular[3] = 1.0f;     // a

		// shininess
		MaterialShininess = 0.1 * 128;

		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

	}
	else {

		glUniform1i(lightingEnabledUniform, 0);

	}

	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);

	//********************************************* 2nd column *******************************//
	
	// 1st sphere
	translationMatrix = vmath::translate(7.5f, 14.0f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);
	modelMatrix = translationMatrix * scaleMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (bLight == TRUE) {

		// ambient material
		MaterialAmbient[0] = 0.329412; // r
		MaterialAmbient[1] = 0.223529; // g
		MaterialAmbient[2] = 0.027451; // b
		MaterialAmbient[3] = 1.0f;     // a

		// diffuse material
		MaterialDiffuse[0] = 0.780392; // r
		MaterialDiffuse[1] = 0.568627; // g
		MaterialDiffuse[2] = 0.113725; // b
		MaterialDiffuse[3] = 1.0f;     // a

		// specular material
		MaterialSpecular[0] = 0.992157; // r
		MaterialSpecular[1] = 0.941176; // g
		MaterialSpecular[2] = 0.807843; // b
		MaterialSpecular[3] = 1.0f;     // a

		MaterialShininess = 0.21794872 * 128;

		// Light Postion
		if (keyPressed == 1) {			// x axis rotation

			LightPosition[1] = 50 * cos(angleForXRotation);
			LightPosition[2] = 50 * sin(angleForXRotation);


		}
		else if (keyPressed == 2) {	// y axis rotation

			LightPosition[0] = 50 * cos(angleForYRotation);
			LightPosition[2] = 50 * sin(angleForYRotation);


		}
		else if (keyPressed == 3) {	// z axis rotation

			LightPosition[0] = 50 * cos(angleForZRotation);
			LightPosition[1] = 50 * sin(angleForZRotation);

		}


		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

	}
	else {

		glUniform1i(lightingEnabledUniform, 0);

	}

	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);
	
	//********************************************************************************************//

	// 2nd sphere
	translationMatrix = vmath::translate(7.5f, 11.5f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);
	modelMatrix = translationMatrix * scaleMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (bLight == TRUE) {

		// ambient material
		MaterialAmbient[0] = 0.2125; // r
		MaterialAmbient[1] = 0.1275; // g
		MaterialAmbient[2] = 0.054;  // b
		MaterialAmbient[3] = 1.0f;   // a

		// diffuse material
		MaterialDiffuse[0] = 0.714;   // r
		MaterialDiffuse[1] = 0.4284;  // g
		MaterialDiffuse[2] = 0.18144; // b
		MaterialDiffuse[3] = 1.0f;    // a

		// specular material
		MaterialSpecular[0] = 0.393548; // r
		MaterialSpecular[1] = 0.271906; // g
		MaterialSpecular[2] = 0.166721; // b
		MaterialSpecular[3] = 1.0f;     // a

		MaterialShininess = 0.2 * 128;

		// Light Postion
		if (keyPressed == 1) {			// x axis rotation

			LightPosition[1] = 50 * cos(angleForXRotation);
			LightPosition[2] = 50 * sin(angleForXRotation);


		}
		else if (keyPressed == 2) {	// y axis rotation

			LightPosition[0] = 50 * cos(angleForYRotation);
			LightPosition[2] = 50 * sin(angleForYRotation);


		}
		else if (keyPressed == 3) {	// z axis rotation

			LightPosition[0] = 50 * cos(angleForZRotation);
			LightPosition[1] = 50 * sin(angleForZRotation);

		}


		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

	}
	else {

		glUniform1i(lightingEnabledUniform, 0);

	}

	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);

	//********************************************************************************************//

	// 3rd sphere
	translationMatrix = vmath::translate(7.5f, 9.0f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);
	modelMatrix = translationMatrix * scaleMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (bLight == TRUE) {

		// ambient material
		MaterialAmbient[0] = 0.25; // r
		MaterialAmbient[1] = 0.25; // g
		MaterialAmbient[2] = 0.25; // b
		MaterialAmbient[3] = 1.0f; // a

		// diffuse material
		MaterialDiffuse[0] = 0.4;  // r
		MaterialDiffuse[1] = 0.4;  // g
		MaterialDiffuse[2] = 0.4;  // b
		MaterialDiffuse[3] = 1.0f; // a

		// specular material
		MaterialSpecular[0] = 0.774597; // r
		MaterialSpecular[1] = 0.774597; // g
		MaterialSpecular[2] = 0.774597; // b
		MaterialSpecular[3] = 1.0f;     // a

		MaterialShininess = 0.6 * 128;

		// Light Postion
		if (keyPressed == 1) {			// x axis rotation

			LightPosition[1] = 50 * cos(angleForXRotation);
			LightPosition[2] = 50 * sin(angleForXRotation);


		}
		else if (keyPressed == 2) {	// y axis rotation

			LightPosition[0] = 50 * cos(angleForYRotation);
			LightPosition[2] = 50 * sin(angleForYRotation);


		}
		else if (keyPressed == 3) {	// z axis rotation

			LightPosition[0] = 50 * cos(angleForZRotation);
			LightPosition[1] = 50 * sin(angleForZRotation);

		}


		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

	}
	else {

		glUniform1i(lightingEnabledUniform, 0);

	}


	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);

	//********************************************************************************************//


	// 4th sphere
	translationMatrix = vmath::translate(7.5f, 6.5f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);
	modelMatrix = translationMatrix * scaleMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (bLight == TRUE) {

		// ambient material
		MaterialAmbient[0] = 0.19125; // r
		MaterialAmbient[1] = 0.0735;  // g
		MaterialAmbient[2] = 0.0225;  // b
		MaterialAmbient[3] = 1.0f;    // a

		// diffuse material
		MaterialDiffuse[0] = 0.7038;  // r
		MaterialDiffuse[1] = 0.27048; // g
		MaterialDiffuse[2] = 0.0828;  // b
		MaterialDiffuse[3] = 1.0f;    // a

		// specular material
		MaterialSpecular[0] = 0.256777; // r
		MaterialSpecular[1] = 0.137622; // g
		MaterialSpecular[2] = 0.086014; // b
		MaterialSpecular[3] = 1.0f;     // a

		MaterialShininess = 0.1 * 128;

		// Light Postion
		if (keyPressed == 1) {			// x axis rotation

			LightPosition[1] = 50 * cos(angleForXRotation);
			LightPosition[2] = 50 * sin(angleForXRotation);


		}
		else if (keyPressed == 2) {	// y axis rotation

			LightPosition[0] = 50 * cos(angleForYRotation);
			LightPosition[2] = 50 * sin(angleForYRotation);


		}
		else if (keyPressed == 3) {	// z axis rotation

			LightPosition[0] = 50 * cos(angleForZRotation);
			LightPosition[1] = 50 * sin(angleForZRotation);

		}


		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

	}
	else {

		glUniform1i(lightingEnabledUniform, 0);

	}


	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);

	//********************************************************************************************//

	// 5th sphere
	translationMatrix = vmath::translate(7.5f, 4.0f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);
	modelMatrix = translationMatrix * scaleMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (bLight == TRUE) {

		// ambient material
		MaterialAmbient[0] = 0.24725; // r
		MaterialAmbient[1] = 0.1995;  // g
		MaterialAmbient[2] = 0.0745;  // b
		MaterialAmbient[3] = 1.0f;    // a

		// diffuse material
		MaterialDiffuse[0] = 0.75164; // r
		MaterialDiffuse[1] = 0.60648; // g
		MaterialDiffuse[2] = 0.22648; // b
		MaterialDiffuse[3] = 1.0f;    // a

		// specular material
		MaterialSpecular[0] = 0.628281; // r
		MaterialSpecular[1] = 0.555802; // g
		MaterialSpecular[2] = 0.366065; // b
		MaterialSpecular[3] = 1.0f;     // a

		MaterialShininess = 0.4 * 128;

		// Light Postion
		if (keyPressed == 1) {			// x axis rotation

			LightPosition[1] = 50 * cos(angleForXRotation);
			LightPosition[2] = 50 * sin(angleForXRotation);


		}
		else if (keyPressed == 2) {	// y axis rotation

			LightPosition[0] = 50 * cos(angleForYRotation);
			LightPosition[2] = 50 * sin(angleForYRotation);


		}
		else if (keyPressed == 3) {	// z axis rotation

			LightPosition[0] = 50 * cos(angleForZRotation);
			LightPosition[1] = 50 * sin(angleForZRotation);

		}


		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

	}
	else {

		glUniform1i(lightingEnabledUniform, 0);

	}

	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);

	//********************************************************************************************//

	// 6th sphere
	translationMatrix = vmath::translate(7.5f, 1.5f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);
	modelMatrix = translationMatrix * scaleMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (bLight == TRUE) {

		// ambient material
		MaterialAmbient[0] = 0.19225; // r
		MaterialAmbient[1] = 0.19225; // g
		MaterialAmbient[2] = 0.19225; // b
		MaterialAmbient[3] = 1.0f;    // a

		// diffuse material
		MaterialDiffuse[0] = 0.50754; // r
		MaterialDiffuse[1] = 0.50754; // g
		MaterialDiffuse[2] = 0.50754; // b
		MaterialDiffuse[3] = 1.0f;    // a

		// specular material
		MaterialSpecular[0] = 0.508273; // r
		MaterialSpecular[1] = 0.508273; // g
		MaterialSpecular[2] = 0.508273; // b
		MaterialSpecular[3] = 1.0f;     // a

		MaterialShininess = 0.4 * 128;

		// Light Postion
		if (keyPressed == 1) {			// x axis rotation

			LightPosition[1] = 50 * cos(angleForXRotation);
			LightPosition[2] = 50 * sin(angleForXRotation);


		}
		else if (keyPressed == 2) {	// y axis rotation

			LightPosition[0] = 50 * cos(angleForYRotation);
			LightPosition[2] = 50 * sin(angleForYRotation);


		}
		else if (keyPressed == 3) {	// z axis rotation

			LightPosition[0] = 50 * cos(angleForZRotation);
			LightPosition[1] = 50 * sin(angleForZRotation);

		}


		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

	}
	else {

		glUniform1i(lightingEnabledUniform, 0);

	}


	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);

	//********************************************************************************************//
	//********************************* 3rd Column *****************************************//

	// 1st sphere
	translationMatrix = vmath::translate(13.5f, 14.0f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);
	modelMatrix = translationMatrix * scaleMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (bLight == TRUE) {

		// ambient material
		MaterialAmbient[0] = 0.0;  // r
		MaterialAmbient[1] = 0.0;  // g
		MaterialAmbient[2] = 0.0;  // b
		MaterialAmbient[3] = 1.0f; // a

		// diffuse material
		MaterialDiffuse[0] = 0.01; // r
		MaterialDiffuse[1] = 0.01; // g
		MaterialDiffuse[2] = 0.01; // b
		MaterialDiffuse[3] = 1.0f; // a

		// specular material
		MaterialSpecular[0] = 0.50; // r
		MaterialSpecular[1] = 0.50; // g
		MaterialSpecular[2] = 0.50; // b
		MaterialSpecular[3] = 1.0f; // a

		MaterialShininess = 0.25 * 128;

		// Light Postion
		if (keyPressed == 1) {			// x axis rotation

			LightPosition[1] = 50 * cos(angleForXRotation);
			LightPosition[2] = 50 * sin(angleForXRotation);


		}
		else if (keyPressed == 2) {	// y axis rotation

			LightPosition[0] = 50 * cos(angleForYRotation);
			LightPosition[2] = 50 * sin(angleForYRotation);


		}
		else if (keyPressed == 3) {	// z axis rotation

			LightPosition[0] = 50 * cos(angleForZRotation);
			LightPosition[1] = 50 * sin(angleForZRotation);

		}


		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

	}
	else {

		glUniform1i(lightingEnabledUniform, 0);

	}


	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);
	
	//********************************************************************************************//

	// 2nd sphere
	translationMatrix = vmath::translate(13.5f, 11.5f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);
	modelMatrix = translationMatrix * scaleMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (bLight == TRUE) {

		// ambient material
		MaterialAmbient[0] = 0.0;  // r
		MaterialAmbient[1] = 0.1;  // g
		MaterialAmbient[2] = 0.06; // b
		MaterialAmbient[3] = 1.0f; // a

		// diffuse material
		MaterialDiffuse[0] = 0.0;        // r
		MaterialDiffuse[1] = 0.50980392; // g
		MaterialDiffuse[2] = 0.50980392; // b
		MaterialDiffuse[3] = 1.0f;       // a

		// specular material
		MaterialSpecular[0] = 0.50196078; // r
		MaterialSpecular[1] = 0.50196078; // g
		MaterialSpecular[2] = 0.50196078; // b
		MaterialSpecular[3] = 1.0f;       // a

		MaterialShininess = 0.25 * 128;

		// Light Postion
		if (keyPressed == 1) {			// x axis rotation

			LightPosition[1] = 50 * cos(angleForXRotation);
			LightPosition[2] = 50 * sin(angleForXRotation);


		}
		else if (keyPressed == 2) {	// y axis rotation

			LightPosition[0] = 50 * cos(angleForYRotation);
			LightPosition[2] = 50 * sin(angleForYRotation);


		}
		else if (keyPressed == 3) {	// z axis rotation

			LightPosition[0] = 50 * cos(angleForZRotation);
			LightPosition[1] = 50 * sin(angleForZRotation);

		}


		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

	}
	else {

		glUniform1i(lightingEnabledUniform, 0);

	}


	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);

	//********************************************************************************************//

	// 3rd sphere
	translationMatrix = vmath::translate(13.5f, 9.0f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);
	modelMatrix = translationMatrix * scaleMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (bLight == TRUE) {

		// ambient material
		MaterialAmbient[0] = 0.0;  // r
		MaterialAmbient[1] = 0.0;  // g
		MaterialAmbient[2] = 0.0;  // b
		MaterialAmbient[3] = 1.0f; // a

		// diffuse material
		MaterialDiffuse[0] = 0.1;  // r
		MaterialDiffuse[1] = 0.35; // g
		MaterialDiffuse[2] = 0.1;  // b
		MaterialDiffuse[3] = 1.0f; // a

		// specular material
		MaterialSpecular[0] = 0.45; // r
		MaterialSpecular[1] = 0.55; // g
		MaterialSpecular[2] = 0.45; // b
		MaterialSpecular[3] = 1.0f; // a

		MaterialShininess = 0.25 * 128;

		// Light Postion
		if (keyPressed == 1) {			// x axis rotation

			LightPosition[1] = 50 * cos(angleForXRotation);
			LightPosition[2] = 50 * sin(angleForXRotation);


		}
		else if (keyPressed == 2) {	// y axis rotation

			LightPosition[0] = 50 * cos(angleForYRotation);
			LightPosition[2] = 50 * sin(angleForYRotation);


		}
		else if (keyPressed == 3) {	// z axis rotation

			LightPosition[0] = 50 * cos(angleForZRotation);
			LightPosition[1] = 50 * sin(angleForZRotation);

		}


		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

	}
	else {

		glUniform1i(lightingEnabledUniform, 0);

	}


	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);

	//********************************************************************************************//

	// 4th sphere
	translationMatrix = vmath::translate(13.5f, 6.5f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);
	modelMatrix = translationMatrix * scaleMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (bLight == TRUE) {

		// ambient material
		MaterialAmbient[0] = 0.0;  // r
		MaterialAmbient[1] = 0.0;  // g
		MaterialAmbient[2] = 0.0;  // b
		MaterialAmbient[3] = 1.0f; // a

		// diffuse material
		MaterialDiffuse[0] = 0.5;  // r
		MaterialDiffuse[1] = 0.0;  // g
		MaterialDiffuse[2] = 0.0;  // b
		MaterialDiffuse[3] = 1.0f; // a

		// specular material
		MaterialSpecular[0] = 0.7;  // r
		MaterialSpecular[1] = 0.6;  // g
		MaterialSpecular[2] = 0.6;  // b
		MaterialSpecular[3] = 1.0f; // a

		MaterialShininess = 0.25 * 128;

		// Light Postion
		if (keyPressed == 1) {			// x axis rotation

			LightPosition[1] = 50 * cos(angleForXRotation);
			LightPosition[2] = 50 * sin(angleForXRotation);


		}
		else if (keyPressed == 2) {	// y axis rotation

			LightPosition[0] = 50 * cos(angleForYRotation);
			LightPosition[2] = 50 * sin(angleForYRotation);


		}
		else if (keyPressed == 3) {	// z axis rotation

			LightPosition[0] = 50 * cos(angleForZRotation);
			LightPosition[1] = 50 * sin(angleForZRotation);

		}


		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

	}
	else {

		glUniform1i(lightingEnabledUniform, 0);

	}


	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);

	//********************************************************************************************//
	
	// 5th sphere
	translationMatrix = vmath::translate(13.5f, 4.0f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);
	modelMatrix = translationMatrix * scaleMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (bLight == TRUE) {

		// ambient material
		MaterialAmbient[0] = 0.0;  // r
		MaterialAmbient[1] = 0.0;  // g
		MaterialAmbient[2] = 0.0;  // b
		MaterialAmbient[3] = 1.0f; // a

		// diffuse material
		MaterialDiffuse[0] = 0.55; // r
		MaterialDiffuse[1] = 0.55; // g
		MaterialDiffuse[2] = 0.55; // b
		MaterialDiffuse[3] = 1.0f; // a

		// specular material
		MaterialSpecular[0] = 0.70; // r
		MaterialSpecular[1] = 0.70; // g
		MaterialSpecular[2] = 0.70; // b
		MaterialSpecular[3] = 1.0f; // a

		MaterialShininess = 0.25 * 128;

		// Light Postion
		if (keyPressed == 1) {			// x axis rotation

			LightPosition[1] = 50 * cos(angleForXRotation);
			LightPosition[2] = 50 * sin(angleForXRotation);


		}
		else if (keyPressed == 2) {	// y axis rotation

			LightPosition[0] = 50 * cos(angleForYRotation);
			LightPosition[2] = 50 * sin(angleForYRotation);


		}
		else if (keyPressed == 3) {	// z axis rotation

			LightPosition[0] = 50 * cos(angleForZRotation);
			LightPosition[1] = 50 * sin(angleForZRotation);

		}


		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

	}
	else {

		glUniform1i(lightingEnabledUniform, 0);

	}


	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);

	//********************************************************************************************//

	// 6th sphere
	translationMatrix = vmath::translate(13.5f, 1.5f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);
	modelMatrix = translationMatrix * scaleMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (bLight == TRUE) {

		// ambient material
		MaterialAmbient[0] = 0.0;  // r
		MaterialAmbient[1] = 0.0;  // g
		MaterialAmbient[2] = 0.0;  // b
		MaterialAmbient[3] = 1.0f; // a

		// diffuse material
		MaterialDiffuse[0] = 0.5;  // r
		MaterialDiffuse[1] = 0.5;  // g
		MaterialDiffuse[2] = 0.0;  // b
		MaterialDiffuse[3] = 1.0f; // a

		// specular material
		MaterialSpecular[0] = 0.60; // r
		MaterialSpecular[1] = 0.60; // g
		MaterialSpecular[2] = 0.50; // b
		MaterialSpecular[3] = 1.0f; // a

		MaterialShininess = 0.25 * 128;

		// Light Postion
		if (keyPressed == 1) {			// x axis rotation

			LightPosition[1] = 50 * cos(angleForXRotation);
			LightPosition[2] = 50 * sin(angleForXRotation);


		}
		else if (keyPressed == 2) {	// y axis rotation

			LightPosition[0] = 50 * cos(angleForYRotation);
			LightPosition[2] = 50 * sin(angleForYRotation);


		}
		else if (keyPressed == 3) {	// z axis rotation

			LightPosition[0] = 50 * cos(angleForZRotation);
			LightPosition[1] = 50 * sin(angleForZRotation);

		}


		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

	}
	else {

		glUniform1i(lightingEnabledUniform, 0);

	}


	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);

	//********************************************************************************************//
	//************************************** 4th column *********************************//
	
	// 1st sphere
	translationMatrix = vmath::translate(19.5f, 14.0f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);
	modelMatrix = translationMatrix * scaleMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (bLight == TRUE) {

		// ambient material
		MaterialAmbient[0] = 0.02; // r
		MaterialAmbient[1] = 0.02; // g
		MaterialAmbient[2] = 0.02; // b
		MaterialAmbient[3] = 1.0f; // a

		// diffuse material
		MaterialDiffuse[0] = 0.01; // r
		MaterialDiffuse[1] = 0.01; // g
		MaterialDiffuse[2] = 0.01; // b
		MaterialDiffuse[3] = 1.0f; // a

		// specular material
		MaterialSpecular[0] = 0.4;  // r
		MaterialSpecular[1] = 0.4;  // g
		MaterialSpecular[2] = 0.4;  // b
		MaterialSpecular[3] = 1.0f; // a

		MaterialShininess = 0.078125 * 128;

		// Light Postion
		if (keyPressed == 1) {			// x axis rotation

			LightPosition[1] = 50 * cos(angleForXRotation);
			LightPosition[2] = 50 * sin(angleForXRotation);


		}
		else if (keyPressed == 2) {	// y axis rotation

			LightPosition[0] = 50 * cos(angleForYRotation);
			LightPosition[2] = 50 * sin(angleForYRotation);


		}
		else if (keyPressed == 3) {	// z axis rotation

			LightPosition[0] = 50 * cos(angleForZRotation);
			LightPosition[1] = 50 * sin(angleForZRotation);

		}


		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

	}
	else {

		glUniform1i(lightingEnabledUniform, 0);

	}


	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);
	//********************************************************************************************//

	// 2nd sphere
	translationMatrix = vmath::translate(19.5f, 11.5f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);
	modelMatrix = translationMatrix * scaleMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (bLight == TRUE) {

		// ambient material
		MaterialAmbient[0] = 0.0;  // r
		MaterialAmbient[1] = 0.05; // g
		MaterialAmbient[2] = 0.05; // b
		MaterialAmbient[3] = 1.0f; // a

		// diffuse material
		MaterialDiffuse[0] = 0.4;  // r
		MaterialDiffuse[1] = 0.5;  // g
		MaterialDiffuse[2] = 0.5;  // b
		MaterialDiffuse[3] = 1.0f; // a

		// specular material
		MaterialSpecular[0] = 0.04; // r
		MaterialSpecular[1] = 0.7;  // g
		MaterialSpecular[2] = 0.7;  // b
		MaterialSpecular[3] = 1.0f; // a

		MaterialShininess = 0.078125 * 128;

		// Light Postion
		if (keyPressed == 1) {			// x axis rotation

			LightPosition[1] = 50 * cos(angleForXRotation);
			LightPosition[2] = 50 * sin(angleForXRotation);


		}
		else if (keyPressed == 2) {	// y axis rotation

			LightPosition[0] = 50 * cos(angleForYRotation);
			LightPosition[2] = 50 * sin(angleForYRotation);


		}
		else if (keyPressed == 3) {	// z axis rotation

			LightPosition[0] = 50 * cos(angleForZRotation);
			LightPosition[1] = 50 * sin(angleForZRotation);

		}


		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

	}
	else {

		glUniform1i(lightingEnabledUniform, 0);

	}


	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);

	//********************************************************************************************//

	// 3rd sphere
	translationMatrix = vmath::translate(19.5f, 9.0f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);
	modelMatrix = translationMatrix * scaleMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (bLight == TRUE) {

		// ambient material
		MaterialAmbient[0] = 0.0;  // r
		MaterialAmbient[1] = 0.05; // g
		MaterialAmbient[2] = 0.0;  // b
		MaterialAmbient[3] = 1.0f; // a

		// diffuse material
		MaterialDiffuse[0] = 0.4;  // r
		MaterialDiffuse[1] = 0.5;  // g
		MaterialDiffuse[2] = 0.4;  // b
		MaterialDiffuse[3] = 1.0f; // a

		// specular material
		MaterialSpecular[0] = 0.04; // r
		MaterialSpecular[1] = 0.7;  // g
		MaterialSpecular[2] = 0.04; // b
		MaterialSpecular[3] = 1.0f; // a

		MaterialShininess = 0.078125 * 128;

		// Light Postion
		if (keyPressed == 1) {			// x axis rotation

			LightPosition[1] = 50 * cos(angleForXRotation);
			LightPosition[2] = 50 * sin(angleForXRotation);


		}
		else if (keyPressed == 2) {	// y axis rotation

			LightPosition[0] = 50 * cos(angleForYRotation);
			LightPosition[2] = 50 * sin(angleForYRotation);


		}
		else if (keyPressed == 3) {	// z axis rotation

			LightPosition[0] = 50 * cos(angleForZRotation);
			LightPosition[1] = 50 * sin(angleForZRotation);

		}


		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

	}
	else {

		glUniform1i(lightingEnabledUniform, 0);

	}

	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);

	//********************************************************************************************//
	
	// 4th sphere
	translationMatrix = vmath::translate(19.5f, 6.5f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);
	modelMatrix = translationMatrix * scaleMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (bLight == TRUE) {

		// ambient material
		MaterialAmbient[0] = 0.05; // r
		MaterialAmbient[1] = 0.0;  // g
		MaterialAmbient[2] = 0.0;  // b
		MaterialAmbient[3] = 1.0f; // a

		// diffuse material
		MaterialDiffuse[0] = 0.5;  // r
		MaterialDiffuse[1] = 0.4;  // g
		MaterialDiffuse[2] = 0.4;  // b
		MaterialDiffuse[3] = 1.0f; // a

		// specular material
		MaterialSpecular[0] = 0.7;  // r
		MaterialSpecular[1] = 0.04; // g
		MaterialSpecular[2] = 0.04; // b
		MaterialSpecular[3] = 1.0f; // a

		MaterialShininess = 0.078125 * 128;

		// Light Postion
		if (keyPressed == 1) {			// x axis rotation

			LightPosition[1] = 50 * cos(angleForXRotation);
			LightPosition[2] = 50 * sin(angleForXRotation);


		}
		else if (keyPressed == 2) {	// y axis rotation

			LightPosition[0] = 50 * cos(angleForYRotation);
			LightPosition[2] = 50 * sin(angleForYRotation);


		}
		else if (keyPressed == 3) {	// z axis rotation

			LightPosition[0] = 50 * cos(angleForZRotation);
			LightPosition[1] = 50 * sin(angleForZRotation);

		}


		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

	}
	else {

		glUniform1i(lightingEnabledUniform, 0);

	}

	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);

	//********************************************************************************************//
	
	// 5th sphere
	translationMatrix = vmath::translate(19.5f, 4.0f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);
	modelMatrix = translationMatrix * scaleMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (bLight == TRUE) {

		// ambient material
		MaterialAmbient[0] = 0.05; // r
		MaterialAmbient[1] = 0.05; // g
		MaterialAmbient[2] = 0.05; // b
		MaterialAmbient[3] = 1.0f; // a

		// diffuse material
		MaterialDiffuse[0] = 0.5;  // r
		MaterialDiffuse[1] = 0.5;  // g
		MaterialDiffuse[2] = 0.5;  // b
		MaterialDiffuse[3] = 1.0f; // a

		// specular material
		MaterialSpecular[0] = 0.7;  // r
		MaterialSpecular[1] = 0.7;  // g
		MaterialSpecular[2] = 0.7;  // b
		MaterialSpecular[3] = 1.0f; // a

		MaterialShininess = 0.078125 * 128;

		// Light Postion
		if (keyPressed == 1) {			// x axis rotation

			LightPosition[1] = 50 * cos(angleForXRotation);
			LightPosition[2] = 50 * sin(angleForXRotation);


		}
		else if (keyPressed == 2) {	// y axis rotation

			LightPosition[0] = 50 * cos(angleForYRotation);
			LightPosition[2] = 50 * sin(angleForYRotation);


		}
		else if (keyPressed == 3) {	// z axis rotation

			LightPosition[0] = 50 * cos(angleForZRotation);
			LightPosition[1] = 50 * sin(angleForZRotation);

		}


		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

	}
	else {

		glUniform1i(lightingEnabledUniform, 0);

	}


	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);

	//********************************************************************************************//

	// 6th sphere
	translationMatrix = vmath::translate(19.5f, 1.5f, 0.0f) * vmath::translate(-10.7f, -7.8f, -18.5f);
	scaleMatrix = vmath::scale(1.50f, 1.50f, 1.50f);
	modelMatrix = translationMatrix * scaleMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (bLight == TRUE) {

		// ambient material
		MaterialAmbient[0] = 0.05; // r
		MaterialAmbient[1] = 0.05; // g
		MaterialAmbient[2] = 0.0;  // b
		MaterialAmbient[3] = 1.0f; // a

		// diffuse material
		MaterialDiffuse[0] = 0.5;  // r
		MaterialDiffuse[1] = 0.5;  // g
		MaterialDiffuse[2] = 0.4;  // b
		MaterialDiffuse[3] = 1.0f; // a

		// specular material
		MaterialSpecular[0] = 0.7;  // r
		MaterialSpecular[1] = 0.7;  // g
		MaterialSpecular[2] = 0.04; // b
		MaterialSpecular[3] = 1.0f; // a

		MaterialShininess = 0.078125 * 128;

		// Light Postion
		if (keyPressed == 1) {			// x axis rotation

			LightPosition[1] = 50 * cos(angleForXRotation);
			LightPosition[2] = 50 * sin(angleForXRotation);


		}
		else if (keyPressed == 2) {	// y axis rotation

			LightPosition[0] = 50 * cos(angleForYRotation);
			LightPosition[2] = 50 * sin(angleForYRotation);


		}
		else if (keyPressed == 3) {	// z axis rotation

			LightPosition[0] = 50 * cos(angleForZRotation);
			LightPosition[1] = 50 * sin(angleForZRotation);

		}


		glUniform1i(lightingEnabledUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1f(materialShininessUniform, MaterialShininess);

	}
	else {

		glUniform1i(lightingEnabledUniform, 0);

	}


	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);
	
	
	glUseProgram(0);

	SwapBuffers(ghdc);

}

void update(void) {
	
	if (keyPressed == 1) {

		angleForXRotation = angleForXRotation + 0.05f;
		//angleForXRotation = angleForXRotation + 1.0f;

	}

	if (keyPressed == 2) {

		angleForYRotation = angleForYRotation + 0.05f;
	}

	if (keyPressed == 3) {

		angleForZRotation = angleForZRotation + 0.05f;
	}

}

void uninitialize(void) {

	// function declarations
	void ToggleFullScreen(void);

	// code
	if (gbFullScreen) {

		ToggleFullScreen();

	}

	// Deletion and uninitialization of vbo
	if (gVbo_sphere_element) {
	
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

	if (gVbo_sphere_normal) {

		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}

	if (gVbo_sphere_position) {

		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}

	// Deletion and uninitialization of vao
	if (gVao_sphere) {
			
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}




	// shader uninitialization
	if (shaderProgramObject) {
		
		GLsizei numAttachedShaders;

		glUseProgram(shaderProgramObject);
		glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);
	
		GLuint* shaderObjects = NULL;

		shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));
		
		// filling this empty buffer 
		glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);

		for (GLsizei i = 0; i < numAttachedShaders; i++) {
			
			glDetachShader(shaderProgramObject, shaderObjects[i]);
			glDeleteShader(shaderObjects[i]);
			shaderObjects[i] = 0;
		
		}
		free(shaderObjects);
		shaderObjects = NULL;
		glUseProgram(0);
		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;

	}

	if (wglGetCurrentContext() == ghrc) {
		
		wglMakeCurrent(NULL, NULL);

	}

	if (ghrc) {
		
		wglDeleteContext(ghrc);
		ghrc = NULL;
	
	}

	if (ghdc) {
		
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	
	}


	if (ghwnd) {
		
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	
	}

	if (gpFile) {

		fprintf(gpFile, "Log File Successfully Closed\n");
		fclose(gpFile);
		gpFile = NULL;

	}

}

