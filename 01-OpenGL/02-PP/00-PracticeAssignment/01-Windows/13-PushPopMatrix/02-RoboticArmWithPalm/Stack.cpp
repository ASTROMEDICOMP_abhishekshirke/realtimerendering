#include <stdio.h>
#include <stdlib.h>
#include "vmath.h"

using namespace vmath;


struct Node {
	vmath::mat4 matrix;
	struct Node *next;
};

struct Node* head = NULL;

struct Node* createNode(mat4 chMatrix) {
	
	struct Node* newNode = (struct Node *)malloc(sizeof(struct Node));
	newNode->matrix = chMatrix;
	newNode->next = NULL;

	return newNode;

}

void push(mat4 chMatrix) {

	struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
	newNode->matrix = chMatrix;

	struct Node* temp = NULL;
	if (head == NULL) {

		head = newNode;

	}
	else {

		newNode->next = head;
		head = newNode;
	}
}

int countNode() {

	struct Node* temp = head;
	int count = 0;
	if (head == NULL) {

		return 0;
	}
	while (temp -> next != NULL) {
		temp = temp->next;
		count++;

	}

	return count;
}

vmath::mat4 pop() {
	
	struct Node* temp;
	int count = countNode();
	mat4 popMatrix = mat4::identity();


	if (head == NULL) {

		//printf("Stack is empty\n");
		return NULL;
	}

	popMatrix = head->matrix;
	temp = head;
	head = head->next;
	free(temp);
	return popMatrix;
}



//void displayNode() {
//	
//	/*printf("display\n");
//	struct Node* temp = head;
//	while (temp-> next != NULL) {
//		
//		temp = temp->next;
//	
//	}*/
//
//}

void main() {
	
	mat4 translationMatrix = mat4::identity();
	push(translationMatrix);
	push(translationMatrix);
	push(translationMatrix);
	push(translationMatrix);
	//printf("count : %d\n", countNode());
	//printf("Display Before delete\n");
	//displayNode();
	pop();
	//printf("count : %d\n", countNode());
	//printf("Display After delete\n");
	//displayNode();

}