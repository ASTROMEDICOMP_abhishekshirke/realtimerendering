#pragma once
#include <stdio.h>
#include <math.h>
#include "vmath.h"

struct Node* createNode(vmath::mat4 chMatrix);
void push(vmath::mat4 chMatrix);
int countNode();
vmath::mat4 pop();
