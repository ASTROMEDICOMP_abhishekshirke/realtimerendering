// header files
#include <windows.h>
#include "OGL.h"

#include <stdio.h>				//for file io functions
#include <stdlib.h>				//for exit()

// OpenGL header files
#include <GL/glew.h>			// This line must be above #include <GL/gl.h> (Compulsory)
#include <GL/gl.h>
#include "vmath.h"

using namespace vmath;

// OpenGL Libraries

#pragma comment (lib, "OpenGL32.lib")
#pragma comment (lib, "glew32.lib")

#define WIN_WIDTH	800			
#define WIN_HEIGHT	600

// vector
#define SUCCESS 1
#define VECTOR_EMPTY 2

// Global Funtion Declaration
LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

//Global variable declaration
HWND ghwnd;
BOOL gbFullScreen = FALSE;
FILE *gpFile = NULL;
BOOL gbActiveWindow = FALSE;
BOOL bDone = FALSE;
int iRetVal = 0;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

// Programmable pipeline related gloabal variables
GLuint shaderProgramObject;

enum {

	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTURE0

};

GLuint vao;
GLuint vbo;
GLuint vbo_indices;

GLuint mvpMatrixUniform;

mat4 perspectiveProjectionMatrix;

struct vec_float {

	float* pf;
	size_t N;

};

struct vec_int {

	int* pa;
	size_t N;

};

#define BUFFER_SIZE 1024
char buffer[BUFFER_SIZE];

FILE* gp_mesh_file;
struct vec_float *gp_vertex, *gp_texture, *gp_normal;
struct vec_float *gp_vertex_sorted, *gp_texture_sorted, *gp_normal_sorted;
struct vec_int *gp_vertex_indices, *gp_texture_indices, *gp_normal_indices;

// Entry Point Function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {

	// function declarations
	int initialize(void);
	void display(void);
	void update(void);
	void uninitialize(void);

	// variable declarations 
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");
	//int width = 800, height = 600;
	int SysWidth = GetSystemMetrics(SM_CXSCREEN);
	int SysHeight = GetSystemMetrics(SM_CYSCREEN);

	// code

	if (fopen_s(&gpFile, "Log.txt", "w") != 0) {

		MessageBox(NULL, TEXT("Creation Of Log File Failed Exiting..."), TEXT("File I/O Error"), MB_OK);
		exit(0);

	}
	else {

		fprintf(gpFile, "Log File Successfully Opned\n");

	}


	// initalization of WNDCLASSEX structure

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;													//handle
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);					//handle
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));								//handle	
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);									//handle
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));


	// registering above wndclass
	RegisterClassEx(&wndclass);

	// create the window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("OGL Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(SysWidth / 2) - WIN_WIDTH / 2,
		(SysHeight / 2) - WIN_HEIGHT / 2,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	// initialize
	iRetVal = initialize();
	if (iRetVal == -1) {

		fprintf(gpFile, "ChoosePixelFormat falied\n");
		uninitialize();

	}
	else if (iRetVal == -2) {

		fprintf(gpFile, "SetPixelFormat falied\n");
		uninitialize();
	}
	else if (iRetVal == -3) {

		fprintf(gpFile, "wglCreateContext falied\n");
		uninitialize();
	}
	else if (iRetVal == -4) {

		fprintf(gpFile, "making OpenGL context as current context failed\n");
		uninitialize();
	
	}
	else if (iRetVal == -5) {
		
		fprintf(gpFile, "glewInit() failed");
		uninitialize();
	
	}
	else {
		
		fprintf(gpFile, "call to initalize successfull\n");
	}


	// show window
	ShowWindow(hwnd, iCmdShow);

	//Foregrounding and focusing the window
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	

	// GameLoop
	while (bDone == FALSE) {
		
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			
			if (msg.message == WM_QUIT) {
				
				bDone = TRUE;
			
			} else {
				
				TranslateMessage(&msg);
				DispatchMessage(&msg);

			}
		
		} else {
		
			if (gbActiveWindow == TRUE) {
				
				// Render the scene
				display();

				// update the scene
				update();
			
			}
		}
	
	
	}
		
	uninitialize();													//change done
	return ((int) msg.wParam);
}

// Callback Function
LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	
	int width, height;
	TCHAR str[255];

	// function declarations
	void ToggleFullScreen(void);
	void resize(int, int);
	//void uninitialize(void);

	// code
	switch (iMsg) {
		
		case WM_SETFOCUS:
			gbActiveWindow = TRUE;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = FALSE;
			break;
		case WM_ERASEBKGND:
			return 0;
		case WM_CHAR:
			switch (wParam) {
				
				case 27:
					PostQuitMessage(0);
					break;
				case 'F':
				case 'f':
					ToggleFullScreen();
					break;
				
				default:
					break;
			}
			break;
		case WM_SIZE:
			resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_CLOSE:
			DestroyWindow(hWnd);
			break;
		case WM_DESTROY:
			//uninitialize();
			PostQuitMessage(0);
			break;
		
		default:
			break;
	}

	return (DefWindowProc(hWnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void) {

	//Variable Declarations
	static DWORD dwStyle;
	static WINDOWPLACEMENT wp;
	MONITORINFO mi;

	//code
	wp.length = sizeof(WINDOWPLACEMENT);

	if (gbFullScreen == FALSE) {

		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) {

			mi.cbSize = sizeof(MONITORINFO);

			if (GetWindowPlacement(ghwnd, &wp) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {

				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}

			ShowCursor(FALSE);
			gbFullScreen = TRUE;

		}

	}
	else {

		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wp);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
		gbFullScreen = FALSE;
	}
}

int initialize(void) {

	// function declarations
	// warmup resize
	void resize(int, int);
	void printGLInfo(void);
	void uninitialize(void);
	void load_mesh();

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	// code
	// Initialization of PIXELFORMATDESCRIPTOR structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 0;
	pfd.cDepthBits = 32;							//24 can also be done

	// Get DC
	ghdc = GetDC(ghwnd);

	// Choose pixel format
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
		return -1;

	// Set the choosen pixel format
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
		return -2;

	// Create OpenGL Rendaring Context
	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
		return -3;

	// make the rendaring context as a current context 
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
		return -4;

	// glew initialization
	if (glewInit() != GLEW_OK)
		return -5;

	// Print OpenGL Info 
	//printGLInfo();

	// Vertex shader (Step 1)
	const GLchar* vertexShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"in vec4 a_position;" \
		"uniform mat4 u_mvpMatrix;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvpMatrix * a_position;" \
		"}";

	// (Step 2)
	GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// (Step 3)
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// (Step 4)
	glCompileShader(vertexShaderObject);

	// (Step 5)
	GLint status;
	GLint infoLogLength;
	char* log = NULL;

	//(a)
	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE) {				// Error is present

		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0) {

			log = (char*)malloc(infoLogLength);

			if (log != NULL) {

				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", log);
				free(log);
				uninitialize();
			}
		}

	}

	// Fragment shader
	const GLchar* fragmentShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
		"}";

	GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	glCompileShader(fragmentShaderObject);

	status = 0;
	infoLogLength = 0;
	log = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status);

	if (status == FALSE) {

		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0) {

			log = (char*)malloc(infoLogLength);
			if (log != NULL) {

				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", log);
				free(log);
				uninitialize();

			}


		}

	}

	// Shader program object.
	shaderProgramObject = glCreateProgram();

	// attach desired shader to 
	glAttachShader(shaderProgramObject, vertexShaderObject);
	glAttachShader(shaderProgramObject, fragmentShaderObject);

	// Step A)-1
	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "a_position");				// andhar
	
	
	glLinkProgram(shaderProgramObject);

	status = 0;
	infoLogLength = 0;
	log = NULL;

	glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status);

	if (status == GL_FALSE) {

		glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0) {

			log = (char*)malloc(infoLogLength);
			if (log != NULL) {

				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);
				fprintf(gpFile, "Shader program link error log : %s\n", log);
				free(log);
				uninitialize();

			}


		}


	}

	mvpMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_mvpMatrix");
	load_mesh();


	// declaration of vertex data arrays;
	// recording concept 
	// vao and vbo related code
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	// very important
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	
	glBufferData(GL_ARRAY_BUFFER, gp_vertex->N * sizeof(float), gp_vertex->pf, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	// indices
	glGenBuffers(1, &vbo_indices);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_indices);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, gp_vertex_indices->N * sizeof(int), gp_vertex_indices->pa, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	
	// Depth related changes
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);


	// Here starts OpenGL code
	// Clear the screen using black color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	perspectiveProjectionMatrix = mat4::identity();

	//warmup resize call
	resize(WIN_WIDTH, WIN_HEIGHT);
	return 0;
}

void load_mesh() {
	
	// function declarations
	void uninitialize();
	struct vec_int* create_vector_int(void);
	struct vec_float* create_vector_float(void);

	void push_back_int(struct vec_int* p_vec_int, int new_element);
	void push_back_float(struct vec_float* p_vec_float, float new_element);

	void destroy_vector_float(struct vec_float* p_vec_float);
	void destroy_vector_int(struct vec_int* p_vec_int);

	// Variables
	char* space = " ", * slash = "/", * first_tok, *token;
	int nr_pos_coord = 0, nr_tex_coord = 0, nr_normal_coord = 0, nr_faces = 0;
	char* f_entries[3] = { NULL, NULL, NULL };

	// code
	
	gp_mesh_file = fopen("MonkeyHead.OBJ", "r");
	if (gp_mesh_file == NULL) {
		
		fprintf(gpFile, "error in openning file\n");
		uninitialize();
	}
	
	gp_vertex = create_vector_float();
	gp_texture = create_vector_float();
	gp_normal = create_vector_float();

	gp_vertex_indices = create_vector_int();
	gp_texture_indices = create_vector_int();
	gp_normal_indices = create_vector_int();


	  
	while (fgets(buffer, BUFFER_SIZE, gp_mesh_file) != NULL) {
			
		first_tok = strtok(buffer, space);

		if (strcmp(first_tok, "v") == 0) {
			
			nr_pos_coord++;
			while ((token = strtok(NULL, space)) != NULL)
				push_back_float(gp_vertex, atof(token));

		} else if(strcmp(first_tok, "vt") == 0) {
			
			nr_tex_coord++;
			while ((token = strtok(NULL, space)) != NULL)
				push_back_float(gp_texture, atof(token));
		

		} else if (strcmp(first_tok, "vn") == 0) {

			nr_normal_coord++;
			while ((token = strtok(NULL, space)) != NULL)
				push_back_float(gp_normal, atof(token));


		} else if (strcmp(first_tok, "f") == 0) {

			nr_faces++;
			for (int i = 0; i < 3; i++) {
				f_entries[i] = strtok(NULL, space);
			}

			for (int i = 0; i < 3; i++) {
				token = strtok(f_entries[i], slash);
				push_back_int(gp_vertex_indices, atoi(token) - 1);

				token = strtok(NULL, slash);
				push_back_int(gp_texture_indices, atoi(token) - 1);

				token = strtok(NULL, slash);
				push_back_int(gp_normal_indices, atoi(token) - 1);
			}
		}
	}

	gp_vertex_sorted = create_vector_float();
	for (int i = 0; i < gp_vertex_indices->N; i++) {
		push_back_float(gp_vertex_sorted, gp_vertex->pf[i]);
	}
	
	gp_texture_sorted = create_vector_float();
	for (int i = 0; i < gp_texture_indices->N; i++) {
		push_back_float(gp_texture_sorted, gp_texture->pf[i]);
	}

	gp_normal_sorted = create_vector_float();
	for (int i = 0; i < gp_normal_indices->N; i++) {
		push_back_float(gp_normal_sorted, gp_normal->pf[i]);
	}

	fclose(gp_mesh_file);
	gp_mesh_file = NULL;
	
	fprintf(gpFile, "vertices %d\n", nr_pos_coord);
	fprintf(gpFile, "textures %d\n", nr_tex_coord);
	fprintf(gpFile, "normals %d\n", nr_normal_coord);
	fprintf(gpFile, "faces %d\n", nr_faces);
	fprintf(gpFile, "faces Values\n");
	for (int i = 0; i < gp_vertex_indices->N; i++) {

		fprintf(gpFile, "faces[%d] %d\n", i, gp_vertex_indices->pa[i]);

	}

	fprintf(gpFile, "Vertex Values\n");
	for (int i = 0; i < gp_vertex->N; i++) {

		fprintf(gpFile, "vertex[%d] %f\n", i, gp_vertex->pf[i]);

	}

}
void printGLInfo(void) {

	// local variable declarations
	GLint numExtensions = 0;

	// code
	fprintf(gpFile, "OpenGL Vendor : %s\n", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer : %s\n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);

	fprintf(gpFile, "Number of suported extensions : %d\n", numExtensions);
	for (int i = 0; i < numExtensions; i++) {
		
		fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS, i));
	
	}

}

void resize(int width, int height) {
	

	//variable declaration

	// code
	if (height == 0)			// to avoid "/ by 0" in future calls
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);


}

void display(void) {
	
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// use the shader program object
	glUseProgram(shaderProgramObject);

	// transformations
	mat4 translationMatrix = mat4::identity();
	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();
	
	translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f);

	modelViewMatrix = translationMatrix;

	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(mvpMatrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glBindVertexArray(vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_indices);
	glDrawElements(GL_TRIANGLES, gp_vertex_indices->N, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);

	// Here there should be drawing code

	// unuse the shader program object
	glUseProgram(0);

	SwapBuffers(ghdc);

}

void update(void) {
	
}

void uninitialize(void) {

	// function declarations
	void ToggleFullScreen(void);

	// code
	if (gbFullScreen) {

		ToggleFullScreen();

	}

	// Deletion and uninitialization of vbo
	if (vbo) {
	
		glDeleteBuffers(1, &vbo);
		vbo = 0;
	}

	// Deletion and uninitialization of vao
	if (vao) {
			
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	// shader uninitialization
	if (shaderProgramObject) {
		
		GLsizei numAttachedShaders;

		glUseProgram(shaderProgramObject);
		glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);
	
		GLuint* shaderObjects = NULL;

		shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));
		
		// filling this empty buffer 
		glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);

		for (GLsizei i = 0; i < numAttachedShaders; i++) {
			
			glDetachShader(shaderProgramObject, shaderObjects[i]);
			glDeleteShader(shaderObjects[i]);
			shaderObjects[i] = 0;
		
		}
		free(shaderObjects);
		shaderObjects = NULL;
		glUseProgram(0);
		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;

	}

	if (wglGetCurrentContext() == ghrc) {
		
		wglMakeCurrent(NULL, NULL);

	}

	if (ghrc) {
		
		wglDeleteContext(ghrc);
		ghrc = NULL;
	
	}

	if (ghdc) {
		
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	
	}


	if (ghwnd) {
		
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	
	}

	if (gpFile) {

		fprintf(gpFile, "Log File Successfully Closed\n");
		fclose(gpFile);
		gpFile = NULL;

	}

}

// vector
struct vec_int* create_vector_int(void) {

	struct vec_int* p_vec_int = NULL;

	p_vec_int = (struct vec_int*)malloc(sizeof(struct vec_int));
	if (p_vec_int == NULL)
	{
		fprintf(stderr, "malloc():fatal:out of memory\n");
		exit(EXIT_FAILURE);
	}
	p_vec_int->pa = NULL;
	p_vec_int->N = 0;

	return p_vec_int;
}

void destroy_vector_int(struct vec_int* p_vec_int) {

	free(p_vec_int->pa);
	free(p_vec_int);
}

void push_back_int(struct vec_int* p_vec_int, int new_element) {

	p_vec_int->N += 1;
	p_vec_int->pa = (int *)realloc(p_vec_int->pa, p_vec_int->N * sizeof(int));
	p_vec_int->pa[p_vec_int->N - 1] = new_element;

}

int pop_back_int(struct vec_int* p_vec_int, int* p_pop_data) {

	if (p_vec_int->N == 0)
		return VECTOR_EMPTY;

	*p_pop_data = p_vec_int->pa[p_vec_int->N - 1];
	p_vec_int->N -= 1;
	p_vec_int->pa = (int *)realloc(p_vec_int->pa, p_vec_int->N * sizeof(int));

	return (SUCCESS);
}

struct vec_float* create_vector_float(void) {

	struct vec_float* p_vec_float = NULL;

	p_vec_float = (struct vec_float*)malloc(sizeof(struct vec_float));
	if (p_vec_float == NULL)
	{
		fprintf(stderr, "malloc():fatal:out of memory\n");
		exit(EXIT_FAILURE);
	}
	p_vec_float->pf = NULL;
	p_vec_float->N = 0;

	return p_vec_float;
}

void destroy_vector_float(struct vec_float* p_vec_float) {

	free(p_vec_float->pf);
	free(p_vec_float);
}

void push_back_float(struct vec_float* p_vec_float, float new_element) {

	p_vec_float->N += 1;
	p_vec_float->pf = (float *)realloc(p_vec_float->pf, p_vec_float->N * sizeof(float));
	p_vec_float->pf[p_vec_float->N - 1] = new_element;

}

int pop_back_float(struct vec_float* p_vec_float, float* p_pop_data) {

	if (p_vec_float->N == 0)
		return VECTOR_EMPTY;

	*p_pop_data = p_vec_float->pf[p_vec_float->N - 1];
	p_vec_float->N -= 1;
	p_vec_float->pf = (float *)realloc(p_vec_float->pf, p_vec_float->N * sizeof(float));

	return (SUCCESS);
}